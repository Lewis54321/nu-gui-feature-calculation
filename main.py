from PyQt5 import QtWidgets
from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.QtWidgets import QFileDialog
from PyQt5.QtWidgets import QActionGroup
from PyQt5.QtWidgets import QMessageBox
import sys
import os
import datetime
from functools import partial

import qdarkstyle
from window import Ui_MainWindow
from uis.mysql_ui import Ui_mysqlUi as MysqlUi
from classes.ProjManager import RatingProjManager, ActivityProjManager, TestProjManager
from classes.TrialManager import BlueTrialManager, MtTrialManager, AppleTrialManager
from classes.ImuManager import NuImuManager, AppleWatchManager, XsensManager
from classes.ExportManager import ExportManagerQuat, ExportManagerFeature, ExportManagerFigure
from classes.FeatureManager import FeatManager
from classes.WindowManager import WindowManager
from classes.BgWorker import WorkerClass


def exception_hook(exctype, value, traceback):
    _excepthook(exctype, value, traceback)
    sys.exit(1)


_excepthook = sys.excepthook
sys.excepthook = exception_hook


class Main(QtWidgets.QMainWindow):
    """
    This is the Main class that will be initialised when the programme is first run
    """

    def __init__(self):

        # Initialising the QMainWindow class Main inherits
        super(Main, self).__init__()
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)

        # Perform initial operations
        self._adcRec = False
        self.set_up_signals()
        self.ui.statusbar.showMessage('Please select data files and rating files to import...')

        # Setting up the background thread and background worker (class variable to prevent loss of scope)
        self.thread = QThread()
        self.worker = None

        # Classes instances and dicts which need to have class-scope so that they can be referenced in subsequent operations
        self._projManager = None
        self._filterBounds = dict()
        self._preProcSettings = dict()
        self._orientSettings = dict()

        # Global Merge Settings
        self._globalMerge = dict()
        self._secondMetrics = dict()
        self._jointMetrics = dict()

        # Export Managers
        self._classExport = None
        self._quatExport = None
        self._figExport = None

        # Directories for each of the user selected data files
        self._dataDirs = None
        self._dataBase = None

        # Base Export Directories
        self._plotExportDir = None
        self._featureExportDir = None
        self._quatExportDir = None
        self._offsetDir = None
        self.set_up_dirs(os.getcwd())

        # QWaitCondition prevents excess plots created on bg threads before closed on main thread
        self._dataProcessing = QWaitCondition()

    def set_up_signals(self):
        """
        Method for setting up the widget signals when form loads
        :return:
        """

        def night_mode(switch):
            if switch:
                app.setStyleSheet(qdarkstyle.load_stylesheet_pyqt5())
            else:
                app.setStyleSheet("")

        def tog_proj_type(label):
            if label == 'Rating':
                self.ui.pushButtonTrialDesc.setEnabled(True)
                self.ui.lineEditTrialDesc.setEnabled(True)
                self.ui.pushButtonRatingDir.setEnabled(True)
                self.ui.lineEditRatingDir.setEnabled(True)
            elif label == 'Activity':
                self.ui.pushButtonTrialDesc.setEnabled(True)
                self.ui.lineEditTrialDesc.setEnabled(True)
                self.ui.pushButtonRatingDir.setDisabled(True)
                self.ui.lineEditRatingDir.setDisabled(True)
            elif label == 'Test':
                self.ui.pushButtonRatingDir.setDisabled(True)
                self.ui.lineEditRatingDir.setDisabled(True)
                self.ui.pushButtonTrialDesc.setDisabled(True)
                self.ui.lineEditTrialDesc.setDisabled(True)
                self.ui.pushButtonCalDir.setDisabled(True)
                self.ui.lineEditCalDir.setDisabled(True)

        def tog_task_type(label):
            if label == 'Timed':
                self.ui.pushButtonTimingDir.setEnabled(True)
                self.ui.lineEditTimingDir.setEnabled(True)
            else:
                self.ui.pushButtonTimingDir.setDisabled(True)
                self.ui.lineEditTimingDir.setDisabled(True)

        def tog_imu_type(label, isTest):
            if label == 'NUIMU' and not isTest:
                self.ui.pushButtonCalDir.setEnabled(True)
                self.ui.lineEditCalDir.setEnabled(True)
            else:
                self.ui.pushButtonCalDir.setDisabled(True)
                self.ui.lineEditCalDir.setDisabled(True)

        def tog_state(widgetParent, switch):

            if switch:
                pass

            if isinstance(widgetParent, QtWidgets.QCheckBox):
                widgetParent.setChecked(False)
            elif isinstance(widgetParent, list):
                for checkBox in widgetParent:
                    checkBox.setChecked(False)
            else:
                for childWidget in widgetParent.findChildren(QtWidgets.QCheckBox):
                    childWidget.setChecked(False)

        def norm_across_tasks_check():
            """
            Checks whether to enable the ability to normalise each axis across all task figures when plotting
            Only possible when creating a new figure per task and are subplotting the metrics
            """
            if self.ui.radioButtonPlotPerTask.isChecked() and self.ui.radioButtonMetricsSubplots.isChecked():
                self.ui.radioButtonNormAxesTasks.setEnabled(True)
            else:
                self.ui.radioButtonNormAxesTasks.setDisabled(True)
                self.ui.radioButtonNormAxesTasks.setChecked(False)

        def seg_by_task_check():
            if self.ui.radioButtonTimeSeries.isChecked() and self.ui.radioButtonDataStream.isChecked():
                self.ui.checkBoxTasksSeg.setEnabled(True)
            else:
                self.ui.checkBoxTasksSeg.setDisabled(True)
                self.ui.checkBoxTasksSeg.setChecked(False)

        def tog_aggregate_features(switch):

            self.ui.frameAggFeat.setEnabled(switch)

        # Import Data
        self.ui.pushButtonData.clicked.connect(self.get_data_files)
        self.ui.pushButtonTrialDesc.clicked.connect(self.get_trial_desc)
        self.ui.pushButtonTaskFile.clicked.connect(self.get_task_file)
        self.ui.pushButtonCalDir.clicked.connect(self.get_cal_dir)
        self.ui.pushButtonRatingDir.clicked.connect(self.get_rating_dir)
        self.ui.pushButtonTimingDir.clicked.connect(self.get_timing_dir)
        self.ui.pushButtonImport.clicked.connect(self.get_import)
        self.ui.comboBoxProjType.currentTextChanged.connect(tog_proj_type)
        self.ui.comboBoxTaskType.currentTextChanged.connect(tog_task_type)
        self.ui.comboBoxImuType.currentTextChanged.connect(partial(tog_imu_type, self.ui.comboBoxProjType.currentText() == 'Test'))

        # Pre-process Data
        checkBoxList = [self.ui.checkBoxTorsoQuat, self.ui.checkBoxTareQuat, self.ui.checkBoxSaveQuat]
        self.ui.pushButtonPreP.clicked.connect(self.get_pre_proc)
        self.ui.groupBoxOrientation.toggled.connect(lambda x: None if x else
            [checkBox.setChecked(False) for checkBox in self.ui.groupBoxOrientation.findChildren(QtWidgets.QCheckBox)])
        self.ui.checkBoxQuat.toggled.connect(lambda x: None if x else
            [checkBox.setChecked(False) for checkBox in checkBoxList])
        self.ui.checkBoxTareQuat.toggled.connect(lambda x: None if x else self.ui.checkBoxAngleOffset.setChecked(False))
        self.ui.checkBoxAngleOffset.toggled.connect(lambda x: self.get_offset_dir() if x else None)
        filterTog = [self.ui.checkBoxWavDenoiseAdc, self.ui.checkBoxWavDenoiseInertial, self.ui.checkBoxNotch]
        self.ui.groupBoxFilter.toggled.connect(partial(tog_state, filterTog))

        # Global Pre-process Data
        self.ui.pushButtonGlobalPreP.clicked.connect(self.get_global_pre_proc)
        self.ui.checkBoxMergeFiltInertial.toggled.connect(partial(tog_state, [self.ui.checkBoxInertialRatio, self.ui.checkBoxJerk]))
        self.ui.checkBoxMergeQuat.toggled.connect(partial(tog_state, self.ui.frameOrientation))

        # Plotting Data
        self.ui.pushButtonPlot.clicked.connect(self.get_plot)
        self.ui.radioButtonPlotPerTask.toggled.connect(norm_across_tasks_check)
        self.ui.radioButtonMetricsSubplots.toggled.connect(norm_across_tasks_check)
        self.ui.radioButtonTimeSeries.toggled.connect(seg_by_task_check)
        self.ui.radioButtonDataStream.toggled.connect(seg_by_task_check)

        # Feature Extraction
        self.ui.checkBoxAggregate.toggled.connect(tog_aggregate_features)
        self.ui.pushButtonFeat.clicked.connect(self.get_features)

        # Actions
        self.ui.actionNight_Mode.toggled.connect(night_mode)
        self.ui.actionQuatDir.triggered.connect(partial(self.get_export_dir, 'Export Quat Dir'))
        self.ui.actionPlotDir.triggered.connect(partial(self.get_export_dir, 'Export Plot Dir'))
        self.ui.actionFeatureDir.triggered.connect(partial(self.get_export_dir, 'Export Feature Dir'))

    def set_up_dirs(self, cwd):
        """
        Method for setting up the default directories for calibration/plotting/export and displaying them
        """

        with open(os.path.join(cwd, 'dir/data_dir.txt'), 'r') as f:
            self._dataBase = f.read()
        with open(os.path.join(cwd, 'dir/trial_desc_dir.txt'), 'r') as f:
            self.ui.lineEditTrialDesc.setText(f.read())
        with open(os.path.join(cwd, 'dir/task_file.txt'), 'r') as f:
            self.ui.lineEditTaskFile.setText(f.read())
        with open(os.path.join(cwd, 'dir/cal_dir.txt'), 'r') as f:
            self.ui.lineEditCalDir.setText(f.read())
        with open(os.path.join(cwd, 'dir/rating_dir.txt'), 'r') as f:
            self.ui.lineEditRatingDir.setText(f.read())
        with open(os.path.join(cwd, 'dir/timing_dir.txt'), 'r') as f:
            self.ui.lineEditTimingDir.setText(f.read())

        self._plotExportDir = os.path.join(cwd, 'export-plots')
        self._featureExportDir = os.path.join(cwd, 'export-features')
        self._quatExportDir = os.path.join(cwd, 'export-quaternions')

    # DIRECTORY BUTTON TRIGGERED SIGNALS
    @pyqtSlot()
    def get_data_files(self):
        """
        Button triggered method for selecting CSV data files and displaying them to the used in a list widget
        :return:
        """
        dlg = QFileDialog()
        dlg.setDirectory(self._dataBase)
        dlg.setFileMode(QFileDialog.ExistingFiles)
        dlg.setNameFilter("CSV File (*.csv)")

        if dlg.exec_():

            self._dataDirs = dlg.selectedFiles()
            model = QStandardItemModel()

            for file in self._dataDirs:
                item = QStandardItem('%s' % file)
                model.appendRow(item)
        try:
            self.ui.listViewData.setModel(model)
            self.ui.listViewData.show()
            self.ui.statusbar.showMessage('%i Data Files Selected' % len(self._dataDirs))
        except BaseException:
            return
        with open('dir\\data_dir.txt', 'w') as f:
            f.write(os.path.dirname(self._dataDirs[0]))

    @pyqtSlot()
    def get_trial_desc(self):
        """
        Button triggered method for selecting the directory where the corresponding trial descriptor file is contained
        :return:
        """
        dlg = QFileDialog()
        dlg.setFileMode(QFileDialog.ExistingFile)
        dlg.setDirectory(os.path.dirname(self.ui.lineEditTrialDesc.text()))
        dlg.setNameFilter("CSV File (*.csv)")

        if dlg.exec_():
            trialDescDir = dlg.selectedFiles()[0]
        try:
            self.ui.lineEditTrialDesc.setText(trialDescDir)
        except BaseException:
            return
        with open('dir\\trial_desc_dir.txt', 'w') as f:
            f.write(trialDescDir)

    @pyqtSlot()
    def get_task_file(self):
        """
        Button triggered method for selecting the directory where the corresponding label files are contained
        :return:
        """

        dlg = QFileDialog()
        dlg.setFileMode(QFileDialog.ExistingFile)
        dlg.setDirectory(os.path.dirname(self.ui.lineEditTaskFile.text()))
        dlg.setNameFilter("Text File (*.txt)")

        if dlg.exec_():
            taskDir = dlg.selectedFiles()[0]
        try:
            self.ui.lineEditTaskFile.setText(taskDir)
        except BaseException:
            return
        with open('dir\\task_file.txt', 'w') as f:
            f.write(taskDir)

    @pyqtSlot()
    def get_cal_dir(self):
        """
        Button triggered method for selecting the directory where the corresponding label files are contained
        :return:
        """
        dlg = QFileDialog()
        dlg.setFileMode(QFileDialog.DirectoryOnly)
        dlg.setDirectory(self.ui.lineEditCalDir.text())

        if dlg.exec_():
            baseCalDir = dlg.selectedFiles()[0]
        try:
            self.ui.lineEditCalDir.setText(baseCalDir)
        except BaseException:
            return
        with open('dir\\cal_dir.txt', 'w') as f:
            f.write(baseCalDir)

    @pyqtSlot()
    def get_rating_dir(self):
        """
        Button triggered method for selecting the directory where the corresponding label files are contained
        :return:
        """
        dlg = QFileDialog()
        dlg.setFileMode(QFileDialog.DirectoryOnly)
        dlg.setDirectory(self.ui.lineEditRatingDir.text())

        if dlg.exec_():
            baseRatingDir = dlg.selectedFiles()[0]
        try:
            self.ui.lineEditRatingDir.setText(baseRatingDir)
        except BaseException:
            return
        with open('dir\\rating_dir.txt', 'w') as f:
            f.write(baseRatingDir)

    @pyqtSlot()
    def get_timing_dir(self):
        """
        Button triggered method for selecting the directory where the corresponding label files are contained
        :return:
        """
        dlg = QFileDialog()
        dlg.setFileMode(QFileDialog.DirectoryOnly)
        dlg.setDirectory(self.ui.lineEditTimingDir.text())

        if dlg.exec_():
            baseTimingDir = dlg.selectedFiles()[0]
        try:
            self.ui.lineEditTimingDir.setText(baseTimingDir)
        except BaseException:
            return
        with open('dir\\timing_dir.txt', 'w') as f:
            f.write(baseTimingDir)

    def get_export_dir(self, exportDirType):
        """
        Button triggered method for selecting the export directory of the features, plots, or quaternions
        :param exportDirType: String [Export directory type the user would like to change from default]
        """
        dlg = QFileDialog()
        dlg.setFileMode(QFileDialog.DirectoryOnly)
        if exportDirType == 'Export Plot Dir':
            dlg.setDirectory(self._plotExportDir)
        elif exportDirType == 'Export Feature Dir':
            dlg.setDirectory(self._featureExportDir)
        elif exportDirType == 'Export Quat Dir':
            dlg.setDirectory(self._quatExportDir)

        if dlg.exec_():
            if exportDirType == 'Export Plot Dir':
                self._plotExportDir = dlg.selectedFiles()[0]
            elif exportDirType == 'Export Feature Dir':
                self._featureExportDir = dlg.selectedFiles()[0]
            elif exportDirType == 'Export Quat Dir':
                self._quatExportDir = dlg.selectedFiles()[0]

    def get_offset_dir(self):

        dlg = QFileDialog()
        dlg.setFileMode(QFileDialog.ExistingFile)
        dlg.setDirectory(os.path.dirname(self.ui.lineEditTrialDesc.text()))
        dlg.setNameFilter("CSV File (*.csv)")

        if dlg.exec_():
            self._offsetDir = dlg.selectedFiles()[0]

    # PROCESS BUTTON TRIGGERED SIGNALS
    @pyqtSlot()
    def get_import(self):
        """
        Method iterates over all the test objects for this test and imports their CSV data and label files
        New 'FileManager' instance created to organise data and label directories into respective test class objects
        which are returned as a dict
        """

        if self.ui.comboBoxImuType.currentText() == 'NUIMU':
            imuClass = NuImuManager
        elif self.ui.comboBoxImuType.currentText() == 'Xsens':
            imuClass = XsensManager
        elif self.ui.comboBoxImuType.currentText() == 'Apple Watch':
            imuClass = AppleWatchManager

        if self.ui.comboBoxTrialType.currentText() == 'Blue':
            trialClass = BlueTrialManager
            subjectIden = 'UID'
        elif self.ui.comboBoxTrialType.currentText() == 'MT Manager':
            trialClass = MtTrialManager
            subjectIden = 'NPT'
        elif self.ui.comboBoxTrialType.currentText() == 'Apple':
            trialClass = AppleTrialManager
            subjectIden = 'NPT'

        # Static class to be used in each TrialManager instance for organising data depending on labelling method
        if self.ui.comboBoxTaskType.currentText() == 'Digital':
            assignTimings = False
        elif self.ui.comboBoxTaskType.currentText() == 'Timed':
            assignTimings = True

        if self.ui.comboBoxProjType.currentText() == 'Rating':
            self._projManager = RatingProjManager(assignTimings, trialClass, imuClass, subjectIden)
        elif self.ui.comboBoxProjType.currentText() == 'Activity':
            self._projManager = ActivityProjManager(assignTimings, trialClass, imuClass, subjectIden)
            self.ui.groupBoxWindow.setEnabled(True)
        elif self.ui.comboBoxProjType.currentText() == 'Test':
            self._projManager = TestProjManager(assignTimings, trialClass, imuClass, subjectIden)

        # Create a list of all the directory lineEdits which access the base file
        # and remove the ones which are not enabled since this means this path is not used for this analysis
        baseLineEdits = [self.ui.lineEditCalDir, self.ui.lineEditRatingDir, self.ui.lineEditTimingDir]
        baseNames = ['cal_dir', 'rating_dir', 'timing_dir']
        baseDir = {baseName: baseLineEdit for baseName, baseLineEdit in zip(baseNames, baseLineEdits) if
                   baseLineEdit.isEnabled()}

        # Variable to check whether user would like to interpolate missing row data
        interpNan = self.ui.menuProject_interpolate_check.isChecked()

        # Creating a new worker class to run the "ImportCsv" method and setup its signals
        # Worker class will loop through each trial in trialDict and import its respective CSV file
        self.worker = WorkerClass(self._projManager.import_operations, interpNan, dataDirs=self._dataDirs,
                                  taskDir=self.ui.lineEditTaskFile.text(), descDir=self.ui.lineEditTrialDesc.text(), **baseDir)
        self.worker.updateProg.connect(self.update_progress)
        self.worker.updateMess.connect(self.update_message)
        self.worker.managedPopup.connect(self.managed_popup)
        self.worker.finished.connect(self.thread_finished)
        self.worker.critExcept.connect(self.critical_exceptions)
        self.worker.moveToThread(self.thread)
        self.thread.started.connect(self.worker.runFunction)
        self.thread.start()
        self.ui.statusbar.showMessage('Importing Data...')
        self.ui.tabImport.setDisabled(True)

    @pyqtSlot()
    def get_pre_proc(self):
        """
        Method iterates over all the test objects for this test and filters their data
        """

        self.ui.statusbar.showMessage('Pre-processing Data...')

        # Orientation Settings
        self._orientSettings = {'flipBodySide': self.ui.checkBoxFlipBody.isChecked(),
                                'calcOrientData': self.ui.checkBoxQuat.isChecked(),
                                'simulateTorso': self.ui.checkBoxTorsoQuat.isChecked(),
                                'tareQuat': self.ui.checkBoxTareQuat.isChecked(),
                                'offsetQuat': self.ui.checkBoxAngleOffset.isChecked(),
                                'offsetDir': self._offsetDir,
                                'saveQuat': self.ui.checkBoxSaveQuat.isChecked()}

        # PreProcess Settings
        gyroBounds = {'LF': self.ui.comboBoxFilterGyroLF.currentText(), 'HF': self.ui.comboBoxFilterGyroHF.currentText()}
        accBounds = {'LF': self.ui.comboBoxFilterAccLF.currentText(), 'HF': self.ui.comboBoxFilterAccHF.currentText()}
        mmgBounds = {'LF': self.ui.comboBoxFilterMmgLF.currentText(), 'HF': self.ui.comboBoxFilterMmgHF.currentText()}
        emgBounds = {'LF': self.ui.comboBoxFilterEmgLF.currentText(), 'HF': self.ui.comboBoxFilterEmgHF.currentText()}
        filterBounds = {'gyrBounds': gyroBounds, 'accBounds': accBounds, 'mmgBounds': mmgBounds, 'emgBounds': emgBounds}
        self._preProcSettings = {'performResamp': self.ui.menuProject_interpolate_check.isChecked(),
                                 'filtBool': self.ui.groupBoxFilter.isChecked(),
                                 'filtBounds': filterBounds,
                                 'wavDenoise': self.ui.checkBoxWavDenoiseAdc.isChecked(),
                                 'notchAdc': self.ui.checkBoxNotch.isChecked()}

        # Assigning the quaternion export manager
        dateTime = datetime.datetime.now().strftime("%Y%m%d_%H%M")
        if self._orientSettings['calcOrientData'] and self._orientSettings['saveQuat']:
            baseExportDir = os.path.join(self._quatExportDir, dateTime)
            self._quatExport = ExportManagerQuat(baseExportDir, dateTime, self._orientSettings)
            self._quatExport.export_report()

        # Worker class will loop through each trial in trialDict and call a method to filter each IMUs data
        self.ui.tabPreproc.setDisabled(True)
        self.worker = WorkerClass(self._projManager.preproc_operations, self._orientSettings, self._preProcSettings)
        self.worker.updateProg.connect(self.update_progress)
        self.worker.updateMess.connect(self.update_message)
        self.worker.updateDataReady.connect(self.update_dataReady)
        self.worker.critExcept.connect(self.critical_exceptions)
        self.worker.finished.connect(self.thread_finished)
        self.worker.moveToThread(self.thread)
        self.thread.started.connect(self.worker.runFunction)
        self.thread.start()

    @pyqtSlot()
    def get_global_pre_proc(self):
        """
        Button triggered method for applying global pre-processing of the data
        This section is responsible for merging all the trial dataframes into super project dataframes for each metric
        In addition additional metric dataframes and submetric columns may be calculated here
        """

        self.ui.statusbar.showMessage('Global Pre-processing Data...')

        self._globalMerge = {'unfiltInertial': self.ui.checkBoxMergeInertial.isChecked(),
                             'filtInertial': self.ui.checkBoxMergeFiltInertial.isChecked(),
                             'unfiltAdc': self.ui.checkBoxMergeAdc.isChecked(),
                             'filtAdc': self.ui.checkBoxMergeFiltAdc.isChecked(),
                             'quat': self.ui.checkBoxMergeQuat.isChecked()}
        self._secondMetrics = {'magInertial': self.ui.checkBoxInertialMag.isChecked(),
                               'ratioInertial': self.ui.checkBoxInertialRatio.isChecked(),
                               'jerkInertial': self.ui.checkBoxJerk.isChecked()}
        self._jointMetrics = {'axisJointAngle': self.ui.checkBoxJointAxis.isChecked(),
                              'magJointAngle': self.ui.checkBoxJointMag.isChecked(),
                              'axisJointPlane': self.ui.checkBoxJointPlane.isChecked()}

        # Worker class will loop through each trial in trialDict and call a method to filter each IMUs data
        self.worker = WorkerClass(self._projManager.global_preproc_operations, self._globalMerge,
                                  self._secondMetrics, self._jointMetrics)
        self.worker.updateProg.connect(self.update_progress)
        self.worker.updateMess.connect(self.update_message)
        self.worker.critExcept.connect(self.critical_exceptions)
        self.worker.finished.connect(self.thread_finished)
        self.worker.moveToThread(self.thread)
        self.thread.started.connect(self.worker.runFunction)
        self.thread.start()
        self.ui.tabGlobalPreproc.setDisabled(True)

    @pyqtSlot()
    def get_plot(self):
        """
        Button triggered event for plotting data
        Data is provided by the super project-wide metric dataframes calculated in the global pre-processsing section
        """

        self.ui.statusbar.showMessage('Plotting Data...')

        # Various plotting settings
        plotSelection = {'unfilteredAcc': [self.ui.checkBoxUfAcc.isChecked(), 'inertialBase', 'unfilt'],
                         'filteredAcc': [self.ui.checkBoxFAcc.isChecked(), 'inertialBase', 'filt'],
                         'unfilteredGyr': [self.ui.checkBoxUfGyr.isChecked(), 'inertialBase', 'unfilt'],
                         'filteredGyr': [self.ui.checkBoxFGyr.isChecked(), 'inertialBase', 'filt'],
                         'filteredJerk': [self.ui.checkBoxFJerk.isChecked(), 'inertialBase', 'filt'],
                         'filteredAccRatio': [self.ui.checkBoxFAccRatio.isChecked(), 'inertialRatio', 'filt'],
                         'filteredGyrRatio': [self.ui.checkBoxFGyrRatio.isChecked(), 'inertialRatio', 'filt'],
                         'unfilteredAdc': [self.ui.checkBoxUfAdc.isChecked(), 'adcBase', 'unfilt'],
                         'filteredAdc': [self.ui.checkBoxFAdc.isChecked(), 'adcBase', 'filt'],
                         'jointAngleAxis': [self.ui.checkBoxJointAnglePlot.isChecked(), 'orient', None],
                         'jointAngleMag': [self.ui.checkBoxJointAngleMagPlot.isChecked(), 'orient', None],
                         'jointPlane': [self.ui.checkBoxJointPlanePlot.isChecked(), 'orient', None]}
        plotSelectionRed = {plot: plotSelection[plot][0] for plot in plotSelection}

        plotC = {'plotBool': any(plot[0] is True for plot in plotSelection.values()),
                 'plotFreqSeries': self.ui.radioButtonFreqSeries.isChecked(),
                 'plotSave': self.ui.radioButtonSavePlot.isChecked()}
        plotD = {'plotStream': self.ui.radioButtonDataStream.isChecked(),
                 'plotTasksSeg': self.ui.checkBoxTasksSeg.isChecked(),
                 'plotPerTask': self.ui.radioButtonPlotPerTask.isChecked(),
                 'subplotMetrics': self.ui.radioButtonMetricsSubplots.isChecked(),
                 'plotActivePeriods': self.ui.checkBoxActive.isChecked(),
                 'normTaskAxes': self.ui.radioButtonNormAxesTasks.isChecked(),
                 'normMetricAxes': self.ui.radioButtonNormAxesMetrics.isChecked(),
                 'legend': self.ui.checkBoxLegend.isChecked(), 'grid': self.ui.checkBoxGrid.isChecked(),
                 'rmsLine': self.ui.checkBoxRmsLine.isChecked(), 'meanFreq': self.ui.checkBoxMeanFreq.isChecked(),
                 'powerFreq': self.ui.checkBoxPower.isChecked()}
        plotMag = {'addMag': self.ui.checkBoxAddMag.isChecked(), 'onlyMag': self.ui.checkBoxOnlyMag.isChecked()}

        if not plotC['plotBool']:
            return

        # Assigning the figure export manager
        dateTime = datetime.datetime.now().strftime("%Y%m%d_%H%M")
        if self.ui.radioButtonSavePlot.isChecked():
            baseExportDir = os.path.join(self._plotExportDir, dateTime)
            self._figExport = ExportManagerFigure(baseExportDir, dateTime, plotSelectionRed, plotC, plotD)

        # Worker class will loop through each trial in trialDict and call a method to filter each IMUs data
        self.worker = WorkerClass(self._projManager.plot_operations, plotSelection, plotC, plotD, plotMag,
                                  self._dataProcessing)
        self.worker.updateProg.connect(self.update_progress)
        self.worker.updateMess.connect(self.update_message)
        if self.ui.radioButtonSavePlot.isChecked():
            self.worker.updateDataReady.connect(self.update_dataReady)
        else:
            self.worker.updateDataReady.connect(self.update_dataDisplay)
        self.worker.critExcept.connect(self.critical_exceptions)
        self.worker.finished.connect(self.thread_finished)
        self.worker.moveToThread(self.thread)
        self.thread.started.connect(self.worker.runFunction)
        self.thread.start()
        self.ui.tabPlot.setDisabled(True)

    @pyqtSlot()
    def get_features(self):
        """
        Button triggered method for extracting features and exporting the data
        Passes "featWorker" method and user set feature settings/ export dir to background thread for processing
        """

        self.ui.statusbar.showMessage('Extracting Features...')

        # Assigning user set windows spec
        windowSpecs = {'windowBool': self.ui.groupBoxWindow.isEnabled(),
                       'windowSlow': self.ui.radioButtonSlow.isChecked(),
                       'windowLength': self.ui.comboBoxWindowLength.currentText(),
                       'windowOverlap': self.ui.comboBoxWindowOverlap.currentText()}
        windowManager = WindowManager(**windowSpecs)

        # Assigning user set feature specs and passing them to the FeatureManager
        featSettings = {'inertialTime': self.ui.comboBoxInertialTime.currentText(),
                        'inertialFreq': self.ui.comboBoxInertialFreq.currentText(),
                        'adcTime': self.ui.comboBoxAdcTime.currentText(),
                        'adcFreq': self.ui.comboBoxAdcFreq.currentText(),
                        'orientTime': self.ui.comboBoxOrientTime.currentText()}
        featureManager = FeatManager(**featSettings, aggFeat=self.ui.checkBoxAggregate.isChecked())
        allMetrics = {**self._globalMerge, **self._secondMetrics, **self._jointMetrics}

        # Repeating for the ExportManager
        dateTime = datetime.datetime.now().strftime("%Y%m%d_%H%M")
        exportFolderDir = os.path.join(self._featureExportDir, 'fc_' + dateTime)
        self._classExport = ExportManagerFeature(
            exportFolderDir, dateTime, procSettings=self._preProcSettings, orientSettings=self._orientSettings,
            allMetrics = allMetrics, featureListDict=featureManager.FuncListDict,
            windowSpecs=windowSpecs, uidList=self._projManager.UidKeys)

        # Creating a new worker class to run the "FilterImu" method and setup its signals
        # Worker class will loop through each trial in trialDict and call a method to filter each IMUs data
        self.worker = WorkerClass(self._projManager.feature_operations, windowManager, featureManager)
        self.worker.updateProg.connect(self.update_progress)
        self.worker.updateMess.connect(self.update_message)
        self.worker.updateDataReady.connect(self.update_dataReady)
        self.worker.finished.connect(self.thread_finished)
        self.worker.managedPopup.connect(self.managed_popup)
        self.worker.critExcept.connect(self.critical_exceptions)
        self.worker.moveToThread(self.thread)
        self.thread.started.connect(self.worker.runFunction)
        self.thread.start()
        self.ui.tabFeature.setDisabled(True)

    # BACKGROUND WORKER TRIGGERED SIGNALS
    def update_progress(self, processType, count, totalCount):
        """
        BG Signal triggered method for updating a progress bar based on a message sent
        :param processType: String [One of the three processes run on the background thread]
        :param count: Int [How much progress has been made out of the totalCount]
        :param totalCount: Int [The total possible progress]
        :return:
        """

        currentProgress = int(count/float(totalCount) * 100)
        if (processType == 'features'):
            self.ui.progressBarFeatTask.setValue(currentProgress)
        elif (processType == 'preprocess'):
            self.ui.progressBarPreP.setValue(currentProgress)
        elif (processType == 'global_preproc'):
            self.ui.progressBarGlobalPreP.setValue(currentProgress)
        elif (processType == 'plotting'):
            self.ui.progressBarPlot.setValue(currentProgress)
        elif (processType == 'import'):
            self.ui.progressBarIm.setValue(currentProgress)

    def update_message(self, message, trans=False):
        """
        Signal triggered by the BgWorker when it needs to send an update to the user
        :param message:
        :param trans:
        """

        if trans:
            self.ui.statusbar.showMessage(message, 5000)
        else:
            self.ui.statusbar.showMessage(message)

    def update_dataReady(self, dataStage, dataType, path, data):
        """
        Thread-safe method communicating with main thread when data ready
        :param dataStage: string [feature, quat, figure]
        :param dataType: string [subDir, data, report]
        :param path: string [path to the new directory or filename depending on dataType]
        :param data: object [pd.Dataframe or Figure to be exported]
        """

        if dataStage == 'feature':
            exportClass = self._classExport
        elif dataStage == 'quat':
            exportClass = self._quatExport
        elif dataStage == 'figure':
            exportClass = self._figExport

        if dataType == 'subDir':
            exportClass.new_sub_dir(path)
        elif dataType == 'data':
            exportClass.export_data(path, data)
        elif dataType == 'report':
            if dataStage == 'feature':
                exportClass.export_report(path, list(data.index.unique(level='label')),
                                          self._projManager.InertialFreq, self._projManager.AdcFreq)
            else:
                exportClass.export_report(path)
        self._dataProcessing.wakeAll()

    def update_dataDisplay(self, dataStage, dataType, path, data):

        if dataType == 'data':
            data.show()

    def thread_finished(self, message, adcStatus):
        """
        BG Signal triggered message for displaying progress bar/ killing thread once the BG worker is finished
        :param message: String [Updates the GUI statusbar]
        :param adcStatus: Bool [Whether adc data was detected in the data]
        """

        self.update_message(message)
        if (message == 'Completed Importing CSV Files'):
            self.ui.progressBarIm.setValue(100)
            if adcStatus:
                self._adcRec = True
                self.ui.frameAdc.setEnabled(True)
                self.ui.checkBoxUfAdc.setEnabled(True)
                self.ui.checkBoxFAdc.setEnabled(True)
        elif (message == 'Completed Pre-processing IMU Data'):
            self.ui.progressBarPreP.setValue(100)
            self.ui.checkBoxMergeInertial.setEnabled(True)
            if self._preProcSettings['filtBool']:
                self.ui.checkBoxMergeFiltInertial.setEnabled(True)
            if self.ui.checkBoxQuat.isChecked():
                self.ui.checkBoxMergeQuat.setEnabled(True)
            if self._adcRec:
                self.ui.checkBoxMergeAdc.setEnabled(True)
                if self._preProcSettings['filtBool']:
                    self.ui.checkBoxMergeFiltAdc.setEnabled(True)
        elif (message == 'Completed Global Pre-processing IMU Data'):
            self.ui.progressBarGlobalPreP.setValue(100)
            if self.ui.checkBoxMergeInertial.isChecked():
                self.ui.checkBoxUfAcc.setEnabled(True)
                self.ui.checkBoxUfGyr.setEnabled(True)
            if self.ui.checkBoxMergeFiltInertial.isChecked():
                self.ui.checkBoxFAcc.setEnabled(True)
                self.ui.checkBoxFGyr.setEnabled(True)
                self.ui.frameInertialFeat.setEnabled(True)
            if self.ui.checkBoxMergeAdc.isChecked():
                self.ui.checkBoxUfAdc.setEnabled(True)
                self.ui.frameAdcFeat.setEnabled(True)
            if self.ui.checkBoxMergeFiltAdc.isChecked():
                self.ui.checkBoxFAdc.setEnabled(True)
                self.ui.frameAdcFeat.setEnabled(True)
            if self.ui.checkBoxInertialMag.isChecked():
                self.ui.checkBoxAddMag.setEnabled(True)
                self.ui.checkBoxOnlyMag.setEnabled(True)
            if self.ui.checkBoxJointAxis.isChecked():
                self.ui.checkBoxJointAngleMagPlot.setEnabled(True)
                self.ui.frameOrientationFeat.setEnabled(True)
            if self.ui.checkBoxJointMag.isChecked():
                self.ui.checkBoxJointAnglePlot.setEnabled(True)
                self.ui.frameOrientationFeat.setEnabled(True)
            if self.ui.checkBoxJointPlane.isChecked():
                self.ui.checkBoxJointPlanePlot.setEnabled(True)
                self.ui.frameOrientationFeat.setEnabled(True)
        elif (message == 'Completed Plotting IMU Data'):
            self.ui.progressBarPlot.setValue(100)
            self.ui.tabPlot.setEnabled(True)
        elif (message == 'Completed Feature Extraction'):
            self.ui.progressBarFeatTask.setValue(100)

        self.thread.quit()
        # IMPORTANT: need to set the worker to None otherwise it will still run the previous method it was initialise with
        self.worker = None

    def managed_popup(self, popupType):
        """
        Method for popups which are triggered by events on the bg threads but can only be handled by the main thread
        :param popupType: String
        """

        def update_db_click(dialog):
            dbDetails = dict()
            dbDetails['schema'] = dialog.input_ui.lineEditSchema.text()
            dbDetails['username'] = dialog.input_ui.lineEditUsername.text()
            dbDetails['password'] = dialog.input_ui.lineEditPassword.text()
            self._projManager.DbDetails = dbDetails
            dialog.reject()
            self.ui.statusbar.showMessage('Details entered. Attempting connection...')

        # Collects MySql connection details to handle memory issues during data merging
        if popupType == 'MemoryError':
            mysqlDialog = PopupDialog(MysqlUi)
            print('Memory limitation encountered. If not already performed please install, set up MySQL server, and create'
                  'a temporarily schema for data to be merged in')
            self.ui.statusbar.showMessage('Memory issue encountered. Please enter details to run in MySQL')
            mysqlDialog.input_ui.pushButtonConnect.clicked.connect(partial(update_db_click, mysqlDialog))
            mysqlDialog.show()

    def critical_exceptions(self, exception, exec_type):
        """
        Method for dealing with exceptions which are raised on the background thread and would otherwise be unhandled
        Raises a Qt error box and also passes the error onto the standard system error excepthook
        :param exception: Exception
        :param exec_type: String
        :return:
        """

        QtWidgets.QMessageBox.critical(None, "Exception Raised", "Project critical exception was raised: %s" % exception)
        self.ui.statusbar.showMessage('Operation halted due to exception. Please try again...')
        self.worker = None
        self._projManager = None
        self.ui.tabImport.setEnabled(True)
        self.ui.tabPreproc.setEnabled(True)
        self.ui.tabFeature.setEnabled(True)
        self.thread.disconnect()
        self.thread.quit()


class PopupDialog(QtWidgets.QDialog):
    """
    This is the Main class that will be initialised when the programme is first run
    """

    def __init__(self, ui):

        super(PopupDialog, self).__init__()
        self.input_ui = ui()
        self.input_ui.setupUi(self)


if __name__ == "__main__":

    # Defines a new application process
    app = QtWidgets.QApplication(sys.argv)

    # Create new UI MainWindow class object and assign the window widget to it
    mainWindow = Main()
    mainWindow.show()
    sys.exit(app.exec_())
