import numpy as np
import numbers
from numpy.linalg import norm
from collections.abc import Sequence
import pandas as pd

from .Quaternion import Quaternion


class MultiQuaternion:

    def __init__(self, w_mq, x=None, y=None, z=None):
        """
        Initializes a MultiQuaternion object
        :param w_mq: MultiQuaternion or Seq(Quaternion) or Seq(4 element array) or x by 4 array
        """

        self._mq = None

        if x is not None and y is not None and z is not None:
            w = w_mq
            mq = np.hstack([w[:, None], x[:, None], y[:, None], z[:, None]])
        elif isinstance(w_mq, MultiQuaternion):
            mq = np.array(w_mq.mq)
        elif isinstance(w_mq, Sequence) or isinstance(w_mq, pd.Series):
            mq = np.vstack(w_mq)
        else:
            mq = np.array(w_mq)
            if np.shape(w_mq)[1] != 4:
                raise ValueError("Expecting a collection of 4-element array or w x y z as parameters")

        self._set_mq(mq)

    def conj(self):
        """
        Returns the conjugate of the quaternion
        :return: Quaternion [conjugate of the quaternion]
        """
        return MultiQuaternion(self._mq[:, 0], -self._mq[:, 1], -self._mq[:, 2], -self._mq[:, 3])

    def normalise(self):
        """
        Returns the normalised quaternion
        :return: Quaternion [normalised quaternion]
        """
        return MultiQuaternion(self._mq / norm(self._mq, axis=1)[:, None])

    def to_vector(self):
        """
        Converts from quaternion to vector (3 element) representation
        :return quatVector: np.array(3)
        """

        return self._mq[:, 1:4]

    def to_twist_decomposition(self, twistAxis):
        """
        Method for decomposing the quaternion to represent the orientation around a given axis only (twist part)
        :param twistAxis: np.array(3) [Represents the axis to get the twist around]
        :return twist: MultiQuaternion [Represents the rotation around the twist axis]
        """
        vecMultiply = self * Quaternion(0, twistAxis[0], twistAxis[1], twistAxis[2]) * self.conj()
        diffVec = np.cross(twistAxis, vecMultiply.to_vector())
        mag = np.dot(twistAxis, vecMultiply.to_vector().T)
        swing = MultiQuaternion(1+mag, diffVec[:, 0], diffVec[:, 1], diffVec[:, 2]).normalise()
        twist = swing.conj() * self
        return twist

    def __mul__(self, other):
        """
        multiply the given quaternion with another quaternion or a scalar
        :param other: MultiQuaternion object, Quaternion object or number
        :return:
        """
        if isinstance(other, MultiQuaternion):
            if np.shape(self._mq)[0] != np.shape(other.mq)[0]:
                raise ValueError("MultiQuaternions must be the same size to allow quaternion multiplication")
            w = self._mq[:, 0]*other.mq[:, 0] - self._mq[:, 1]*other.mq[:, 1] - self._mq[:, 2]*other.mq[:, 2] - self._mq[:, 3]*other.mq[:, 3]
            x = self._mq[:, 0]*other.mq[:, 1] + self._mq[:, 1]*other.mq[:, 0] + self._mq[:, 2]*other.mq[:, 3] - self._mq[:, 3]*other.mq[:, 2]
            y = self._mq[:, 0]*other.mq[:, 2] - self._mq[:, 1]*other.mq[:, 3] + self._mq[:, 2]*other.mq[:, 0] + self._mq[:, 3]*other.mq[:, 1]
            z = self._mq[:, 0]*other.mq[:, 3] + self._mq[:, 1]*other.mq[:, 2] - self._mq[:, 2]*other.mq[:, 1] + self._mq[:, 3]*other.mq[:, 0]
            return MultiQuaternion(w, x, y, z)
        elif isinstance(other, Quaternion):
            w = self._mq[:, 0]*other.q[0] - self._mq[:, 1]*other.q[1] - self._mq[:, 2]*other.q[2] - self._mq[:, 3]*other.q[3]
            x = self._mq[:, 0]*other.q[1] + self._mq[:, 1]*other.q[0] + self._mq[:, 2]*other.q[3] - self._mq[:, 3]*other.q[2]
            y = self._mq[:, 0]*other.q[2] - self._mq[:, 1]*other.q[3] + self._mq[:, 2]*other.q[0] + self._mq[:, 3]*other.q[1]
            z = self._mq[:, 0]*other.q[3] + self._mq[:, 1]*other.q[2] - self._mq[:, 2]*other.q[1] + self._mq[:, 3]*other.q[0]
            return MultiQuaternion(w, x, y, z)
        elif isinstance(other, numbers.Number):
            mq = self._mq * other
            return MultiQuaternion(mq)

    def __add__(self, other):
        """
        add two quaternions element-wise or add a scalar to each element of the quaternion
        :param other:
        :return:
        """
        if not isinstance(other, MultiQuaternion):
            if np.shape(self._mq) != np.shape(other):
                raise TypeError("MultiQuaternions must be added to other MultiQuaternions or an array with same dimension")
            mq = self._mq + other
        else:
            if np.shape(self._mq)[0] != np.shape(other.mq)[0]:
                raise ValueError("MultiQuaternions must be the same size to allow MultiQuaternion addition")
            mq = self._mq + other.mq

        return MultiQuaternion(mq)

    # Implementing other interfaces to ease working with the class
    def _set_mq(self, mq):
        self._mq = mq

    def _get_mq(self):
        return self._mq

    mq = property(_get_mq, _set_mq)

    def __getitem__(self, item):
        return self._mq[item]

    def __array__(self):
        return self._mq

