import numpy as np
from .Quaternion import Quaternion
import pandas as pd
from .MultiQuaternion import MultiQuaternion


class Orientation:

    @staticmethod
    def JointQuat(quatDf):
        """
        Calculate the joint quaternion between two sets of quaternions that composes the joint for one subject
        If there is missing data present in either column then return nan to avoid errors further along processing
        :param quatDf: pd.DataFrame(Quaternion) [Dataframe composed of a columns for each body segment of the joint]
        :return jointQuat: pd.Series(Quaternion)
        """

        if any(quatDf.iloc[:, 0].isna()) or any(quatDf.iloc[:, 1].isna()):
            return pd.Series(np.repeat(np.nan, len(quatDf.index)), index=quatDf.index)
        joint1 = MultiQuaternion(quatDf.iloc[:, 0])
        joint2 = MultiQuaternion(quatDf.iloc[:, 1])
        jointQuat = joint1.conj() * joint2
        return pd.Series(list(jointQuat.__array__()), index=quatDf.index)

    @staticmethod
    def QuatToVec(jointQuat):
        """
        Calculate the vector representation of the joint quaternion
        :param jointQuat: pd.Series(np.array(4)) [4 elements of quaternion]
        :return jointVec: pd.Series(np.array(3)) [3 element vector representation of quaternion]
        """

        if any(jointQuat.isna()):
            return pd.Series(np.tile(np.nan, (len(jointQuat.index), 3)).tolist())

        # vector with direction parallel to the gravity vector in the Madgwick reference frame
        vertVec = (0, 0, 1)
        # This is the pure quaternion representation of above vector
        pureQuat = Quaternion(0, vertVec[0], vertVec[1], vertVec[2])
        # Generate the multi quaternion for faster processing
        jointMultiQuat = MultiQuaternion(jointQuat)

        # Calculating the vector that has orientation defined by the rotation of second limb relative to the first limb
        pureJointVec = jointMultiQuat * pureQuat * jointMultiQuat.conj()
        jointVec = pureJointVec.to_vector()
        return pd.Series(list(jointVec), index=jointQuat.index)

    @staticmethod
    def MinAngleBetweenVectors(vec1, vec2):
        """
        Calculate the minimum angle between two vectors
        :param vec1: np.array(n by 3) [Quaternion Rotation as as vertically stacked vector for each data point]
        :param vec2: np.array(1 by 3) [Constant Global Vector, typically vertical]
        :return minAngleMag: np.array(Float) [Minimum angle for each vector input]
        """

        dotProd = np.dot(vec1, vec2.T)
        crossProdNorm = np.linalg.norm(np.cross(vec1, vec2), axis=-1)
        minAngleMag = np.arctan2(crossProdNorm, dotProd) * (180 / np.pi)

        return minAngleMag




