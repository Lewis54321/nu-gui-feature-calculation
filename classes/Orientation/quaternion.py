# -*- coding: utf-8 -*-
"""
    Copyright (c) 2015 Jonas Böer, jonas.boeer@student.kit.edu

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

import numpy as np
import numbers
from numpy.linalg import norm


class Quaternion:
    """
    A simple class implementing basic quaternion arithmetic.
    """
    def __init__(self, w_or_q, x=None, y=None, z=None):
        """
        Initializes a Quaternion object
        :param w_or_q: A scalar representing the real part of the quaternion, another Quaternion object or a
                    four-element array containing the quaternion values
        :param x: The first imaginary part if w_or_q is a scalar
        :param y: The second imaginary part if w_or_q is a scalar
        :param z: The third imaginary part if w_or_q is a scalar
        """
        self._q = np.array([1, 0, 0, 0])

        if x is not None and y is not None and z is not None:
            w = w_or_q
            q = np.array([w, x, y, z])
        elif isinstance(w_or_q, Quaternion):
            q = np.array(w_or_q.q)
        else:
            q = np.array(w_or_q)
            if len(q) != 4:
                raise ValueError("Expecting a 4-element array or w x y z as parameters")

        self._set_q(q)

    def conj(self):
        """
        Returns the conjugate of the quaternion
        :return: Quaternion [conjugate of the quaternion]
        """
        return Quaternion(self._q[0], -self._q[1], -self._q[2], -self._q[3])

    def normalise(self):
        """
        Returns the normalised quaternion
        :return: Quaternion [normalised quaternion]
        """
        return Quaternion(self._q / norm(self._q))

    def to_vector(self):
        """
        Converts from quaternion to vector (3 element) representation
        :return quatVector: np.array(3)
        """

        return np.array([self.q[1], self.q[2], self.q[3]])

    def to_angle_axis(self):
        """
        Converts from quaternion to angle and axis representation
        If the quaternion is the identity quaternion (1, 0, 0, 0), a rotation along the x axis with angle 0 is returned.
        :return: tuple(float, float, float, float) [radians rotation, x axis value, y axis value, z axis value]
        """
        if self[0] == 1 and self[1] == 0 and self[2] == 0 and self[3] == 0:
            return 0, 1, 0, 0
        rad = np.arccos(self[0]) * 2
        imaginary_factor = np.sin(rad / 2)
        if abs(imaginary_factor) < 1e-8:
            return 0, 1, 0, 0
        x = self._q[1] / imaginary_factor
        y = self._q[2] / imaginary_factor
        z = self._q[3] / imaginary_factor
        return rad, x, y, z

    def to_euler_angles(self):
        """
        Converts to quaternion to euler angle representation
        :return: tuple (float, float, float) [roll, pitch and yaw]
        """
        pitch = np.arcsin(2 * self[1] * self[2] + 2 * self[0] * self[3])
        if np.abs(self[1] * self[2] + self[3] * self[0] - 0.5) < 1e-8:
            roll = 0
            yaw = 2 * np.arctan2(self[1], self[0])
        elif np.abs(self[1] * self[2] + self[3] * self[0] + 0.5) < 1e-8:
            roll = -2 * np.arctan2(self[1], self[0])
            yaw = 0
        else:
            roll = np.arctan2(2 * self[0] * self[1] - 2 * self[2] * self[3], 1 - 2 * self[1] ** 2 - 2 * self[3] ** 2)
            yaw = np.arctan2(2 * self[0] * self[2] - 2 * self[1] * self[3], 1 - 2 * self[2] ** 2 - 2 * self[3] ** 2)
        return roll, pitch, yaw

    def to_euler123(self):
        """
        Converts from quaternion to euler 123 angle order
        :return: tuple(float, float, float) [roll, pitch and yaw euler angles]
        """
        roll = np.arctan2(-2*(self[2]*self[3] - self[0]*self[1]), self[0]**2 - self[1]**2 - self[2]**2 + self[3]**2)
        pitch = np.arcsin(2*(self[1]*self[3] + self[0]*self[1]))
        yaw = np.arctan2(-2*(self[1]*self[2] - self[0]*self[3]), self[0]**2 + self[1]**2 - self[2]**2 - self[3]**2)
        return roll, pitch, yaw

    def to_twist_decomposition(self, twistAxis):
        """
        Method for decomposing the quaternion to represent the orientation around a given axis only (twist part)
        :param twistAxis: np.array(3)
        :return twist: Quaternion [Represents the rotation around the twist axis]
        """
        vecMultiply = self * Quaternion(0, twistAxis[0], twistAxis[1], twistAxis[2]) * self.conj()
        diffVec = np.cross(twistAxis, vecMultiply.to_vector())
        mag = np.dot(twistAxis, vecMultiply.to_vector())
        swing = Quaternion(1+mag, diffVec[0], diffVec[1], diffVec[2]).normalise()
        twist = swing.conj() * self
        return twist

    @staticmethod
    def from_angle_axis(rad, x, y, z):
        s = np.sin(rad / 2)
        return Quaternion(np.cos(rad / 2), x*s, y*s, z*s)

    @staticmethod
    def identity():
        return Quaternion(1, 0, 0, 0)

    @staticmethod
    def from_euler_angle_zyz_intrinsic(yaw, pitch, roll):
        """
        Method for converting from Tait–Bryan angles (ZYX order) intrinsic to quaternion representation
        :param yaw: float [rotation around z axis in radians]
        :param pitch: float [rotation around y axis in radians]
        :param roll: float [rotation around x axis in radians]
        :return: Quaternion
        """

        cy = np.cos(yaw * 0.5)
        sy = np.sin(yaw * 0.5)
        cp = np.cos(pitch * 0.5)
        sp = np.sin(pitch * 0.5)
        cr = np.cos(roll * 0.5)
        sr = np.sin(roll * 0.5)

        w = cy * cp * cr + sy * sp * sr
        x = cy * cp * sr - sy * sp * cr
        y = sy * cp * sr + cy * sp * cr
        z = sy * cp * cr - cy * sp * sr
        return Quaternion(w, x, y, z)

    def __mul__(self, other):
        """
        multiply the given quaternion with another quaternion or a scalar
        :param other: a Quaternion object or a number
        :return:
        """
        if isinstance(other, Quaternion):
            w = self._q[0]*other._q[0] - self._q[1]*other._q[1] - self._q[2]*other._q[2] - self._q[3]*other._q[3]
            x = self._q[0]*other._q[1] + self._q[1]*other._q[0] + self._q[2]*other._q[3] - self._q[3]*other._q[2]
            y = self._q[0]*other._q[2] - self._q[1]*other._q[3] + self._q[2]*other._q[0] + self._q[3]*other._q[1]
            z = self._q[0]*other._q[3] + self._q[1]*other._q[2] - self._q[2]*other._q[1] + self._q[3]*other._q[0]
            return Quaternion(w, x, y, z)
        elif isinstance(other, numbers.Number):
            q = self._q * other
            return Quaternion(q)

    def __truediv__(self, other):
        if isinstance(other, numbers.Number):
            q = self._q / other
        return q

    def __add__(self, other):
        """
        add two quaternions element-wise or add a scalar to each element of the quaternion
        :param other:
        :return:
        """
        if not isinstance(other, Quaternion):
            if len(other) != 4:
                raise TypeError("Quaternions must be added to other quaternions or a 4-element array")
            q = self.q + other
        else:
            q = self.q + other.q

        return Quaternion(q)

    def __sub__(self, other):

        if not isinstance(other, Quaternion):
            if len(other) != 4:
                raise TypeError("Quaternions must be subtracted to other quaternions or a 4-element array")
            q = self.q - other
        else:
            q = self.q - other.q
        return q

    # Implementing other interfaces to ease working with the class
    def _set_q(self, q):
        self._q = q

    def _get_q(self):
        return self._q

    q = property(_get_q, _set_q)

    def __getitem__(self, item):
        return self._q[item]

    def __array__(self):
        return self._q
