import pandas as pd
import numpy as np
from scipy.stats import variation
from scipy.stats import iqr
from scipy.stats import skew
from scipy.stats import kurtosis

from classes.FilterManager import FilterManager
from classes.FeatureSet.FeatureSet import FeatureSet as fs


class FeatManager:

    def __init__(self, inertialTime, inertialFreq, adcTime, adcFreq, orientTime, aggFeat):
        """
        Feature Manager Constructor
        :param inertialTime: dict{string}
        :param inertialFreq: dict{string}
        :param adcTime: dict{string}
        :param adcFreq: dict{string}
        :param orientTime: dict{string}
        :param aggFeat: bool [Determines whether aggregate features should be calculated]
        """

        # Whether or not to find aggregate features or keep them in raw format instead
        self._aggFeat = aggFeat

        # Inertial
        self._funcListInertialTime = self.feature_dict_inertial_time(inertialTime)
        self._funcListJerkTime = self.feature_dict_jerk_time(inertialTime)
        self._funcListRatioTime = self.feature_dict_ratio_time(inertialTime)
        self._funcListInertialFreq = self.feature_dict_inertial_freq(inertialFreq)

        # Adc
        self._funcListAdcTime = self.feature_dict_adc_time(adcTime)
        self._funcListAdcFreq = self.feature_dict_adc_freq(adcFreq)

        # Orientation
        self._funcListAngle = self.feature_dict_angle_time(orientTime)
        self._funcListPlane = self.feature_dict_plane_time(orientTime)

        # Timer
        self._funcListTimer = [fs.PeakToPeak]

        # Dict of the feature methods
        self._funcListDict = {'inertialTime': self._funcListInertialTime, 'inertialJerk': self._funcListJerkTime,
                              'inertialRatio': self._funcListRatioTime, 'inertialFreq': self._funcListInertialFreq,
                              'adcTime': self._funcListAdcTime, 'adcFreq': self._funcListAdcFreq,
                              'jointPlane': self._funcListPlane, 'jointAngle': self._funcListAngle,
                              'generalTimer': self._funcListTimer}

    @staticmethod
    def feature_dict_inertial_time(featureSet):
        """
        Dict to select inertial time feature functions set based on user input
        :param featureSet:
        :return:
        """
        return {
            'Rating': [np.min, np.max, fs.PeakToPeak, fs.MeanAbsValues, fs.MedianAbsValues, np.std, variation,
                       fs.Decorators.missing_data(iqr), fs.cusMedianCrossNorm, skew, kurtosis, fs.cusRms,
                       fs.SignalPower, fs.TrapInt],
            'Activity': [np.min, np.max, fs.PeakToPeak, fs.MeanAbsValues, fs.MedianAbsValues, np.std, variation,
                         fs.Decorators.missing_data(iqr), fs.cusMedianCrossNorm, skew, kurtosis, fs.cusRms,
                         fs.SignalPower, fs.TrapInt],
            'Low Cost': [fs.PeakToPeak, np.mean, np.std],
        }.get(featureSet, [])

    @staticmethod
    def feature_dict_jerk_time(featureSet):
        """
        Dict to select inertial jerk time feature functions set based on user input
        :param featureSet:
        :return:
        """
        return {
            'Rating': [np.max, np.min, fs.PeakToPeak],
            'Activity': [np.max, np.min, fs.PeakToPeak],
            'Low Cost': [fs.PeakToPeak],
        }.get(featureSet, [])

    @staticmethod
    def feature_dict_ratio_time(featureSet):
        """
        Dict to select inertial ratio time feature functions set based on user input
        :param featureSet:
        :return:
        """
        return {
            'Rating': [np.max, np.min, fs.PeakToPeak, np.mean, np.median, np.std, variation, fs.cusMedianCrossNorm],
            'Activity': [np.max, np.min, fs.PeakToPeak, np.mean, np.median, np.std, variation, fs.cusMedianCrossNorm],
            'Low Cost': [fs.PeakToPeak, np.mean, np.std],
        }.get(featureSet, [])

    @staticmethod
    def feature_dict_inertial_freq(featureSet):
        """
        Dict to select inertial freq feature functions set based on user input
        :param featureSet:
        :return:
        """
        return {
            'Rating': [fs.DomFrequency, fs.MeanFrequency, fs.MedianFrequency, fs.MeanPower, fs.TotalPower,
                       fs.FrequencyRatio, fs.PowerSpectrumRatioFreq],
            'Activity': [fs.DomFrequency, fs.MeanFrequency, fs.MedianFrequency, fs.MeanPower, fs.TotalPower,
                         fs.FrequencyRatio, fs.PowerSpectrumRatioFreq],
            'Low Cost': [fs.MeanPower],
        }.get(featureSet, [])

    @staticmethod
    def feature_dict_adc_time(featureSet):
        """
        Dict to select adc time feature function set based on user input
        :param featureSet:
        :return:
        """

        return {
            'Rating': [fs.ModMeanAbsValue1, fs.LogDetector, fs.AverageAmplitudeChange,
                       fs.DifferenceAbsSD, fs.ZeroCrossingPercentage, fs.MyopulseRatePercentage, fs.WillisonAmplitudePercentage, fs.SlopeSignChangePercentage,
                       fs.HammingWindowPercentage],
            'Activity': [fs.ModMeanAbsValue1, fs.LogDetector, fs.AverageAmplitudeChange,
                         fs.DifferenceAbsSD, fs.ZeroCrossingPercentage, fs.MyopulseRatePercentage, fs.WillisonAmplitudePercentage, fs.SlopeSignChangePercentage,
                         fs.HammingWindowPercentage],
            'Low Cost': [fs.MyopulseRatePercentage, fs.ModMeanAbsValue1, fs.PeakToPeak],
        }.get(featureSet, [])

    @staticmethod
    def feature_dict_adc_freq(featureSet):
        """
        Dict to select adc freq feature function set based on user input
        :param featureSet:
        :return:
        """

        return {
            'Rating': [fs.DomFrequency, fs.MeanFrequency, fs.MedianFrequency, fs.MeanPower, fs.TotalPower,
                       fs.FrequencyRatio, fs.PowerSpectrumRatioPoints, fs.VarianceCentralFrequency],
            'Activity': [fs.DomFrequency, fs.MeanFrequency, fs.MedianFrequency, fs.MeanPower, fs.FrequencyRatio,
                         fs.PowerSpectrumRatioPoints, fs.VarianceCentralFrequency],
            'Low Cost': [fs.MeanPower],
        }.get(featureSet, [])

    @staticmethod
    def feature_dict_angle_time(featureSet):
        """
        Dict to select joint angle time feature functions set based on user input
        :param featureSet:
        :return:
        """
        return {
            'Rating': [np.max, np.min, fs.PeakToPeak, np.mean, np.median, np.std, variation, fs.cusMedianCrossNorm],
            'Activity': [np.max, np.min, fs.PeakToPeak, np.mean, np.median, np.std, variation, fs.cusMedianCrossNorm],
            'Low Cost': [fs.PeakToPeak, np.mean, np.std],
        }.get(featureSet, [])

    @staticmethod
    def feature_dict_plane_time(featureSet):
        """
        Dict to select joint plane time feature functions set based on user input
        :param featureSet:
        :return:
        """
        return {
            'Rating': [np.max, np.min, fs.PeakToPeak, np.mean, np.median, np.std, variation, fs.cusMedianCrossNorm],
            'Activity': [np.max, np.min, fs.PeakToPeak, np.mean, np.median, np.std, variation, fs.cusMedianCrossNorm],
            'Low Cost': [fs.PeakToPeak, np.mean, np.std],
        }.get(featureSet, [])

    # ************************* FEATURE METHODS ******************************* #

    def feature_calc(self, data, seriesType, metricType, freq):
        """
        Method for calculating the user selected set of features for the passed DataFrame
        :param data: pd.DataFrame [index: trial/ window, columns: location/ metric]
        :param seriesType: String
        :param metricType: String
        :param freq: Int [the lower frequency across the project, used for FFT and standardising time series data]
        :return featData: pd.DataFrame [index: trial, columns: metric/ feature]
        """

        # RAW FEATURES
        if not self._aggFeat:
            if metricType == 'adc':
                return None
            else:
                data = data.T.set_index(np.repeat('Raw', data.shape[1]), append=True).T
                return data
        """Drop ADC data due to imbalanced number data points compared to inertial data (due to different sampling rates)"""

        # AGGREGATE FEATURE CALCULATION
        funcList = {
            'time': {'inertial': self._funcListInertialTime, 'adc': self._funcListAdcTime,
                     'jerk': self._funcListJerkTime, 'ratio': self._funcListRatioTime,
                     'jointPlane': self._funcListPlane, 'jointAngle': self._funcListAngle}.get(metricType),
            'freq': {'inertial': self._funcListInertialFreq, 'adc': self._funcListAdcFreq}.get(metricType),
            'general': {'timer': self._funcListTimer}.get(metricType),
        }.get(seriesType)

        # STANDARDISE DATA
        if seriesType == 'time':
            data = data.groupby(level=['trial', 'window']).apply(FilterManager.standardise_time_series, freq, False)
        elif seriesType == 'freq':
            freqData = data.groupby(level=['trial', 'window']).apply(FilterManager.fast_fourier_transform, freq)
            data = freqData
        """This step adds either time or freq bins as the innermost index. Index: trial/ window/ bins"""

        # FEATURE CALCULATION
        featData = data.groupby(level=['trial', 'window']).agg(funcList)
        """index: trial/ window, columns: metric/ feature"""

        # DROP WINDOWS INDEX SINCE NO LONGER REQUIRED
        featData.index = featData.index.droplevel(level='window')

        return featData

    # ***************************** PROPERTIES ******************************** #

    @property
    def FuncListDict(self):
        return self._funcListDict

