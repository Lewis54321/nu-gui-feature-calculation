import numpy as np
import pandas as pd


class WindowManager:

    def __init__(self, windowBool, windowSlow, windowLength=None, windowOverlap=None):
        """
        Initializer
        :param windowBool: Bool [Whether task data should be windowed (Activity) or not (Scored)
        :param windowLength: Float [Length (seconds) of the window]
        :param windowOverlap: Int [Percentage window overlap]
        """

        self._windowBool = windowBool
        if windowBool:
            self._windowLength = float(windowLength)
            self._windowOverlap = int(windowOverlap)

        if windowSlow:
            self._windowFunc = self.window_function_slow
        else:
            self._windowFunc = self.window_function_fast
        self._winSlow = windowSlow

        self._prevPt = 0
        self._prevTask = 0
        self._window = 1
        self._winDict = dict()    # slow implementation
        self._winIdxList = list()     # fast implementation
        self._winOverlapIdxList = list()    # fast implementation

    def window_data(self, data, projFreq, dropTime):
        """
        Method for applying windows to the entire DataFrame
        In the case of no windowing to be applied then just want to use the entire task instance as the window (i.e. rename that column)
        :param data: pd.DataFrame [index: trial/ task/ task_inst, columns: location/ metric]
        :param projFreq: int [The minimum frequency used across all trials and all imus]
        :param dropTime: Bool
        :return winData: pd.DataFrame [index: task/ trial/ window, columns: location/ metric]
        """

        if not self._windowBool:
            data.index.rename('window', level='task_inst', inplace=True)
            if dropTime:
                return data.reorder_levels(['task', 'trial', 'window'], axis=0).drop('time_stamp', level='metric', axis=1)
            return data.reorder_levels(['task', 'trial', 'window'])

        if dropTime:
            metricsDrop = ['task_inst', 'ALL']
        else:
            metricsDrop = ['task_inst']

        # Calculating the window parameters per patient
        elementsPerWindow = int(int(projFreq) * self._windowLength)
        elementsPerOverlap = int((self._windowOverlap / 100) * elementsPerWindow)
        elementsPerGap = elementsPerWindow - (elementsPerOverlap * 2)

        # Passing a group of data equal to each task instance to the windowing function
        for _, taskInst in data.groupby(level=['trial', 'task', 'task_inst']):
            trial = taskInst.index.get_level_values('trial')[0]
            task = taskInst.index.get_level_values('task')[0]
            self._windowFunc(taskInst, elementsPerWindow, elementsPerOverlap, elementsPerGap)

        # Different method for the fast and slow windowing implementations
        if self._winSlow:
            winData = pd.concat(self._winDict)
            del self._winDict
            winData.index.set_names('window', level=0, inplace=True)
            winData.index = winData.index.droplevel(level='task_inst')
        else:
            # Merging the two windows columns together and removing periods of data with no window
            data['window1'] = np.concatenate(self._winIdxList, axis=None)
            data['window2'] = np.concatenate(self._winOverlapIdxList, axis=None)
            del self._winIdxList, self._winOverlapIdxList
            winData = pd.concat([data.drop('window2', axis=1, level=0).rename({'window1': 'window'}, axis='columns').reset_index(),
                                data.drop('window1', axis=1, level=0).rename({'window2': 'window'}, axis='columns').reset_index()], ignore_index=True)
            winData = winData.loc[winData['window'] != 0].set_index(['trial', 'task', 'window']).sort_index().drop(metricsDrop, axis=1, level=0)
        return winData.reorder_levels(['task', 'trial', 'window'])

    def window_function_slow(self, taskInst, elementsPerWindow, elementsPerOverlap, elementsPerGap):
        """
        Method for applying windows to the data in the form of an additional pandas index
        :param taskInst: pd.DataFrame[(PT/Task/Task Instance)] [Data equal to a single task instance to be windowed]
        :param elementsPerWindow
        :param elementsPerOverlap
        :param elementsPerGap
        """

        winStart = 0
        winStartOverlap = (winStart + elementsPerWindow) - elementsPerOverlap
        while (winStartOverlap + elementsPerWindow) < len(taskInst.index):
            winEnd = winStart + elementsPerWindow
            winEndOverlap = winStartOverlap + elementsPerWindow
            self._winDict[self._window] = taskInst.iloc[winStart:winEnd]
            self._winDict[self._window + 1] = taskInst.iloc[winStartOverlap:winEndOverlap]
            winStart = elementsPerGap + winEnd
            winStartOverlap = elementsPerGap + winEndOverlap
            self._window += 2

    def window_function_fast(self, taskInst, elementsPerWindow, elementsPerOverlap, elementsPerGap):
        """
        Method for applying windows to the data in the form of an additional pandas index
        :param taskInst: pd.DataFrame[(PT/Task/Task Instance)] [Data equal to a single task instance to be windowed]
        :param elementsPerWindow
        :param elementsPerOverlap
        :param elementsPerGap
        """

        # Define the number and start windows for norm and overlapping windows
        win = np.zeros(len(taskInst.index))
        winOverlap = np.zeros(len(taskInst.index))

        winStart = 0
        winStartOverlap = (winStart + elementsPerWindow) - elementsPerOverlap
        while (winStartOverlap + elementsPerWindow) < len(taskInst.index):
            winEnd = winStart + elementsPerWindow
            winEndOverlap = winStartOverlap + elementsPerWindow
            win[winStart:winEnd] = self._window
            winOverlap[winStartOverlap:winEndOverlap] = self._window+1
            winStart = elementsPerGap + winEnd
            winStartOverlap = elementsPerGap + winEndOverlap
            self._window += 2
        self._winIdxList.append(win)
        self._winOverlapIdxList.append(winOverlap)











