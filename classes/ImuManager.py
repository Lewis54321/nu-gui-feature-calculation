from abc import ABC, abstractmethod
import pandas as pd
import numpy as np
from itertools import compress
import math

from classes.FilterManager import FilterManager
from classes.Orientation.MadgwickAhrs import MadgwickAHRS
from classes.Orientation.Quaternion import Quaternion


class ImuManager(ABC):
    """
    Base class for all IMU types
    Implements the common methods of "filter_data" and "plot_data"
    """

    @abstractmethod
    def __init__(self, imuData, imuOrder, location,  firmware, adcFreq, imuFreq, pins, loc, orientation):
        """
        :param imuData: pd.DataFrame [This is None unless data for all IMUs was recorded in one csv file]
        :param imuOrder: Int [Number that increments from 0 to organise IMUs in a given trial]
        :param config: dict(String/ List) [Header data. Includes list of ADC pins for NUIMU Type]
        :param location: String [Unique location of the IMU given as side_limbLoc
        :param adcFreq
        :param imuFreq
        :param pins
        :param loc
        :param orientation
        :param firmware: Int
        """

        super().__init__()

        self._rawData = imuData
        self._order = imuOrder
        self._uniqueLoc = location
        self._adcFreq = adcFreq
        self._imuFreq = imuFreq
        self._adcPins = pins
        self._loc = loc
        self._orientation = orientation
        self._firmware = firmware

        # A reduced version of 'self._adcPins' which only includes the active pins which form the headings for 'self._adcData'
        self._activeAdcPins = list()

        # Standard keys for easy access to metrics
        self._mCol = ['mag_x', 'mag_y', 'mag_z']
        self._gCol = ['gyr_x', 'gyr_y', 'gyr_z']
        self._aCol = ['acc_x', 'acc_y', 'acc_z']

        # IMU specific properties
        self._imuMax = None
        self._adcMax = None

        # Calibration Data
        self._calData = dict()

        # How each task (as int) as it appears corresponds to its position in the list (as int) of all possible tasks
        self._taskKeyDict = dict()

        """
        These fields correspond to the most raw data that corresponds to this IMU as well as the same data after
        it has been cleaned to remove dropped packets etc
        """
        self._data = None
        self._cleanData = None

        """
        These fields correspond to "CleanData" after being processed by at least one of the following methods:
        "PartitionData", "CalibrateImus", "StandardiseUnits"
        """
        self._inertialData = None
        self._adcData = None
        self._orientData = None        # As extracted from the inertial data in "extract_orientation_data" method

    # LOCAL IMPORT METHODS
    def import_imu_data(self, headerLines=None, dataWidth=None, dataDir=None):
        """
        This method is available to every IMU for trial types which have one data file per IMU
        :param dataDir:
        :param headerLines:
        :param dataWidth:
        :param expMetrics:
        :param standMetrics:
        """

        # Read csv file in the case data has not already been read at the trial-scope
        if self._rawData is None:
            self._rawData = pd.read_csv(dataDir, header=headerLines, skip_blank_lines=False,
                                        usecols=range(dataWidth))

        # Converting the raw data from local to standard heading names
        self._rawData.rename(columns=self._transCol, inplace=True)

        # Reducing the list of expected and standard columns for the Trial/ IMU to those actually seen in the data
        self._sMetrics = list(compress(self._sMetrics, [metric in self._rawData.columns for metric in self._sMetrics]))
        self._sMeta = list(compress(self._sMeta, [heading in self._rawData.columns for heading in self._sMeta]))

        # Extract just the meta and data heading from the total DataFrame
        self._data = self._rawData.loc[:, self._sMeta + self._sMetrics]
        del self._rawData

    def clean_imu_data(self, timeMultiplier):
        """
        This method is available to every IMU for trial types which have one data file per IMU
        Index of inmported DataFrame represents order of the imu with the entire dataset and is not important
        :param timeMultiplier: Int [Multiplier to apply to the Time Stamp to convert it to milliseconds]
        """

        # Fill any na data (when sensor data was not recorded) with zeros
        self._data.fillna(value=0, inplace=True)

        # Converting time stamp from 10th ms to ms for ease of use
        if 'time_stamp' in self._sMeta:
            self._data['time_stamp'] = self._data['time_stamp'] * timeMultiplier

        # Remove packets where the data exceeds the buffer limits (10 bit for ADC and 15 bit for Inertial)
        if 'adc_1' in self._data.columns:
            self._data = self._data[(self._data[['adc_1', 'adc_2', 'adc_3']].abs() < self._adcMax).all(axis=1)]
        if 'mag_x' in self._data.columns:
            self._data = self._data[(self._data[['mag_x', 'mag_y', 'mag_z']].abs() < self._imuMax).all(axis=1)]
            self._data = self._data[(self._data[['gyr_x', 'gyr_y', 'gyr_z', 'acc_x', 'acc_y', 'acc_z']].abs() < self._imuMax).all(axis=1)]

        # In the case where timings are not assigned we expect to have a "task" and "task_stage" column which we
        # can use to further removed false packets and to organise the index
        if 'task_stage' in self._sMeta:
            # Remove packets where the task stage does not contain either a one or zero or task not valid value
            self._data = self._data[(self._data['task_stage'] == 0) | (self._data['task_stage'] == 1)]
            self._data = self._data[(self._data['task'] >= 0) & (self._data['task'] < 30)]
            # Replace the periods where no task is being performed with the 0 label and remove the 'task_stage' column
            self._data.loc[self._data.loc[:, 'task_stage'] == 0, 'task'] = 0
            self._data.drop('task_stage', axis=1, inplace=True)
            self._sMeta.remove('task_stage')

        self._cleanData = self._data.reset_index(drop=True)
        del self._data

    def make_tasks_unique(self):
        """
        In the event that tasks have been digitally assigned (and we don't trust the counter column), it is necessary
        to make each task unique so that they do not all get sorted in numerical order when the IMUs are concat together
        Also useful to check for the requirement that there are no duplicate tasks in the event of rating study
        An error will be thrown here if an IMU has empty dataframe for whatever reason
        """

        if 'task' not in self._sMeta:
            return

        self._cleanData['task_key'] = np.zeros(len(self._cleanData.index.values), dtype=int)
        taskChangeDiff = np.diff(self._cleanData['task'].values)
        taskChange = np.append(np.where(taskChangeDiff != 0)[0], len(self._cleanData.index))
        prevIdx = 0
        taskKey = 1
        # Index has been reset in previous step so loc should operate like iloc in this case
        for idx in taskChange+1:
            self._taskKeyDict[taskKey] = self._cleanData.loc[prevIdx, 'task']
            self._cleanData.loc[prevIdx: idx-1, 'task_key'] = int(taskKey)
            prevIdx = idx
            taskKey += 1
        self._sMeta.append('task_key')

    def partition_adc_imu_data(self):
        """
        Method to partition the inertial and ADC data into separate DataFrames
        """

        # Extracting the inertial data columns only and removing zero data (packets where only ADC data was recorded)
        nonAdcData = self._cleanData.loc[(self._cleanData[self._sInertialMetrics] != 0).any(axis=1)]
        self._inertialData = nonAdcData.reindex(self._sMeta + self._sInertialMetrics + self._sOrientMetrics, axis='columns')

        # Repeating for ADC data. Using header information to determine which ADC columns contain useful data
        adcPinsIdx = list()
        for enum, pin in enumerate(self._adcPins):
            if 'ignore' in pin:
                continue
            if ('MMG' in pin) or ('EMG' in pin) or ('Other' in pin):
                self._activeAdcPins.append(pin)
                adcPinsIdx.append(enum)
        if len(self._activeAdcPins) > 0:
            selectedPins = [self._sAdcMetrics[i] for i in adcPinsIdx]
            self._adcData = self._cleanData.reindex(self._sMeta + selectedPins, axis='columns')
            self._adcData.columns = self._sMeta + self._activeAdcPins
        del self._cleanData

    def continuous_counter(self):
        """
        Method for converting the overloading counter into a continuous counter
        Places NaN values in the data where dropped packets are found
        Should bear in mind that any extended period of dropped packets (more that 10 NaNs in a row is probable not worth interpolating
        since may not look like real data in that case)
        In the event that there is no Counter column in the data then a very approximate counter is found using the length of the data
        :return:
        """

        def calc_counter(data, packetRatio):

            counter = data['counter'].values
            counter = counter - counter[0]
            counterDiff = np.diff(counter)

            """
            Check if counter differential matches the expected differential for every packet
            If not there are two cases:
            (1) Negative differential suggests overload of the 8 bit packet counter (add 256 to rectify)
            (2) With or without rectification, the counter continues to be out of sync (i.e. the next packet is
                not at it's expected regular increment. In this case it means that multiple packet overloads have been
                missed and so rectification is performed until packets are regular again
            """
            counterDrop = np.where(counterDiff != packetRatio)[0]
            for dropIdx in counterDrop:
                if counterDiff[dropIdx] < 0:
                    counter[dropIdx + 1:] = counter[dropIdx + 1:] + self._counterRes + 1
                while (counter[dropIdx + 1] % packetRatio) != 0:
                    counter[dropIdx + 1:] = counter[dropIdx + 1:] + self._counterRes + 1

            # Reindex the data with correct counter (as if no packets were dropped) leaving NaN in the data
            # where packets were dropped
            # For some reason there are duplicated values and duplicated indices (counter values) which need to be
            # removed to avoid duplicate index error being thrown
            counterCorrect = np.arange(counter[0], counter[-1] + 1, int(packetRatio))
            data['counter'] = counter
            data.set_index('counter', inplace=True)
            data = data.loc[~data.index.duplicated(keep='first')]
            data = data.reindex(counterCorrect)
            data.reset_index(inplace=True)
            return data

        packetRatio = int(float(self._adcFreq) / float(self._imuFreq))

        if 'counter' not in self._sMeta:
            self._inertialData['counter'] = np.arange(len(self._inertialData.index)) * packetRatio
            if len(self._activeAdcPins) > 0:
                self._adcData['counter'] = np.arange(len(self._adcData.index))
            self._sMeta.append('counter')
            return

        self._inertialData = calc_counter(self._inertialData, packetRatio)
        if len(self._activeAdcPins) > 0:
            self._adcData = calc_counter(self._adcData, 1)

    def interpolate_missing_rows(self, interpNan):

        def pre_sample(data):
            """
            Method for interpolating the NaN data where packets have been dropped
            Interpolation will not work for tasks (don't want half tasks i.e. 2.5) so instead just copy these values forwards
            tasks will only be present here in the event of them being assigned (i.e. digital tasks rather than timed)
            :return:
            dataInterp: The interpolated version of the input data
            """

            if 'task' in self._sMeta:
                data['task'] = data['task'].fillna(method='ffill')
                data['task_key'] = data['task_key'].fillna(method='ffill')
            if self._sOrientMetrics:
                data[self._orientMetrics] = data[self._orientMetrics].fillna(method='ffill')
            dataInterp = data.interpolate()
            return dataInterp

        if interpNan:
            self._inertialData = pre_sample(self._inertialData)
            if len(self._activeAdcPins) > 0:
                self._adcData = pre_sample(self._adcData)
        else:
            self._inertialData.dropna(inplace=True)
            if len(self._activeAdcPins) > 0:
                self._adcData.dropna(inplace=True)

    def calc_time_stamp(self):
        """
        Method for calculating time stamp from the counter
        In the event that a datetime like time stamp is already included in the data this will be overwritten here
        Time stamp units are ms
        """

        # The inertial data will already have a counter number in ratio with ADC data so should apply adcFreq rather than imuFreq
        if ('counter' in self._sMeta):
            self._inertialData['time_stamp'] = self._inertialData['counter'] * int(1000 / float(self._adcFreq))
            if len(self._activeAdcPins) > 0:
                self._adcData['time_stamp'] = self._adcData['counter'] * int(1000 / float(self._adcFreq))
            if 'time_stamp' not in self._sMeta:
                self._sMeta.append('time_stamp')

    def extract_orientation_data(self):
        """
        Extract orientation from the raw data if it is contained
        """

        self._orientData = self._inertialData.loc[:, self._sMeta + self._sOrientMetrics]

    # LOCAL PRE-PROCESSING METHODS
    @abstractmethod
    def calibrate_data(self):
        """
        Method for calibrating the gyroscope, acceleration, and magnetometer of the recorded IMUs
        """
        pass

    @abstractmethod
    def standardise_units(self):
        pass

    @abstractmethod
    def rotate_to_standard_frame(self):
        """
        Method for rotating different IMUs into the standardised Madgwick axis frame for direct comparison and
        for the calculation of quaternions
        :return:
        """
        pass

    def flip_body_side(self):
        """
        Method for flipping the sensor axes (of the non-torso imus) of standardised axis frame in the case
        that sensor data was recorded on the left hand side, to simulate right hand side readings for all recording
        """

        if self._loc != 'Torso':
            gyroFlip = np.array([[1, 0, 0], [0, -1, 0], [0, 0, -1]])
            accFlip = np.array([[-1, 0, 0], [0, 1, 0], [0, 0, 1]])
            magFlip = np.array([[-1, 0, 0], [0, 1, 0], [0, 0, 1]])
            self._inertialData[self._gCol] = np.dot(gyroFlip, self._inertialData[self._gCol].values.T).T
            self._inertialData[self._aCol] = np.dot(accFlip, self._inertialData[self._aCol].values.T).T
            if 'mag_x' in self._inertialData.columns:
                self._inertialData[self._mCol] = np.dot(magFlip, self._inertialData[self._mCol].values.T).T

    def calc_quaternion(self):
        """
        Method to calculate the orientation of the IMU
        Must be performed on the IMU-scope since relies on contain magnetometer data and on the IMU frequency
        """

        orientData = list()
        if all(elem in self._sOrientMetrics for elem in ['quat_w', 'quat_x', 'quat_y', 'quat_z']):
            for index, row in self._orientData.iterrows():
                orientData.append(Quaternion(row[['quat_w', 'quat_x', 'quat_y', 'quat_z']]))
            self._orientData.drop(['quat_w', 'quat_x', 'quat_y', 'quat_z'], axis=1, inplace=True)
            self._sOrientMetrics = [elem for elem in self._sOrientMetrics if elem not in ['quat_w', 'quat_x', 'quat_y', 'quat_z']]

        elif all(elem in self._sOrientMetrics for elem in ['yaw', 'pitch', 'roll']):
            self._orientData.loc[:, self._sOrientMetrics] = self._orientData.loc[:, self._sOrientMetrics].transform(np.radians)
            for index, row in self._orientData.iterrows():
                orientData.append(Quaternion.from_euler_angle_zyz_tb_intrinsic(row['yaw'], row['pitch'], row['roll']))
            self._orientData.drop(['yaw', 'pitch', 'roll'], axis=1, inplace=True)
            self._sOrientMetrics = [elem for elem in self._sOrientMetrics if elem not in ['yaw', 'pitch', 'roll']]

        else:
            madgwickAHRS = MadgwickAHRS(samplePeriod=(1/float(self._imuFreq)), beta=0.5)
            if any(elem not in self._sInertialMetrics for elem in ['mag_x', 'mag_y', 'mag_z', 'fake']):
                for index, row in self._inertialData.iterrows():
                    madgwickAHRS.Update_imu(row[['gyr_x', 'gyr_y', 'gyr_z']], row[['acc_x', 'acc_y', 'acc_z']])
                    orientData.append(madgwickAHRS.quaternion)
            else:
                for index, row in self._inertialData.iterrows():
                    madgwickAHRS.Update(row[['gyr_x', 'gyr_y', 'gyr_z']], row[['acc_x', 'acc_y', 'acc_z']],
                                        row[['mag_x', 'mag_y', 'mag_z']])
                    orientData.append(madgwickAHRS.quaternion)

        self._orientData['quat'] = orientData
        self._sOrientMetrics.append('quat')

    # GLOBAL METHODS
    def filter_data(self, rawData, metricType, allFilterBounds, notch):
        """
        Method to filter the raw IMU and ADC data
        Filter has to be done in IMU-scope (rather than trial-scope) since relies on individual IMU frequency
        :param rawData: pd.DataFrame [Data to be filtered, columns selected from the total DataFrame]
        :param metricType: String ['mmg' or 'emg' or 'inertial']
        :param allFilterBounds: Dict(Dict(Int/ Bool)) [User set boundaries for filtering]
        :param notch: Bool [Whether or not to apply notch filtering to the data (only applicable for ADC data)]
        :return procData: pd.DataFrame [Filtered version of 'rawData']
        """

        # Getting the filter boundaries
        if 'gyr' in rawData.name:
            filterBounds = allFilterBounds['gyrBounds']
        elif 'acc' in rawData.name:
            filterBounds = allFilterBounds['accBounds']
        elif 'MMG' in rawData.name:
            filterBounds = allFilterBounds['mmgBounds']
        elif 'EMG' in rawData.name:
            filterBounds = allFilterBounds['emgBounds']

        # Creating the specs and boundaries variables for each metric to be filtered
        if metricType == 'inertial':
            filterSpecs = {'fSamp': self._imuFreq, 'order': 3}
        elif metricType == ('adc'):
            filterSpecs = {'fSamp': self._adcFreq, 'order': 3}
        notchFilterSpecs = {'fSamp': self._adcFreq, 'order': 4, 'filtType': 'bandstop'}

        # Update the filter type and the cutoff based on the combination of user boundary settings
        if (filterBounds['LF'] == 'None' and filterBounds['HF'] == 'None'):
            filterSpecs['filtType'] = None
        elif filterBounds['LF'] == 'None':
            filterSpecs['filtType'] = 'lowpass'
            filterSpecs['cutOff'] = int(filterBounds['HF'])
        elif filterBounds['HF'] == 'None':
            filterSpecs['filtType'] = 'highpass'
            filterSpecs['cutOff'] = int(filterBounds['LF'])
        else:
            filterSpecs['filtType'] = 'bandpass'
            filterSpecs['cutOff'] = np.array([int(filterBounds['LF']), int(filterBounds['HF'])])

        # Filtering the data using a butter-worth IIR filter
        if filterSpecs['filtType'] is not None:
            procData = FilterManager.ButterIirFilter(rawData, filterSpecs)
        else:
            procData = rawData

        # Application of a butter-stop filter applied at 50 Hz harmonics to remove noise
        if notch:
            for harmonic in range(1, 5):
                notchFilterSpecs['cutOff'] = np.array([(50 * harmonic) - 1, (50 * harmonic) + 1])
                procData = FilterManager.ButterIirFilter(procData, notchFilterSpecs)

        return procData

    # PROPERTIES
    @property
    def TaskKeyDict(self):
        return self._taskKeyDict

    @property
    def AdcRecorded(self):
        if len(self._activeAdcPins) > 0:
            return True
        return False

    @property
    def QuatRecorded(self):
        """
        If there is standard format quat data existing in the dataframe
        Quat data should be single cell format and not multicell (quat_w, quat_x, quat_y, quat_z format)
        :return quatBool:
        """
        if 'quat' in self._sOrientMetrics:
            return True
        return False

    def MergeInertialData(self, trialInertialFreq):
        """
        Properties which returns the DataFrame prepared for merging across the different IMUs recorded. 2 Cases are:
        (1) digitally marked data ('task' will be one of the columns of the data already but counter may not exist or be unreliable)
        (2) Timed data ('counter' will be one of the columns of data and will be accurate enough to assign tasks by later)
        :param trialInertialFreq: the lowest frequency imu across the whole trial
        """

        def norm_time(norm_data):
            norm_data['time_stamp'] = norm_data['time_stamp'] - norm_data['time_stamp'].min()
            return norm_data

        data = FilterManager.memory_reduce(self._inertialData)
        if int(trialInertialFreq) < int(self.InertialFreq):
            data = FilterManager.downsample(data, int(int(self._imuFreq) / int(trialInertialFreq)))
        del self._inertialData

        if 'task_key' in self._sMeta:
            data = data[self._gCol + self._aCol + ['task_key', 'time_stamp']].reset_index(drop=True)
            data = data.groupby(['task_key']).apply(norm_time)
            data = data.set_index(['task_key', 'time_stamp'])
        else:
            data = data[self._gCol + self._aCol + ['time_stamp']].reset_index(drop=True)
            data = data.set_index(['time_stamp'])
        return data

    def MergeAdcData(self, trialAdcFreq):
        """
        Properties which returns the DataFrame prepared for merging across the different IMUs recorded. 2 Cases are:
        (1) digitally marked data ('task' will be one of the columns of the data already but counter may not exist or be unreliable)
        (2) Timed data ('counter' will be one of the columns of data and will be accurate enough to assign tasks by later)
        """

        def norm_time(norm_data):
            norm_data['time_stamp'] = norm_data['time_stamp'] - norm_data['time_stamp'].min()
            return norm_data

        if not self.AdcRecorded or trialAdcFreq is None:
            return None

        data = FilterManager.memory_reduce(self._adcData)
        if int(trialAdcFreq) < int(self.AdcFreq):
            data = FilterManager.downsample(data, int(int(self._adcFreq) / int(trialAdcFreq)))
        del self._adcData

        if 'task_key' in self._sMeta:
            data = data[self._activeAdcPins + ['task_key', 'time_stamp']].reset_index(drop=True)
            data = data.groupby(['task_key']).apply(norm_time)
            data = data.set_index(['task_key', 'time_stamp'])
        else:
            data = data[self._activeAdcPins + ['time_stamp']].reset_index(drop=True)
            data = data.set_index(['time_stamp'])
        return data

    def MergeQuatData(self, trialInertialFreq):

        def norm_time(norm_data):
            norm_data['time_stamp'] = norm_data['time_stamp'] - norm_data['time_stamp'].min()
            return norm_data

        if not self.QuatRecorded:
            return None

        data = self._orientData
        if int(trialInertialFreq) < int(self.InertialFreq):
            data = FilterManager.downsample(data, int(int(self._imuFreq) / int(trialInertialFreq)))
        del self._orientData

        if 'task_key' in self._sMeta:
            data = data[self._sOrientMetrics + ['task_key', 'time_stamp']].reset_index(drop=True)
            data = data.groupby(['task_key']).apply(norm_time)
            data = data.set_index(['task_key', 'time_stamp'])
        else:
            data = data[self._sOrientMetrics + ['time_stamp']].reset_index(drop=True)
            data = data.set_index(['time_stamp'])
        return data

    @property
    def InertialFreq(self):
        return int(self._imuFreq)

    @property
    def AdcFreq(self):
        if not self.AdcRecorded:
            return None
        return int(self._adcFreq)


class NuImuManager(ImuManager):
    """
    Inherits from "ImuManager" base class. Contains low level methods for importing and processing data
    """

    def __init__(self, imuOrder, config, sTrialMeta, sTrialMetrics, transCol, location, firmware=None, imuData=None):
        """
        :param imuOrder: Int [Number that increments from 0 to organise IMUs in a given trial]
        :param config: dict(String/ List) [Header data. Includes list of ADC pins]
        :param eTrialMeta: List(String) [list of meta headings the GUI can record in non-standard format]
        :param sTrialMeta: List(String) [list of meta headings the GUI can record in standard format]
        :param eTrialMetrics: List(String) [list of metric headings the GUI can record in non-standard format]
        :param sTrialMetrics: List(String) [list of metric headings the GUI can record in standard format]
        :param location: String [The unique location (side_location) of the imu]
        :param firmware: Int [Firmware number of the IMU. Default is none since trial may not provided this information]
        :param imuData: pd.DataFrame [The dataframe of imu data. This is provided if one csv file per trial (since data
                                      imported already in 'import_trial_data'.
                                      If one csv file per imu this is not provided until later in 'import_imu_data']
        """

        # These are the headings that it is possible for the IMU to record (standard format)
        localImuHeadings = ['gyr_x', 'gyr_y', 'gyr_z', 'mag_x', 'mag_y', 'mag_z', 'acc_x', 'acc_y', 'acc_z']
        localAdcHeadings = ['adc_1', 'adc_2', 'adc_3', 'adc_4', 'adc_5', 'adc_6', 'adc_7', 'adc_8']
        localOrientHeadings = ['quat_w', 'quat_x', 'quat_y', 'quat_z']
        localMetaHeadings = ['time_stamp', 'counter', 'imu_num', 'task', 'task_stage']

        # Finding the overlap in standard headings locally (per imu) and globally (per trial data file/s)
        self._sInertialMetrics = list(compress(sTrialMetrics, [metric in localImuHeadings for metric in sTrialMetrics]))
        self._sAdcMetrics = list(compress(sTrialMetrics, [metric in localAdcHeadings for metric in sTrialMetrics]))
        self._sOrientMetrics = list(compress(sTrialMetrics, [metric in localOrientHeadings for metric in sTrialMetrics]))

        # Combined meta and data headings (required for data importing for multi-file trial data)
        self._sMeta = list(compress(sTrialMeta, [metric in localMetaHeadings for metric in sTrialMeta]))
        self._sMetrics = self._sAdcMetrics + self._sInertialMetrics + self._sOrientMetrics

        # Global trans columns is a dict of all possible local headings converted to the standard equivalent
        self._transCol = transCol

        super().__init__(imuData, imuOrder, location, firmware, **config)

        # Maximum value recorded from the sensors
        self._imuMax = 32767
        self._adcMax = 1023
        self._counterRes = 255

    # LOCAL PRE-PROCESSING METHODS
    def calibrate_data(self, calData):
        """
        Method for calibrating the gyroscope, acceleration, and magnetometer of the recorded IMUs
        """

        if 'mag_x' in self._inertialData.columns and 'mag' in calData.keys():
            self._inertialData[self._mCol] = self._inertialData[self._mCol] - calData['mag'][0:3]
            self._inertialData[self._mCol] = self._inertialData[self._mCol] * calData['mag'][3:]

        if 'gyr' in calData.keys():
            self._inertialData[self._gCol] = self._inertialData[self._gCol] - calData['gyr']

        if 'acc' in calData.keys():
            self._inertialData[self._aCol] = self._inertialData[self._aCol] - calData['acc']

    def standardise_units(self):
        """
        Method for converting the raw saved data to standard units (overwrites existing fields)
        Gyro converted to radians per second, mag converted to something, acc converted to units of gravity
        :return:
        """

        if 'mag_x' in self._inertialData.columns:
            # Invert the z axis of the magnetometer so it is in the same left handed axis frame as other inertial metrics
            self._inertialData['mag_z'] = -self._inertialData['mag_z']
            self._inertialData[self._mCol] = self._inertialData[self._mCol].apply(lambda x: x * (2 / float(self._imuMax)))
        self._inertialData[self._gCol] = self._inertialData[self._gCol].apply(lambda x: np.radians(x * 500 / float(self._imuMax)))
        self._inertialData[self._aCol] = self._inertialData[self._aCol].apply(lambda x: x * (2 / float(self._imuMax)))
        if len(self._activeAdcPins) > 0:
            self._adcData[self._activeAdcPins] = self._adcData[self._activeAdcPins].apply(lambda x: x * (3.3 / float(self._adcMax)))

    def rotate_to_standard_frame(self):
        """
        Method for rotating the recording IMUs to simulate them all facing a downwards direction
        :return:
        """

        def ApplyRotationMatrix(data, theta, axis):
            """
            Method for applying a rotation matrix to the inertial data
            :param data: inertial dataframe containing gyro, mag, and acc data
            :param theta: angle to rotate by
            :param axis: axis to rotate around (x, y, z)
            :return:
            """

            theta = math.radians(theta)
            if axis == 1:
                rotMatrix = np.array([[1, 0, 0], [0, np.cos(theta), -np.sin(theta)], [0, np.sin(theta), np.cos(theta)]])
            elif axis == 2:
                rotMatrix = np.array([[np.cos(theta), 0, np.sin(theta)], [0, 1, 0], [-np.sin(theta), 0, np.cos(theta)]])
            elif axis == 3:
                rotMatrix = np.array([[np.cos(theta), -np.sin(theta), 0], [np.sin(theta), np.cos(theta), 0], [0, 0, 1]])

            if 'mag_x' in data.columns:
                data[self._mCol] = np.dot(rotMatrix, data[self._mCol].values.T).T
            data[self._gCol] = np.dot(rotMatrix, data[self._gCol].values.T).T
            data[self._aCol] = np.dot(rotMatrix, data[self._aCol].values.T).T

            return data

        # Rotating from the sensor axis frame to the Madgwick expected axis frame (x and z axis effectively inverted)
        self._inertialData = ApplyRotationMatrix(self._inertialData, 180, 2)

        # Rotating the axis frame for the y axis to be pointing parallel to the gravity vector (rather than orthogonal)
        if self._orientation == 'Downwards':
            self._inertialData = ApplyRotationMatrix(self._inertialData, -90, 1)
        elif self._orientation == 'Upwards':
            self._inertialData = ApplyRotationMatrix(self._inertialData, 90, 1)
        elif self._orientation == 'Left':
            self._inertialData = ApplyRotationMatrix(self._inertialData, 90, 2)
        elif self._orientation == 'Right':
            self._inertialData = ApplyRotationMatrix(self._inertialData, -90, 2)


class AppleWatchManager(ImuManager):
    """
    Inherits from "ImuManager" base class. Contains low level methods for importing and processing data
    Methods are broadly same as "NuImuManager" except does not contain magnetometer  or ADC data
    """

    def __init__(self, imuOrder, config, eTrialMeta, sTrialMeta, eTrialMetrics, sTrialMetrics,
                 location, firmware=None, imuData=None):
        """
        :param imuOrder: Int [Number that increments from 0 to organise IMUs in a given trial]
        :param config: dict(String/ List) [Header data. Includes list of ADC pins]
        :param eTrialMeta: List(String) [list of meta headings the GUI can record in non-standard format]
        :param sTrialMeta: List(String) [list of meta headings the GUI can record in standard format]
        :param eTrialMetrics: List(String) [list of metric headings the GUI can record in non-standard format]
        :param sTrialMetrics: List(String) [list of metric headings the GUI can record in standard format]
        :param location: String [The unique location (side_location) of the imu]
        :param firmware: Int [Firmware number of the IMU. Default is none since trial may not provided this information]
        :param imuData: pd.DataFrame [The dataframe of imu data. This is provided if one csv file per trial (since data
                                      imported already in 'import_trial_data'.
                                      If one csv file per imu this is not provided until later in 'import_imu_data']
        """

        # These are the headings that it is possible for the IMU to record (standard format)
        localImuHeadings = ['gyr_x', 'gyr_y', 'gyr_z', 'acc_x', 'acc_y', 'acc_z']
        localAdcHeadings = []
        localOrientHeadings = []
        localMetaHeadings = ['time_stamp', 'imu_num', 'task', 'task_stage']

        # Finding the overlap in standard headings locally (imu) and globally (gui) and using those to extract the
        # expected and standard headings
        eInertialMetrics = list(compress(eTrialMetrics, [metric in localImuHeadings for metric in sTrialMetrics]))
        self._sInertialMetrics = list(compress(sTrialMetrics, [metric in localImuHeadings for metric in sTrialMetrics]))

        eAdcMetrics = list(compress(eTrialMetrics, [metric in localAdcHeadings for metric in sTrialMetrics]))
        self._sAdcMetrics = list(compress(sTrialMetrics, [metric in localAdcHeadings for metric in sTrialMetrics]))

        eOrientMetrics = list(compress(eTrialMetrics, [metric in localOrientHeadings for metric in sTrialMetrics]))
        self._sOrientMetrics = list(compress(sTrialMetrics, [metric in localOrientHeadings for metric in sTrialMetrics]))

        self._eMeta = list(compress(eTrialMeta, [metric in localMetaHeadings for metric in sTrialMeta]))
        self._sMeta = list(compress(sTrialMeta, [metric in localMetaHeadings for metric in sTrialMeta]))

        # Combined data headings
        self._eMetrics = eAdcMetrics + eInertialMetrics + eOrientMetrics
        self._sMetrics = self._sAdcMetrics + self._sInertialMetrics

        super().__init__(imuData, imuOrder, location, firmware, **config)

    # LOCAL PRE-PROCESSING METHODS
    def calibrate_data(self):
        """
        Method for calibrating the gyroscope, acceleration, and magnetometer of the recorded IMUs
        """
        pass

    def standardise_units(self):
        pass

    def rotate_to_standard_frame(self):
        pass


class XsensManager(ImuManager):
    """
    Inherits from "ImuManager" base class. Contains low level methods for importing and processing data
    Methods are broadly same as "NuImuManager" except does not contain magnetometer  or ADC data
    """

    def __init__(self, imuOrder, config, eTrialMeta, sTrialMeta, eTrialMetrics, sTrialMetrics,
                 location, firmware=None, imuData=None):
        """
        :param imuOrder: Int [Number that increments from 0 to organise IMUs in a given trial]
        :param config: dict(String/ List) [Header data. Includes list of ADC pins]
        :param eTrialMeta: List(String) [list of meta headings the GUI can record in non-standard format]
        :param sTrialMeta: List(String) [list of meta headings the GUI can record in standard format]
        :param eTrialMetrics: List(String) [list of metric headings the GUI can record in non-standard format]
        :param sTrialMetrics: List(String) [list of metric headings the GUI can record in standard format]
        :param location: String [The unique location (side_location) of the imu]
        :param firmware: Int [Firmware number of the IMU. Default is none since trial may not provided this information]
        :param imuData: pd.DataFrame [The dataframe of imu data. This is provided if one csv file per trial (since data
                                      imported already in 'import_trial_data'.
                                      If one csv file per imu this is not provided until later in 'import_imu_data']
        """

        # These are the headings that it is possible for the IMU to record (standard format)
        localImuHeadings = ['gyr_x', 'gyr_y', 'gyr_z', 'acc_x', 'acc_y', 'acc_z']
        localAdcHeadings = []
        localOrientHeadings = ['roll', 'pitch', 'yaw']
        localMetaHeadings = ['time_stamp', 'counter', 'imu_num', 'task', 'task_stage']

        # Finding the overlap in standard headings locally (imu) and globally (gui) and using those to extract the
        # expected and standard headings
        eInertialMetrics = list(compress(eTrialMetrics, [metric in localImuHeadings for metric in sTrialMetrics]))
        self._sInertialMetrics = list(compress(sTrialMetrics, [metric in localImuHeadings for metric in sTrialMetrics]))

        eAdcMetrics = list(compress(eTrialMetrics, [metric in localAdcHeadings for metric in sTrialMetrics]))
        self._sAdcMetrics = list(compress(sTrialMetrics, [metric in localAdcHeadings for metric in sTrialMetrics]))

        eOrientMetrics = list(compress(eTrialMetrics, [metric in localOrientHeadings for metric in sTrialMetrics]))
        self._sOrientMetrics = list(compress(sTrialMetrics, [metric in localOrientHeadings for metric in sTrialMetrics]))

        self._eMeta = list(compress(eTrialMeta, [metric in localMetaHeadings for metric in sTrialMeta]))
        self._sMeta = list(compress(sTrialMeta, [metric in localMetaHeadings for metric in sTrialMeta]))

        # Combined data headings
        self._eMetrics = eAdcMetrics + eInertialMetrics + eOrientMetrics
        self._sMetrics = self._sAdcMetrics + self._sInertialMetrics + self._sOrientMetrics

        super().__init__(imuData, imuOrder, location, firmware, **config)

        # Maximum value recorded from the sensors
        self._imuMax = 32767
        self._adcMax = 1023
        self._counterRes = 65535

    # LOCAL PRE-PROCESSING METHODS
    def calibrate_data(self):
        """
        Method for calibrating the gyroscope, acceleration, and magnetometer of the recorded IMUs
        """
        pass

    def standardise_units(self):
        pass

    def rotate_to_standard_frame(self):
        pass


