from abc import ABC, abstractmethod
import os
import re

from classes.TrialManager import ClinicalBlueTrialManager
from classes.TrialManager import ActivityBlueTrialManager


class FileManager(ABC):
    """
    This is the base class of all file manager type classes
    Inherits from the abstract base class
    """

    @abstractmethod
    def __init__(self):

        super().__init__()

        # Bool variable assigns whether or not a label file is also provided with the data file
        self._labelRec = False

    @abstractmethod
    def manage_data_dirs(self, dataDirs):
        """
        Method for managing all the user selected data file directory paths
        :param dataDirs: String [directory of the data files]
        :return:
        """
        pass

    @abstractmethod
    def manage_label_dirs(self, labelDirs):
        """
        Method for managing all the user selected label file directory paths
        :param labelDirs: String [directory of the label files]
        :return:
        """
        pass

    @abstractmethod
    def gen_trials(self):
        """
        Method for generating trials (one per subject tested) by providing the corresponding data and label files
        to the object
        :return:
        """
        pass


class BlueFileManager(FileManager):
    """
    Class inherits from the base class file managed for the importing of data and labels files
    This subclass is specialised for data files saved using the "BlueGui" user interface
    """

    def __init__(self):

        super().__init__()

        self._dataFileDict = dict()
        self._taskList = None

    # override
    def manage_data_dirs(self, dataDirs):
        """
        Method for extracting the patient information from the filename
        Test trials (not on patients) are assigned a zero for number and patient UID
        :param dataDirs: The directory path for each data file to be imported
        :return:
        """

        for dataDir in dataDirs:

            fileName = str(os.path.basename(dataDir))

            # Find the patient number and UID from the filename
            try:
                uid = re.findall(r'UID(\d+)\)', fileName)[0]
                initials = re.findall(r'__(\w+)__', fileName)[0]
            except:
                uid = '0'
                initials = 'AA'

            self._dataFileDict[uid] = {'uid': uid, 'filepath': dataDir, 'initials': initials}

    @abstractmethod
    def manage_label_dirs(self, labelDirs):
        pass

    @abstractmethod
    def gen_trials(self, labelRec):
        pass

    @property
    def TaskList(self):
        return self._taskList

    @property
    def LabelRec(self):
        return self._labelRec


class ClinicalBlueFileManager(BlueFileManager):
    """
    Class inherits from the base class file managed for the importing of data and labels files
    This subclass is specialised for data files saved using the "BlueGui" user interface
    """

    def __init__(self):

        super().__init__()

        self._labelFileDict = dict()

    # override
    def manage_label_dirs(self, labelDirs):
        """
        Method for extracting the patient number from the label filename
        :param labelDirs: The directory path for each label file to be imported
        :return:
        """

        for dataDir in labelDirs:

            fileName = str(os.path.basename(dataDir))
            uid = re.findall(r'UID(\d+)\)', fileName)[0]
            self._labelFileDict[uid] = {'filepath': dataDir}

        self._labelRec = True

    # override
    def gen_trials(self):
        """
        Method for generating a new trial (TrialManager) instance for each trial file imported
        Matches the appropriate label file to the data file using the extracted patient numbers
        :param labelRec:
        :return:
        """

        # Read text file containing all possible tasks
        taskFile = open('task lists/fma_full.txt', 'r')
        self._taskList = taskFile.read().split(',')
        trialDict = dict()

        for key in self._dataFileDict:

            if self._labelRec:
                self._labelRec = True
                try:
                    labelFile = self._labelFileDict[key]
                    labelPath = labelFile['filepath']
                except BaseException:
                    raise Exception('Error, could not find label file associated with patient no %s' % key)
            else:
                labelPath = None

            uid = key
            dataPath = self._dataFileDict[key]['filepath']

            trialDict[key] = ClinicalBlueTrialManager(uid, self._labelRec, self._taskList)

        return trialDict, dataPath, labelPath


class ActivityBlueFileManager(BlueFileManager):
    """
    Class inherits from the base class file managed for the importing of data and labels files
    This subclass is specialised for data files saved using the "BlueGui" user interface
    """

    def __init__(self):

        super().__init__()

        self._labelFile = str()

    # override
    def manage_label_dirs(self, labelDir):
        """
        Method for extracting the patient number from the label filename
        :param labelDirs: The directory path for each label file to be imported
        :return:
        """

        self._labelFile = str(labelDir[0])
        self._labelRec = True

    # override
    def gen_trials(self):
        """
        Method for generating a new trial (TrialManager) instance for each trial file imported
        Matches the appropriate label file to the data file using the extracted patient numbers
        :return:
        """

        # Read text file containing all possible tasks
        taskFile = open('task lists/activity.txt', 'r')
        self._taskList = taskFile.read().split(',')
        trialDict = dict()

        if not self._labelRec:
            raise Exception('Error: Could not find label containing the activities which correspond to the data files')

        for key in self._dataFileDict:
            uid = key
            dataPath = self._dataFileDict[key]['filepath']
            trialDict[key] = ActivityBlueTrialManager(uid, dataPath, self._labelFile, self._labelRec, self._taskList)

        return trialDict








































