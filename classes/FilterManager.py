import pandas as pd
import numpy as np
from scipy import signal


class FilterManager:
    """
    Static class which provides methods for Butterworth filtering and coverting to the frequency domain
    """

    @staticmethod
    def ButterIirFilter(data, filterSpecs):
        """
        Method used to apply Butterworth IIR Filter to the data
        :param data: pd.DataFrame/ array-like [Data to be filtered]
        :param filterSpecs: dict(String) [Dict of the necessary filter settings]
        :return dataOut: pd.DataFrame/ array-like [Filtered data]
        """

        nyq = 0.5 * int(filterSpecs['fSamp'])
        normalCutOff = filterSpecs['cutOff'] / nyq

        b, a = signal.butter(filterSpecs['order'], normalCutOff, btype=filterSpecs['filtType'], analog=False)

        if isinstance(data, pd.DataFrame):
            dataOut = data.apply(FilterManager.wrapperFiltFiltFunc, args=(b, a))
        else:
            if (3 * max(len(a), len(b))) > len(data):
                dataOut = signal.filtfilt(b, a, data.values, axis=-1, padlen=len(data) - 1)
            else:
                dataOut = signal.filtfilt(b, a, data, axis=-1)
        return dataOut

    @staticmethod
    def wrapperFiltFiltFunc(data, b, a):
        """
        Wrapper function created to fix the problem of apply method using pd.Series of pd.DataFrame as first argument
        To use the 'signal.filtfilt' function we want the pd.Series data provided as the final argument
        :param data: pd.Series
        :param b: np.Array(Float) [Butterworth parameter]
        :param a: np.Array(Float) [Butterworth parameter]
        :return:
        """

        # By default filtfilt requires length of data > padlen (where padlen by default equals 3 * max(len(a), len(b))
        # This section fixes this by adding a custom padlen in this circumstance
        if (3 * max(len(a), len(b))) > len(data):
            dataOut = signal.filtfilt(b, a, data.values, axis=-1, padlen=len(data)-1)   # This will throw false error if SciPy version < 1.2.0
        else:
            dataOut = signal.filtfilt(b, a, data.values, axis=-1)

        return dataOut

    @staticmethod
    def standardise_time_series(data, freq, append):
        """
        Method for adding a time bins index to a temporally ordered pd.DataFrame or array-like data
        :param data: pd.DataFrame/ array-like data type [input data]
        :param freq: Int [sample frequency used to calculate real-time independent axis]
        :param append: Bool [Whether bins should be appended to rather than replacing existing index/indices]
        :return: pd.DataFrame [data with time bins index]
        """

        data = pd.DataFrame(data)
        bins = np.arange(0, len(data.index)) / float(freq)
        if append:
            data.set_index(bins, append=True, inplace=True)
            data.index.set_names([name for name in data.index.names if name is not None] + ['bins'], inplace=True)
            return data
        else:
            return data.set_index(bins)

    @staticmethod
    def fast_fourier_transform(data, freq):
        """
        Method of FFT for converting time series to freq series data (data index becomes frequency bins)
        :param data: pd.DataFrame or array-like data type [input data]
        :param freq: Int [sample frequency used to calculate real-time independent axis]
        :return: pd.DataFrame [Regularised freq series data with freq bins index]
        """

        def fourier_transform(data_loc):
            """
            Converting time series data to freq series data for every column
            Freq bins may be calculated once since will be the same for each metric
            :param data_loc:
            :return transData:
            """
            # Initial filtering stage to remove DC offset from the signal to avoid peak there in transformed data
            filterSpecs = {'filtType': 'highpass', 'fSamp': freq, 'order': 4, 'cutOff': 0.1}
            dataOffsetRem = FilterManager.ButterIirFilter(data_loc, filterSpecs)
            # Using FFT to get the amplitude and bins of the spectrum in the frequency domain
            # Frequency response is symmetric (-ve and +ve frequencies) so only use half
            amplitude = np.absolute(np.fft.fft(dataOffsetRem))
            idx = int(len(amplitude) / 2)
            return amplitude[:idx]

        data = pd.DataFrame(data)
        freqData = data.apply(fourier_transform)
        freqSeries = np.fft.fftfreq(len(data.index), 1 / float(freq))[:int(len(freqData.index))]
        return freqData.set_index(freqSeries)

    @staticmethod
    def memory_reduce(data):
        """
        Method for reducing the memory requirement for each column of DataFrame
        :param data: pd.DataFrame
        :return data: pd.DataFrame
        """

        for col in data.columns:
            if data[col].dtype == 'float64':
                data[col] = data[col].astype('float32')
            if data[col].dtype == 'int64':
                data[col] = data[col].astype('int32')
        return data

    @staticmethod
    def downsample(data, ratio):
        """
        Method for downsampling pandas dataframe by a ratio
        :param data: pandas.DataFrame [dataframe to have rows downsampled]
        :param ratio: int [factor to downsample the data by]
        :return data: pd.DataFrame
        """

        data = data.iloc[::ratio, :]
        return data
