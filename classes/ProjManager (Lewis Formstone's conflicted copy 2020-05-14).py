from abc import ABC, abstractmethod
import os
import re
import sys
import numpy as np
import pandas as pd
from classes.Orientation.Quaternion import Quaternion
from classes.Orientation.Orientation import Orientation
from classes.Orientation.MultiQuaternion import MultiQuaternion
from sqlalchemy import create_engine
from PyQt5 import QtCore
from PyQt5.QtCore import QTimer
from classes.PlotManager import PlotManager


class ProjManager(ABC):
    """
    Base Class for all project types
    Implements several import methods since these are expected to be the same regardless of project type
    Does not implement feature calculation since some project types require windowing prior to feature calculation
    """

    def __init__(self, assignTimings, trialClass, imuClass, subjectIden):
        """
        One ProjManager instance created at runtime regardless of number of trials selected since
        all trials must be from the same project  type
        This abstract base class is inherited by all other project class types
        :param assignTimings: Bool [Whether the project has a separate timings file which must be assigned to the data]
        :param trialClass: Pointer [Points to inherited class from TrialManager]
        :param imuClass: Pointer [Points to inherited class from ImuManager]
        """

        super().__init__()

        def global_time():
            self._globalTime += 0.1

        self._assignTimings = assignTimings
        self._trialClass = trialClass
        self._imuClass = imuClass
        self._subjectIden = subjectIden

        # Timer
        self._globalTimer = QTimer()
        self._globalTimer.timeout.connect(global_time)
        self._globalTimer.start(100)
        self._globalTime = float(0)

        # Collection of all the possible tasks that could have been recorded for this project (provided externally)
        self._allTasks = list()

        # Dict for each trial recorded
        self._trialDict = dict()

        # Details of the database which are sent from the main thread in case of memory exception when merging trials
        self.DbDetails = dict()

        # Main data containers
        self._unfiltInertialDf, self._filtInertialDf = None, None
        self._unfiltAdcDf, self._filtAdcDf = None, None
        self._quatDf, self._orientDf = None, None

        # The location headings for the global DataFrames
        # Each location key contains a list composed of the two body location the location is made up of
        self._ratioLocs = dict()
        self._jointLocs = dict()

        # Standard project metric headings
        # Inertial
        self._sAccMetric = ['acc_x', 'acc_y', 'acc_z']
        self._sAccMagMetric = ['acc_mag']
        self._sGyrMetric = ['gyr_x', 'gyr_y', 'gyr_z']
        self._sGyrMagMetric = ['gyr_mag']
        self._sJerkMetric = ['jerk_x', 'jerk_y', 'jerk_z']
        self._sJerkMagMetric = ['jerk_mag']
        # Orientation
        self._sJointAngMetric = ['joint_x', 'joint_y', 'joint_z']
        self._sJointAngMagMetric = ['joint_mag']
        self._sJointPlaneMetric = ['plane_x', 'plane_y', 'plane_z']

        # Project Frequencies
        self._inertialFreq = None
        self._adcFreq = None

        self._magInertial = False

    # IMPORT METHODS
    @staticmethod
    def import_proj_tasks(taskDir):
        """
        Method for reading in the text file which contains all possible tasks for this project type
        :param taskDir:
        :return
        TaskFile
        """

        taskData = open(taskDir, 'r').read().splitlines()
        taskDict = dict(zip(map(int, taskData[0].split(",")), taskData[1].split(",")))
        return taskDict

    @staticmethod
    def import_proj_details(descDir):
        """
        Method for reading in the csv file which should contain the following project information for each IMU:
        (1) data directory (2) project uid (3) subject initials (4) imu order (5) imu number (6) imu location
        :param descDir:
        :return
        descFile
        """
        descData = pd.read_csv(descDir)
        return descData

    @staticmethod
    def organise_dirs(dataDirs, projDetails, subjectIden, **baseDirs):

        def manage_data_dirs(projDetails_l, dataDirs_l):
            """
            Method for extracting the patient information from the filename
            Test trials (not on patients) are assigned a zero for number and patient UID
            :param projDetails_l: pd.DataFrame [Local scope version of field in "organise_dirs"]
            :param dataDirs_l: List(String) [Local scope version of field in "organise_dirs"]
            :return: subProjDetails_l: pd.DataFrame [local scope DataFrame containing details and directories for the subjects analysed in this project]
            """

            # Create empty DataFrame with matching columns to the descData DataFrame initially
            expParameters = ['project_uid', 'subject_initials', 'imu_order', 'imu_num', 'imu_side',
                             'imu_loc', 'imu_orientation', 'affected_side']
            imuList = list()
            projDetails_l.set_index('data_filename', inplace=True)

            # For each user selected directory, check if a matching directory is stored in the list of project details
            # If this is the case then get all the details for this directory from the project details
            # If the user selected directory is not found in project details then assume this is a test file
            for dataDir in dataDirs_l:
                fileName = str(os.path.splitext(os.path.basename(dataDir))[0])
                dirData = projDetails_l.loc[[fileName]]
                numImus = len(dirData.index)
                for imu in range(numImus):
                    imuDetails = pd.Series(index=['data_dir'] + expParameters)
                    imuDetails['data_dir'] = dataDir
                    for expParameter in expParameters:
                        try:
                            imuDetails[expParameter] = dirData[expParameter].iloc[imu]
                        except BaseException as exception:
                            raise Exception(
                                'Missing expected parameter from the project details files: %s' % expParameter)
                    imuList.append(imuDetails)

            subProjDetails = pd.DataFrame(imuList)
            subProjDetails['unique_loc'] = subProjDetails['imu_side'].str.cat(subProjDetails['imu_loc'], sep='_')
            return subProjDetails.set_index(['project_uid', 'unique_loc'])

        def manage_cal_basedir(subProjDetails_l, calBaseDir, subjectIden_l):
            """
            Method for organising the calibration files from their base directory
            :param subProjDetails_l: pd.DataFrame [Local scope version of field declared in "manage_data_dirs"]
            :param calBaseDir: String [Base directory of the calibration files]
            :param subjectIden_l: String [Local scope version of field declared in "organise_dirs"]
            :return: subProjDetails_l: pd.DataFrame [See above]
            """

            allDirs = [os.path.join(calBaseDir, dir) for dir in os.listdir(calBaseDir)]
            # for uid in subProjDetails_l.drop(0, level=0).index:
            for uid in subProjDetails_l.index:
                try:
                    subProjDetails_l.loc[uid, 'mag_dir'] = [dir for dir in allDirs if "TRIALS" in dir and "MAG" in dir and
                                                            str(int(subProjDetails_l.loc[uid, 'imu_num'])) in dir][0]
                except IndexError:
                    subProjDetails_l.loc[uid, 'mag_dir'] = None
                try:
                    subProjDetails_l.loc[uid, 'gyr_dir'] = [dir for dir in allDirs if "ALL" in dir and "GYR" in dir and
                                                        str(int(subProjDetails_l.loc[uid, 'imu_num'])) in dir][0]
                except IndexError:
                    subProjDetails_l.loc[uid, 'gyr_dir'] = None
                try:
                    subProjDetails_l.loc[uid, 'acc_dir'] = [dir for dir in allDirs if "ALL" in dir and "ACC" in dir and
                                                        str(int(subProjDetails_l.loc[uid, 'imu_num'])) in dir][0]
                except IndexError:
                    subProjDetails_l.loc[uid, 'acc_dir'] = None

            return subProjDetails_l

        def manage_basedir(subProjDetails_l, baseDirName, baseDir, subjectIden_l):
            """
            Method for organising the other base directories (rating and timings) into the collected DataFrame
            :param subProjDetails_l: pd.DataFrame [Local scope version of field declared in "manage_data_dirs"]
            :param baseDirName: String [Name assigned to the metric directory]
            :param baseDir: String [The base directory path for a directory which contains files of a certain format]
            :param subjectIden_l: String [Local scope version of field declared in "organise_dirs"]
            :return: subProjDetails_l: pd.DataFrame [See above]
            """

            allDirs = [os.path.join(baseDir, filename) for filename in os.listdir(baseDir) if filename.endswith('csv')]
            for iden in subProjDetails_l.index:
                try:
                    subProjDetails_l.loc[iden, baseDirName] = \
                    [dir for dir in allDirs if subjectIden_l + "{0:0=3d}".format(iden[0]) in dir][0]
                except IndexError as exception:
                    raise Exception('No files for %s found' % baseDirName)

            return subProjDetails_l

        subProjDetails = manage_data_dirs(projDetails, dataDirs)
        for baseDirKey in baseDirs:
            if baseDirKey == 'cal_dir':
                subProjDetails = manage_cal_basedir(subProjDetails, baseDirs['cal_dir'].text(), subjectIden)
            else:
                subProjDetails = manage_basedir(subProjDetails, baseDirKey, baseDirs[baseDirKey].text(), subjectIden)
        return subProjDetails

    def gen_trials(self, subProjDetails):
        """
        Generate trial instance per data file and its corresponding label file/s
        :param subProjDetails: pd.DataFrame{index: project_uid/ unique_loc}
        :return trialDict: dict{TrialManager} [Dictionary of TrialManager instance for each subject recorded]
        """

        trialDict = dict()
        for uid in subProjDetails.index.get_level_values('project_uid').unique():
            trialDict[uid] = self._trialClass(self, self._assignTimings, self._imuClass, uid, subProjDetails.loc[uid])
        return trialDict

    @abstractmethod
    def import_operations(self, desDir, dataDirs, calBaseDir, taskDir, updateProg, updateMess, updateDataReady,
                          managedPopup, **baseDirs):
        pass

    # PRE-PROCESS METHODS
    def preproc_operations(self, orientSettings, procSettings, updateProg, updateMess, updateDataReady, managedPopup):
        """
        All the operations associated with pre-processing the data
        :param orientSettings Dict(Bool) Bool represents whether certain orientation settings are active]
        :param procSettings: Dict(Bool) [Bool represents whether certain pre-processing settings are active]
        :param updateProg: Signal [Updates the GUI when files have been processed for each trial]
        :param updateMess: Signal [Updates the GUI when expected exceptions occur]
        :param updateDataReady: Signal
        :param managedPopup: Signal [Updates the GUI with a popup for the user when additional details required]
        :return message: String [Updates the GUI statusbar]
        :return adcStatus: Bool [Whether adc was recorded for the project]
        """

        print('PREPROC OPERATIONS (%.1f seconds)' % self._globalTime)

        progressCount = 0
        updateProg.emit('preprocess', progressCount, len(self._trialDict))
        for key in self._trialDict:

            updateMess.emit('Pre-processing Data (trial %s)...' % key)
            progressCount += 1

            # IMU-WIDE OPERATIONS
            if procSettings['performResamp']:
                self._trialDict[key].presample_data()
            print('PREPROC[Trial %s]- Re-sampling Complete (%.1f seconds)' % (key, self._globalTime))
            if self._trialDict[key].CalImported:
                self._trialDict[key].calibrate_data()
            self._trialDict[key].standardise_units()
            print('PREPROC[Trial %s]- Calibration Operations Complete (%.1f seconds)' % (key, self._globalTime))
            if orientSettings['flipBodySide'] or orientSettings['calcOrientData']:
                self._trialDict[key].rotate_to_standard_frame()
            if orientSettings['flipBodySide']:
                self._trialDict[key].flip_body_side()
            if orientSettings['calcOrientData']:
                self._trialDict[key].calc_quaternion()
            print('PREPROC[Trial %s]- IMU-wide Orientation Operations Complete (%.1f seconds)' % (key, self._globalTime))

            # TRIAL-WIDE OPERATIONS
            self._trialDict[key].merge_imu_data()
            self._trialDict[key].missing_trial_data()
            if orientSettings['simulateTorso']:
                self._trialDict[key].simulate_torso_quaternion()
            print('PREPROC[Trial %s]- Trial-wide Merging Operations Complete (%.1f seconds)' % (key, self._globalTime))
            if self._assignTimings:
                self._trialDict[key].integrate_task_timings()
            self._trialDict[key].organise_tasks(self._assignTimings)
            print('PREPROC[Trial %s]- Trial-wide Task Organisation Operations Complete (%.1f seconds)' % (key, self._globalTime))
            if orientSettings['tareQuat']:
                self._trialDict[key].tare_orientation()
            if orientSettings['offsetQuat']:
                self._trialDict[key].offset_orientation(orientSettings['offsetDir'])
            self._trialDict[key].remove_initialisation_routines()
            print('PREPROC[Trial %s]- Trial-wide Metric Operations Complete (%.1f seconds)' % (key, self._globalTime))
            self._trialDict[key].filter_data(procSettings['filtBounds'], procSettings['filtBool'], procSettings['wavDenoise'],
                                             procSettings['notchAdc'])
            print('PREPROC[Trial %s]- Filtering Operations Complete (%.1f seconds)' % (key, self._globalTime))
            if orientSettings['calcOrientData'] and orientSettings['saveQuat']:
                self._trialDict[key].save_quat_data(updateDataReady)
            print('PREPROC[Trial %s]- Saving Operations Complete (%.1f seconds)' % (key, self._globalTime))
            updateProg.emit('preprocess', progressCount, len(self._trialDict))
        return 'Completed Pre-processing IMU Data', True

    def global_preproc_operations(self, globalMerge, secondMetrics, jointMetrics, updateProg, updateMess, updateDataReady, managedPopup):
        """
        All the operations associated with pre-processing the data
        :param globalMerge Dict(Bool) Bool represents whether certain metrics will be included in the global merge]
        :param secondMetrics: Dict(Bool) [Bool represents which secondary metrics will be calculated]
        :param updateProg: Signal [Updates the GUI when files have been processed for each trial]
        :param updateMess: Signal [Updates the GUI when expected exceptions occur]
        :param updateDataReady: Signal
        :param managedPopup: Signal [Updates the GUI with a popup for the user when additional details required]
        :return message: String [Updates the GUI statusbar]
        :return adcStatus: Bool [Whether adc was recorded for the project]
        """

        def data_concat_trials(dataDict, names, managedPopup):
            """
            Method for concatenating all trial data into one large dataframe
            :param dataDict: Dict(DataFrame) [Dataframe of each trial data, keys are the trial ID]
            :param names: List(String) [The names for the resulting concatenated dataframe]
            :param managedPopup:
            :return dataConcat
            """

            try:
                dataConcat = pd.concat(dataDict, names=names)
            except MemoryError as exception:
                exc_type = str(sys.exc_info()[0].__name__)
                managedPopup.emit(exception, exc_type)

                while not self.DbDetails:
                    pass
                engine = create_engine('mysql+pymysql://%s:%s@localhost/%s'
                                       % (self.DbDetails['username'], self.DbDetails['password'],
                                          self.DbDetails['schema']), pool_recycle=3600)
                dbConnection = engine.connect()
                command = ''
                for id in dataDict:
                    if command != '':
                        command = command + 'UNION '
                    dataDict[id]['trial'] = id
                    dataDict[id].reset_index().to_sql(str(id), dbConnection, chunksize=50000, if_exists='replace')
                    command = command + '(SELECT * FROM %s.%s) ' % (self.DbDetails['schema'], id)
                dataConcat = pd.read_sql(command, con=dbConnection)
                dataConcat.drop('index', axis=1, inplace=True)
                dataConcat.columns = pd.MultiIndex.from_tuples((eval(column) for column in dataConcat.columns),
                                                               names=['meta', 'metric'])
                dataConcat.set_index(names, inplace=True)

            return dataConcat

        def data_prep(data):
            """
            Prepares dataframe data for plotting and further processing
            :param data: pd.DataFrame [index: trial/ incr]
            :return data: pd.DataFrame [index: trial/ task/  task_inst]
            """
            # Drop the outer index which is just an incremental index
            data.index = data.index.droplevel(level=1)
            data.set_index([('ALL', 'task'), ('ALL', 'task_inst')], append=True, inplace=True)
            data.index.names = ['trial', 'task', 'task_inst']
            return data

        def calc_joint_metrics(data, jointMetrics_loc):
            """
            Method for calculating the joint angle between IMUs on limb segments
            :param data: pd.DataFrame{index: trial/ task/ window, columns: location/ metric} [The calculated quaternions for each joint]
            :param jointMetrics_loc: Dict{Bool} [Which joint metrics are checked to have features calculated]
            :return data: pd.DataFrame{index: trial/ task/ window, columns: location/ metric} [The calculated joint metrics for the joint pairs]
            """

            def joint_angle_mag(jointVec):
                """
                Method for calculating the norm angle between two vectors
                :param jointVecs: pd.DataFrame [index: trial/ task/ window, columns: location/ metric]
                :return pd.DataFrame [index: trial/ task/ window, columns: 0]
                """

                if any(jointVec.iloc[:, 0].isna()):
                    return pd.DataFrame(np.repeat(np.nan, len(jointVec.index)), index=jointVec.index)

                # vector with direction parallel to the gravity vector in the Madgwick reference frame
                vertVec = np.array((0, 0, 1))

                # Find the minimum angle between these two vectors
                jointAngleMag = Orientation.MinAngleBetweenVectors(np.vstack(jointVec), vertVec)

                return pd.DataFrame(data={'joint_mag': jointAngleMag}, index=jointVec.index)

            def joint_angles(jointQuat):
                """
                Method for calculating each of the axis angles between two vectors
                :param jointQuat: pd.Series [index: trial/ task/ window, columns: location/ metric]
                :return pd.DataFrame [index: trial/ task/ window, columns: 0]
                """

                if any(jointQuat.iloc[:, 0].isna()):
                    return pd.DataFrame(np.tile(np.nan, (len(jointQuat.index), 3)), index=jointQuat.index,
                                        columns=['jointX', 'jointY', 'jointZ'])

                # vector with direction parallel to the gravity vector in the Madgwick reference frame
                vertVec = np.array([0, 0, 1])
                altVec = np.array([0, 1, 0])
                # This is the pure quaternion representation of above vector
                pureVertQuat = Quaternion(0, vertVec[0], vertVec[1], vertVec[2])
                pureAltQuat = Quaternion(0, vertVec[0], vertVec[2], vertVec[1])
                # Calculating the MultiQuaternion for speed of processing (convert from df to series first)
                jointMultiQuat = MultiQuaternion(jointQuat.iloc[:, 0])

                # Calculating the joint angles in each direction
                jointMultiQuatX = jointMultiQuat.to_twist_decomposition(np.array((1, 0, 0)))
                pureJointXVec = jointMultiQuatX * pureVertQuat * jointMultiQuatX.conj()
                jointAngleX = Orientation.MinAngleBetweenVectors(pureJointXVec.to_vector(), vertVec)

                jointMultiQuatY = jointMultiQuat.to_twist_decomposition(np.array((0, 1, 0)))
                pureJointYVec = jointMultiQuatY * pureVertQuat * jointMultiQuatY.conj()
                jointAngleY = Orientation.MinAngleBetweenVectors(pureJointYVec.to_vector(), vertVec)

                jointMultiQuatZ = jointMultiQuat.to_twist_decomposition(np.array((0, 0, 1)))
                pureJointZVec = jointMultiQuatZ * pureAltQuat * jointMultiQuatZ.conj()
                jointAngleZ = Orientation.MinAngleBetweenVectors(pureJointZVec.to_vector(), altVec)

                return pd.DataFrame(data={'joint_x': jointAngleX, 'joint_y': jointAngleY, 'joint_z': jointAngleZ},
                                    index=jointQuat.index)

            jointPairs = [['CENTRE_T', 'RIGHT_UA'], ['RIGHT_UA', 'RIGHT_LA'], ['CENTRE_T', 'LEFT_UA'],
                          ['LEFT_UA', 'LEFT_LA']]
            jointDict = dict()
            jointLocs = dict()
            for jointPair in jointPairs:
                metricList = list()
                jointName = jointPair[0] + '_' + jointPair[1]
                if jointPair[0] in data.columns.unique(level='location') and jointPair[1] in data.columns.unique(level='location'):
                    # Joint metric preparation
                    # Outputs Quaternions as 4-element arrays
                    data.loc[:, (jointName, 'joint_quat')] = data.loc[:, [(jointPair[0], 'quat'), (jointPair[1], 'quat')]].groupby(
                        level='trial', group_keys=False, squeeze=True).apply(Orientation.JointQuat)
                    # Outputs Joint Vectors as 3-element arrays
                    data.loc[:, (jointName, 'joint_vec')] = data.loc[:, (jointName, 'joint_quat')].groupby(
                        level='trial', group_keys=False, squeeze=True).apply(Orientation.QuatToVec)
                    print('GLOBAL_PREPROC[All Tasks]- Orientation Prep for Joint Pair %s Calculated (%.1f seconds)' % (jointPair, self._globalTime))

                    # Joint metric calculation
                    if jointMetrics_loc['axisJointAngle']:
                        metricList.append(
                            data.loc[:, [(jointName, 'joint_quat')]].groupby(level='trial', group_keys=False,
                                                                             squeeze=True).apply(joint_angles))
                    if jointMetrics_loc['magJointAngle']:
                        metricList.append(
                            data.loc[:, [(jointName, 'joint_vec')]].groupby(level='trial', group_keys=False,
                                                                            squeeze=True).apply(joint_angle_mag))
                    if jointMetrics_loc['axisJointPlane']:
                        metricList.append(
                            pd.DataFrame(data.loc[:, (jointName, 'joint_vec')].values.tolist(), index=data.index,
                                         columns=['plane_x', 'plane_y', 'plane_z']))
                    print('GLOBAL_PREPROC[All Tasks]- Orientation Metrics for Joint Pair %s Calculated (%.1f seconds)' % (jointPair, self._globalTime))
                    jointDict[jointName] = pd.concat(metricList, axis=1)
                    jointLocs[jointName] = [jointPair[0], jointPair[1]]
                    print('GLOBAL_PREPROC[All Tasks]- Orientation Merge for Joint Pair %s Calculated (%.1f seconds)' % (jointPair, self._globalTime))


            jointMetrics_loc = pd.concat(jointDict, axis=1, names=['location', 'metric'])
            return jointMetrics_loc, jointLocs

        def ratio_calc(data):
            """
            Method for calculating the ratio value between certain body segment pairs
            :param data: pd.DataFrame [index: trial/ task/ window, columns: location/ metric]
            :return data: pd.DataFrame [index: trial/ task/ window, columns: location/ metric]
            :return ratioList: list{string}
            """

            def ratio(subData, ratioLoc1, ratioLoc2):
                """
                Calculate simple ratio between two Dataframe columns
                An first step ensures than ratio is only calculated when there is a minimum level of amplitude (to reduce unhelpful high values)
                A second step removes unhelpful stupidly high values which occur when one location registers very low values
                :param subData: pd.DataFrame  [index: trial/ task/ window, columns: location/ metric]
                :return ratioCalc: pd.Series [index: trial/ task/ window]
                """

                ratioCalc = pd.Series(data=np.repeat(1, len(subData.index)), index=subData.index)
                # Avoid error of division by nan which will arise with multiple subjects with different locations
                if subData.isnull().values.any():
                    return ratioCalc
                # Test for condition of minimum amplitude to ensure that at least one of the locations contains
                # sufficient amplitude at the data-point to prevent misleading high ratio values
                # Checks for each data-point that the sum of the absolutes of the 2 location is greater than half the absolute mean across all data-points
                intensityThresh = subData.abs().sum(axis=1) > subData.abs().sum(axis=1).mean(axis=0) / 2
                # Apply ratio to samples that satisfy condition
                ratioCalc.loc[intensityThresh] = subData.loc[intensityThresh, ratioLoc1] / subData.loc[intensityThresh, ratioLoc2]
                ratioCalc.replace(np.inf, 100.0, inplace=True)  # Replace any inf values which occur when one gyroscope reads zero
                ratioCalc.loc[ratioCalc > 100] = 100.0
                return ratioCalc

            ratioPairs = [['RIGHT_LA', 'RIGHT_UA'], ['LEFT_LA', 'LEFT_UA']]
            ratioLocs = dict()
            for ratioPair in ratioPairs:
                ratioName = ratioPair[0] + '_' + ratioPair[1]
                if ratioPair[0] in data.columns.unique(level='location') and ratioPair[1] in data.columns.unique(level='location'):
                    for metric in ['acc_x', 'acc_y', 'acc_z', 'gyr_x', 'gyr_y', 'gyr_z']:
                        data.loc[:, (ratioName, metric)] = data.loc[:, [(ratioPair[0], metric), (ratioPair[1], metric)]].groupby(
                                 level='trial', group_keys=False, squeeze=True).apply(ratio, args=(ratioPair[0], ratioPair[1]))
                    ratioLocs[ratioName] = [ratioPair[0], ratioPair[1]]
            return data, ratioLocs

        def jerk_calc(data, inertialFreq):
            """
            Method for calculating the jerk from the accelerometer axes
            :param data: pd.DataFrame [filtered inertial dataframe]
            :return data: pd.DataFrame [same as input except with jerk columns appended]
            """

            for location in data.columns.unique(level='location'):
                if location == 'ALL':
                    continue
                if self._accMetrics[0] in data[location].columns.unique(level='metric'):
                    data.loc[:, (location, ['jerk_x', 'jerk_y', 'jerk_z'])] = \
                        data.loc[:, (location, ['acc_x', 'acc_y', 'acc_z'])].diff().multiply(inertialFreq, axis=0)
            return data

        def magnitude_calc(data):
            """
            Method for calculating the magnitude value across accelerometer and gyroscope axes
            :param data: pd.DataFrame [filtered inertial dataframe]
            :return data: pd.DataFrame [same as input except with magnitude columns appended]
            """
            for location in data.columns.unique(level='location'):
                if location == 'ALL':
                    continue
                if self._accMetrics[0] in data[location].columns.unique(level='metric'):
                    data.loc[:, (location, 'acc_mag')] = np.linalg.norm(data.loc[:, (location, self._accMetrics)].values,
                                                                        axis=1)
                if self._jerkMetrics[0] in data[location].columns.unique(level='metric'):
                    data.loc[:, (location, 'jerk_mag')] = np.linalg.norm(data.loc[:, (location, self._gyrMetrics)].values,
                                                                        axis=1)
                if self._gyrMetrics[0] in data[location].columns.unique(level='metric'):
                    data.loc[:, (location, 'gyr_mag')] = np.linalg.norm(data.loc[:, (location, self._gyrMetrics)].values,
                                                                        axis=1)
            return data

        print('GLOBAL PREPROC OPERATIONS (%.1f seconds)' % self._globalTime)
        # GLOBAL MERGE
        # Collect the DataFrames from each trial, down-sampling to match the project frequency if necessary
        if globalMerge['unfiltInertial']:
            unfiltInertialDict = {trial: self._trialDict[trial].InertialTrialDf(self.InertialFreq, filt=False) for trial in self._trialDict}
            self._unfiltInertialDf = data_concat_trials(unfiltInertialDict, names=['trial'], managedPopup=managedPopup)
            self._unfiltInertialDf = data_prep(self._unfiltInertialDf)
            del unfiltInertialDict
        if globalMerge['filtInertial']:
            filtInertialDict = {trial: self._trialDict[trial].InertialTrialDf(self.InertialFreq, filt=True) for trial in self._trialDict}
            self._filtInertialDf = data_concat_trials(filtInertialDict, names=['trial'], managedPopup=managedPopup)
            self._filtInertialDf = data_prep(self._filtInertialDf)
            del filtInertialDict
        if globalMerge['unfiltAdc']:
            unfiltAdcDict = {trial: self._trialDict[trial].AdcTrialDf(self.AdcFreq, filt=False) for trial in self._trialDict}
            self._unfiltAdcDf = data_concat_trials(unfiltAdcDict, names=['trial'], managedPopup=managedPopup)
            self._unfiltAdcDf = data_prep(self._unfiltAdcDf)
            del unfiltAdcDict
        if globalMerge['filtAdc']:
            filtAdcDict = {trial: self._trialDict[trial].AdcTrialDf(self.AdcFreq, filt=True) for trial in self._trialDict}
            self._filtAdcDf = data_concat_trials(filtAdcDict, names=['trial'], managedPopup=managedPopup)
            self._filtAdcDf = data_prep(self._filtAdcDf)
            del filtAdcDict
        if globalMerge['quat']:
            quatDict = {trial: self._trialDict[trial].OrientTrialDf(self.InertialFreq) for trial in self._trialDict}
            self._quatDf = data_concat_trials(quatDict, names=['trial'], managedPopup=managedPopup)
            self._quatDf = data_prep(self._quatDf)
            del quatDict
        print('GLOBAL_PREPROC[All Tasks]- Project-wide Merge Complete (%.1f seconds)' % self._globalTime)
        updateProg.emit('global_preproc', 1, 4)
        updateMess.emit('Completed trial-wide merge')

        """SECONDARY METRICS"""
        """Performed on the project-wide scale rather than imu-wide to avoid memory errors when merging"""
        # DataFrame, Location and Metric level
        if any(jointMetrics.values()):
            self._orientDf, self._jointLocs = calc_joint_metrics(self._quatDf, jointMetrics)
            del self._quatDf
        updateProg.emit('global_preproc', 2, 4)
        # Location and Metric level
        if secondMetrics['ratioInertial']:
            self._filtInertialDf, self._ratioLocs = ratio_calc(self._filtInertialDf)
        updateProg.emit('global_preproc', 3, 4)
        # Metric level
        if secondMetrics['jerkInertial']:
            self._filtInertialDf = jerk_calc(self._filtInertialDf, self.InertialFreq)
        # Sub-Metric level
        if secondMetrics['magInertial']:
            self._magInertial = True
            if globalMerge['unfiltInertial']:
                self._unfiltInertialDf = magnitude_calc(self._unfiltInertialDf)
            if globalMerge['filtInertial']:
                self._filtInertialDf = magnitude_calc(self._filtInertialDf)
        print('GLOBAL_PREPROC[All Tasks]- Secondary Metrics Calculated (%.1f seconds)' % self._globalTime)
        updateMess.emit('Completed calculation of secondary metrics')

        return 'Completed Global Pre-processing IMU Data', True

    # PLOTTING METHODS
    def plot_operations(self, plotSelection, plotConfig, plotDisplay, plotMag, dataProcessing,
                        updateProg, updateMess, updateDataReady, managedPopup):
        """
        All the operations associated with pre-processing the data
        :param plotSelection: Dict(Bool) [Bool represents which metrics are to be plotted]
        :param plotConfig: Dict(Bool) [Bool represents where various plot configuration settings are active]
        :param plotDisplay: Dict(Bool) [Bool represents where various plot configuration settings are active]
        :param updateProg: Signal [Updates the GUI when files have been processed for each trial]
        :param updateMess: Signal [Updates the GUI when expected exceptions occur]
        :param updateDataReady: Signal
        :param managedPopup: Signal [Updates the GUI with a popup for the user when additional details required]
        :return message: String [Updates the GUI statusbar]
        :return adcStatus: Bool [Whether adc was recorded for the project]
        """

        def get_plot_metric_info(pt, pm):
            """
            Returns the required plot data given certain data parameters
            :param pt: string [plot type]
            :param pm: dict{Bool} [plot magnitude metrics to calculate]
            :return: plotData
            """

            magMetric = None
            if pt == 'unfilteredAcc' or pt == 'filteredAcc' or pt == 'filteredAccRatio':
                metric = self._sAccMetric
                magMetric = self._sAccMagMetric
            elif pt == 'unfilteredGyr' or pt == 'filteredGyr' or pt == 'filteredGyrRatio':
                metric = self._sGyrMetric
                magMetric = self._sGyrMagMetric
            elif pt == 'filteredJerk':
                metric = self._sJerkMetric
                magMetric = self._sJerkMagMetric
            elif pt == 'unfilteredAdc' or pt == 'filteredAdc':
                metric = self._adcMetric
            elif pt == 'jointAngleAxis':
                metric = self._sJointAngMetric
            elif pt == 'jointAngleMagAxis':
                metric = self._sJointAngMagMetric
            elif pt == 'jointPlane':
                metric = self._planeMetric

            # Set the magnitude metric in the base that it exists for current metric
            if pm['addMag'] and magMetric is not None:
                metric = metric + magMetric
            elif pm['onlyMag'] and magMetric is not None:
                metric = magMetric

            return metric

        print('PLOTTING OPERATIONS (%.1f seconds)' % self._globalTime)

        plotList = [plotKey for plotKey in plotSelection.keys() if plotSelection[plotKey][0]]
        progressCount = 0
        updateProg.emit('plotting', progressCount, len(plotList))
        plotObj = PlotManager(plotConfig, plotDisplay, self._allTasks, dataProcessing)
        for plotType in plotList:
            plotMetric = plotSelection[plotType][1]
            filtStatus = plotSelection[plotType][2]
            metricHeading = get_plot_metric_info(plotType, plotMag)
            updateMess.emit('Plotting Data (%s)...' % plotType)
            updateDataReady.emit('figure', 'newDir', plotType, None)
            progressCount += 1
            if plotMetric == 'inertialBase':
                if filtStatus == 'filt':
                    df = self._filtInertialDf
                else:
                    df = self._unfiltInertialDf
                for trial in self._trialDict:
                    for imu in self._trialDict[trial].ImuDict.keys():
                        plotName = 'trial' + str(trial) + '_imu' + imu + '_' + plotMetric + '_' + plotType
                        plotFreq = self._trialDict[trial].ImuDict[imu].InertialFreq
                        plotObj.plot_data(updateDataReady, df.loc[trial, (location, [metricHeading])], plotName, plotFreq)
            elif plotMetric == 'adcBase' and self.AdcRecorded:
                if filtStatus == 'filt':
                    df = self._filtAdcDf
                else:
                    df = self._unfiltAdcDf
                for trial in self._trialDict:
                    for imu in self._trialDict[trial].ImuDict.keys():
                        plotName = 'trial' + str(trial) + '_imu' + imu + '_' + plotMetric + '_' + plotType
                        plotFreq = self._trialDict[trial].ImuDict[imu].AdcFreq
                        plotObj.plot_data(updateDataReady, df.loc[trial, (location, [metricHeading])], plotName, plotFreq)
            elif plotMetric == 'inertialRatio':
                df = self._filtInertialDf
                for trial in self._trialDict:
                    for ratioLoc in self._ratioLocs.keys():
                        plotName = 'trial' + str(trial) + '_ratio' + ratioLoc + '_' + plotMetric + '_' + plotType
                        plotFreq = min([self._trialDict[trial].ImuDict[self._ratioLocs[ratioLoc][0]].InertialFreq,
                                        self._trialDict[trial].ImuDict[self._ratioLocs[ratioLoc][1]].InertialFreq])
                        plotObj.plot_data(updateDataReady, df.loc[trial, (location, [metricHeading])], plotName, plotFreq)
            elif plotMetric == 'orient':
                df = self._orientDf
                for trial in self._trialDict:
                    for joint in self._jointLocs.keys():
                        plotName = 'trial' + str(trial) + '_joint' + joint + '_' + plotMetric + '_' + plotType
                        plotFreq = min([self._trialDict[trial].ImuDict[self._jointLocs[joint][0]].InertialFreq,
                                        self._trialDict[trial].ImuDict[self._jointLocs[joint][1]].InertialFreq])
                        plotObj.plot_data(updateDataReady, df.loc[trial, (location, [metricHeading])], plotName, plotFreq)

            updateProg.emit('plotting', progressCount, len(plotList))
            print('PLOTTING[Data %s]- Plotting Type Complete (%.1f seconds)' % (plotType, self._globalTime))

        updateDataReady.emit('figure', 'report', 'plot_description', None)
        return 'Completed Plotting IMU Data', True

    # FEATURE CALCULATION METHODS
    def feature_operations(self, windowManager, fm, updateProg, updateMess, updateDataReady, managedPopup):
        """
        All the operations associated with windowing the data, calculating features, and exporting the features
        :param windowManager: WindowManager [Instance responsible for assigning window to each task instance]
        :param fm: FeatureManager [Instance responsible for extracting features from the data]
        :param updateProg: Signal [Updates the GUI when files have been processed for each trial]
        :param updateMess: Signal [Updates the GUI with progress messages]
        :param updateDataReady: Signal
        :param managedPopup: Signal [Updates the GUI with a popup for the user when additional details required]
        """

        print('FEATURE OPERATIONS (%.1f seconds)' % self._globalTime)

        # DATA PREP
        for dataset in [self._filtInertialDf, self._orientDf, self._unfiltAdcDf, self._filtAdcDf]:
            if dataset is not None:
                dataset.drop('no task', level='task', inplace=True)
                dataset.sort_index(inplace=True)
                numProcSteps = len(dataset.index.unique('task')) + 2
        updateProg.emit('features', 0, numProcSteps)

        # LABEL DATA
        labelDf = self.get_label_data(self._trialDict)
        """This data is required in the case of classifying to something other than the task being performed"""
        updateProg.emit('features', 1, numProcSteps)

        # WINDOWING
        winInertialDf, winOrientDf, winRawAdcDf, winAdcDf = None, None, None, None
        if self._filtInertialDf is not None:
            winInertialDf = windowManager.window_data(self._filtInertialDf, self.InertialFreq, dropTime=False)
        if self._orientDf is not None:
            winOrientDf = windowManager.window_data(self._orientDf, self.InertialFreq, dropTime=True)
        if self._unfiltAdcDf is not None:
            winRawAdcDf = windowManager.window_data(self._unfiltAdcDf, self.AdcFreq, dropTime=True)
        if self._filtAdcDf is not None:
            winAdcDf = windowManager.window_data(self._filtAdcDf, self.AdcFreq, dropTime=True)
        print('FEATURE[All Tasks]- Data Windowed (%.1f seconds)' % self._globalTime)
        updateProg.emit('features', 2, numProcSteps)
        updateMess.emit('Windowed the Data Successfully')
        """window added as innermost index, task instance index removed, task swapped to outermost index
           New MultiIndex- task/trial/window"""

        # FEATURE CALCULATION
        taskIdx = 1
        # Locations
        inertialLocs = [location for location in winInertialDf.columns.unique(level='location') if location not in ['ALL']]
        ratioLocs = list(self._ratioLocs.keys())
        jointLocs = list(self._jointLocs.keys())
        # Metrics
        inertialMetric = self._sAccMetric + self._sGyrMetric
        jerkMetric = self._sJerkMetric
        if self._magInertial:
            inertialMetric = inertialMetric + self._sAccMagMetric + self._sGyrMetric
            jerkMetric = jerkMetric + self._sJerkMagMetric

        # Cycle through each task to find the feature DataFrame for each super DataFrame
        for task in winInertialDf.index.unique('task'):
            updateDataReady.emit('feature', 'subDir', task, None)
            md = dict()
            md['timer'] = fm.feature_calc(winInertialDf.loc[task, [('ALL', 'time_stamp')]], 'general', 'timer', self.InertialFreq)
            if winInertialDf is not None:
                md['inertial_time'] = fm.feature_calc(winInertialDf.loc[task, (inertialLocs, inertialMetric)], 'time', 'inertial', self.InertialFreq)
                md['inertial_freq'] = fm.feature_calc(winInertialDf.loc[task, (inertialLocs, inertialMetric)], 'freq', 'inertial', self.InertialFreq)
                if jerkMetric[0] in winInertialDf.columns.get_level_values(level=1):
                    md['inertialJerk_time'] = fm.feature_calc(winInertialDf.loc[task, (inertialLocs, jerkMetric)], 'time', 'jerk', self.InertialFreq)
                if ratioLocs:
                    md['inertialRatio_time'] = fm.feature_calc(winInertialDf.loc[task, ratioLocs], 'time', 'ratio', self.InertialFreq)
            if winRawAdcDf is not None:
                md['rawAdc_time'] = fm.feature_calc(winRawAdcDf.loc[task], 'time', 'adc', self.AdcFreq)
                md['rawAdc_freq'] = fm.feature_calc(winRawAdcDf.loc[task], 'freq', 'adc', self.AdcFreq)
            if winAdcDf is not None:
                md['adc_time'] = fm.feature_calc(winAdcDf.loc[task], 'time', 'adc', self.AdcFreq)
                md['adc_freq'] = fm.feature_calc(winAdcDf.loc[task], 'freq', 'adc', self.AdcFreq)
            if winOrientDf is not None:
                if self._sJointAngMagMetric in winOrientDf.columns.get_level_values(level=1):
                    md['jointAngleAngleMag_time'] = fm.feature_calc(winOrientDf.loc[task, (jointLocs, self._sJointAngMagMetric)],
                        'time', 'jointAngle', self.InertialFreq)
                if self._sJointAngMetric[0] in winOrientDf.columns.get_level_values(level=1):
                    md['jointAngleAngleMag_time'] = fm.feature_calc(winOrientDf.loc[task, (jointLocs, self._sJointAngMetric)],
                        'time', 'jointAngle', self.InertialFreq)
                if self._jointMetrics['axisJointPlane']:
                    md['jointPlane_time'] = fm.feature_calc(winOrientDf.loc[task, (jointLocs, self._sJointPlaneMetric)],
                        'time', 'jointPlane', self.InertialFreq)

            # Concatenate the dict of dataframes and pass back to the main thread for saving
            featData = pd.concat(md, axis=1)
            print('FEATURE[%s]- Features Calculated (%.1f seconds)' % (task, self._globalTime))
            updateProg.emit('features', taskIdx + 2, numProcSteps)
            updateMess.emit('Features calculated for task: ' + task)
            exportData = self.prepare_export(featData, task, labelDf)
            updateDataReady.emit('feature', 'data', 'features', exportData)
            updateDataReady.emit('feature', 'report', 'export_description', exportData)
            taskIdx += 1
        del winInertialDf, winAdcDf

        return 'Completed Feature Extraction', True

    @staticmethod
    @abstractmethod
    def get_label_data(data):
        pass

    # PROPERTIES
    @property
    def UidKeys(self):
        return list(self._trialDict.keys())

    @property
    def AllTasks(self):
        return self._allTasks

    @property
    def OrientRecorded(self):
        """
        :return orientRecorded: Bool
        """
        return any(self._trialDict[trial].QuatRecorded for trial in self._trialDict)

    @property
    def AdcRecorded(self):
        """
        :return adcRecorded: Bool
        """
        return any(self._trialDict[trial].AdcRecorded for trial in self._trialDict)

    @property
    def InertialFreq(self):
        """
        Returns the mimimum inertial frequency recorded across all imus for the trial
        :return:
        """
        if self._inertialFreq is None:
            self._inertialFreq = min([self._trialDict[trial].InertialFreq for trial in self._trialDict])
        return self._inertialFreq

    @property
    def AdcFreq(self):
        """
        Returns the mimimum adc frequency recorded across all trials for the project
        :return:
        """
        if self._adcFreq is not None:
            return self._adcFreq
        if all(self._trialDict[trial].AdcFreq is None for trial in self._trialDict):
            return None
        self._adcFreq = min([self._trialDict[trial].AdcFreq for trial in self._trialDict if self._trialDict[trial].AdcFreq is not None])
        return self._adcFreq


class RatingProjManager(ProjManager):
    """
    This class inherits from the "ProjManager" base class
    This trial type aims to develop a classification model which can predict the score of a performed task
    using the window of sensor data for that task
    """

    def __init__(self, assignTimings, trialClass, imuClass, subjectIden):
        """
        One ProjManager instance created at runtime regardless of number of trials
        :param assignTimings: Bool [Whether the project has a separate timings file which must be assigned to the data]
        :param trialClass: Pointer [Points to inherited class from TrialManager]
        :param imuClass: Pointer [Points to inherited class from ImuManager]
        """

        super().__init__(assignTimings, trialClass, imuClass, subjectIden)

    def import_operations(self, interpNan, dataDirs, taskDir, descDir, updateProg, updateMess, updateDataReady, managedPopup,
                          **baseDirs):
        """
        All the operations associated with organising the data files and importing them
        :param dataDirs: List(String) [Directory for each of the data files selected]
        :param taskDir: Dict(List(String)/ String) [Dict of the different directory types]
        :param descDir: String [Directory of the project descriptor files]
        :param updateProg: Signal [Updates the GUI when files have been processed for each trial]
        :param updateMess: Signal [Updates the GUI when expected exceptions occur]
        :param managedPopup: Signal [Deals with managable exceptions]
        :param baseDirs: Dict(String) [Directories which may be optional depending on the project type (calibration/ rating/ timing)]
        :return message:
        :return adcStatus:
        """

        print('IMPORT OPERATIONS (%.1f seconds)' % self._globalTime)
        # REQUIRED METHODS (PROJECT SHOWS ERROR MESSAGE IF ERROR THROWN)
        self._allTasks = self.import_proj_tasks(taskDir)
        projDetails = self.import_proj_details(descDir)
        # Extract only the necessary subset of project details for this sub-project. Also organises the correct directories for the subjects tested
        subProjDetails = self.organise_dirs(dataDirs, projDetails, self._subjectIden, **baseDirs)
        self._trialDict = self.gen_trials(subProjDetails)

        progressCount = 0
        for key in self._trialDict:

            updateMess.emit('Importing Data (trial %s)...' % key)
            progressCount += 1

            # OPTIONAL METHODS (TRIAL WILL STILL ATTEMPT TO RUN IF ERROR THROWN)
            try:
                self._trialDict[key].import_rating()
            except BaseException as exception:
                updateMess.emit('Rating files could not be imported (trial %s)' % key)
                QtCore.QThread.currentThread().sleep(2)
            try:
                self._trialDict[key].import_calibration()
            except BaseException as exception:
                updateMess.emit('Calibration files could not be imported for trial %s' % key)

            # REQUIRED METHODS (TRIAL DISCARDED IF ERROR THROWN)
            keysRemoved = list()
            try:
                headerLines, dataWidth = self._trialDict[key].import_header_data()
                print('IMPORT[Trial %s]- Importing Header Data Complete (%.1f seconds)' % (key, self._globalTime))
                self._trialDict[key].import_trial_data(headerLines, dataWidth)
                self._trialDict[key].gen_imus()
                self._trialDict[key].import_imu_data(headerLines, dataWidth)
                print('IMPORT[Trial %s]- Generating IMUs and Importing Body Data Complete (%.1f seconds)' % (key, self._globalTime))
                self._trialDict[key].clean_trial_data()
                self._trialDict[key].make_tasks_unique()
                self._trialDict[key].partition_adc_imu_data()
                self._trialDict[key].continuous_counter()
                self._trialDict[key].interpolate_missing_rows(interpNan)
                self._trialDict[key].calc_time_stamp()
                self._trialDict[key].extract_orientation_data()
                print('IMPORT[Trial %s]- Cleaning Data Complete (%.1f seconds)' % (key, self._globalTime))
                updateProg.emit('import', progressCount, len(self._trialDict))
            except BaseException as exception:
                raise(exception)
                updateMess.emit('Trial %s removed from analysis due to exception: %s' % (key, exception))
                print('Trial %s removed from analysis due to exception: %s' % (key, exception))
                keysRemoved.append(key)

        for key in keysRemoved:
            self._trialDict.pop(key)

        return 'Completed Importing CSV Files', self.AdcRecorded

    # FEATURE CALCULATION
    @staticmethod
    def get_label_data(trialDict):
        """
        Method for collecting rating data for outputting data ready for classification
        :param trialDict: dict{TrialManager}
        :return ratingDf: pd.DataFrame{column: trials, index: task, data: scores}
        """

        ratingDict = {trial: trialDict[trial].RatingData for trial in trialDict}
        if all([ratingData is None for ratingData in ratingDict.values()]):
            raise Exception('No rating data found so data will not be exported')
        ratingDf = pd.concat(ratingDict, axis=1)
        ratingDf.dropna(inplace=True)  # Drop the tasks which contain NaN (reflex tasks etc)
        return ratingDf

    @staticmethod
    def prepare_export(featData, task, ratingDf):
        """
        Method for preparing the data and passing it to the exporter class instance
        :param featData: pd.DataFrame [index: trial, columns: metric/ feature]
        :param task: string
        :param ratingDf:
        :return prepFeatData: pd.DataFrame [index: label/ trial, columns: metric/ feature]
        """

        # In the event rating data recorded then apply this data as an index to
        # the existing feature data for easy indexing and later use
        for trial in featData.index.unique('trial'):
            featData.loc[trial, 'label'] = ratingDf.loc[task, trial]
        featData = featData.set_index(featData['label'].astype(int), append=True).reorder_levels(['label', 'trial'])
        featData.drop('label', level=0, axis=1, inplace=True)
        return featData


class ActivityProjManager(ProjManager):
    """
    This class inherits from the "ProjManager" base class
    This trial type aims to develop a classification model which can predict which task is being performed using
    long streams of historic data of that task
    Unlike "RatingProjManager" DOES NOT require rating file and DOES require windowing of data
    """

    def __init__(self, assignTimings, trialClass, imuClass, subjectIden):
        """
        One ProjManager instance created at runtime regardless of number of trials selected
        :param assignTimings: Bool [Whether the project has a separate timings file which must be assigned to the data]
        :param trialClass: Pointer [Points to inherited class from TrialManager]
        :param imuClass: Pointer [Points to inherited class from ImuManager]
        """

        super().__init__(assignTimings, trialClass, imuClass, subjectIden)

    # IMPORT DATA
    def import_operations(self, interpNan, dataDirs, taskDir, descDir, updateProg, updateMess, managePopup,
                          **baseDirs):
        """
        All the operations associated with organising the data files and importing them
        Same as for the Class "RatingProjManager" except do not need to import rating files
        :param dataDirs: String [Directory for each of the data files selected]
        :param taskDir: Dict(List(String)/ String) [Dict of the different directory types]
        :param descDir: String [Directory of the project descriptor file]
        :param updateProg: Signal [Updates the GUI when files have been processed for each trial]
        :param updateMess: Signal [Updates the GUI when expected exceptions occur]
        :param baseDirs
        :return:
        """

        print('IMPORT OPERATIONS (%.1f seconds)' % self._globalTime)
        # REQUIRED METHODS (PROJECT FAILS WITH ERROR MESSAGE IF ERROR THROWN)
        self._allTasks = self.import_proj_tasks(taskDir)
        projDetails = self.import_proj_details(descDir)
        # Extract only the necessary subset of project details for this sub-project. Also organises the correct directories for the subjects tested
        subProjDetails = self.organise_dirs(dataDirs, projDetails, self._subjectIden, **baseDirs)
        self._trialDict = self.gen_trials(subProjDetails)

        progressCount = 0
        for key in self._trialDict:

            updateMess.emit('Importing Data (trial %s)...' % key)
            progressCount += 1

            # OPTIONAL METHODS (TRIAL WILL STILL ATTEMPT TO RUN IF ERROR THROWN)
            try:
                self._trialDict[key].import_calibration()
            except BaseException as exception:
                updateMess.emit('Calibration files could not be imported (trial %s)' % key)

            # REQUIRED METHODS (TRIAL DISCARDED IF ERROR THROWN)
            keysRemoved = list()
            try:
                headerLines, dataWidth = self._trialDict[key].import_header_data()
                print('Finished Importing Header Data for trial %s (%.1f seconds)' % (key, self._globalTime))
                self._trialDict[key].import_trial_data(headerLines, dataWidth)   # One csv file per trial
                self._trialDict[key].gen_imus()
                self._trialDict[key].import_imu_data(headerLines, dataWidth)     # One csv file per imu
                print('Finished Generating IMUs and importing data for trial %s (%.1f seconds)' % (key, self._globalTime))
                self._trialDict[key].clean_trial_data()
                self._trialDict[key].make_tasks_unique()
                self._trialDict[key].partition_adc_imu_data()
                self._trialDict[key].continuous_counter()
                self._trialDict[key].interpolate_missing_rows(interpNan)
                self._trialDict[key].calc_time_stamp()
                self._trialDict[key].extract_orientation_data()
                print('Finished Cleaning Data for trial %s (%.1f seconds)' % (key, self._globalTime))
                updateProg.emit('import', progressCount, len(self._trialDict))
            except BaseException as exception:
                raise exception
                print('Trial %s removed from analysis due to exception: %s' % (key, exception))
                keysRemoved.append(key)
                continue

        for key in keysRemoved:
            self._trialDict.pop(key)

        return 'Completed Importing CSV Files', self.AdcRecorded

    # FEATURE CALCULATION
    @staticmethod
    def get_label_data(ratingData=None):
        """
        No rating data present in activity data processing so this method is ignored
        :param ratingData:
        :return:
        """
        return None

    @staticmethod
    def prepare_export(featDict, exportManager, ratingDf=None):
        """
        Method for preparing the data and passing it to the exporter class instance
        :param featDict:
        :param exportManager:
        :param ratingDf:
        :return:
        """

        featData = pd.concat(featDict, names=['label', 'trial', 'window'])
        exportManager.export_to_python(featData, 'activity')


class TestProjManager(ProjManager):
    """
    This class inherits from the "ProjManager" base class
    Objects which include this class may not output any processed results but may be used to plot
    intermediate results
    """

    def __init__(self, assignTimings, trialClass, imuClass, subjectIden):
        """
        One ProjManager instance created at runtime regardless of number of trials selected
        :param assignTimings: Bool [Whether the project has a separate timings file which must be assigned to the data]
        :param trialClass: Pointer [Points to inherited class from TrialManager]
        :param imuClass: Pointer [Points to inherited class from ImuManager]
        """

        super().__init__(assignTimings, trialClass, imuClass, subjectIden)

    @staticmethod
    def organise_dirs(dataDirs, subjectIden, **baseDirs):

        def manage_data_dirs(dataDirs_l, subjectIden):
            """
            Method for extracting the patient information from the filename
            Test trials (not on patients) are assigned a zero for number and patient UID
            Finds patient number contained between the strings "UID" and ")"
            :param dataDirs_l: List(String) [Local scope version of field in "organise_dirs"]
            :return: subProjDetails_l: pd.DataFrame [local scope DataFrame containing details and directories for the subjects analysed in this project]
            """

            # Create empty DataFrame with matching columns to the descData DataFrame initially
            expParameters = ['project_uid', 'subject_initials', 'imu_order', 'imu_num', 'imu_side', 'imu_loc',
                             'imu_orientation', 'affected_side']
            subTrialDetails_l = pd.DataFrame(columns=['data_dir'] + expParameters)
            subProjDetails_l = pd.DataFrame(columns=['data_dir'] + expParameters)

            num = 0
            for dataDir in dataDirs_l:
                num += 1
                uid = int(re.compile('(?:%s)([0-9]{3})+' % subjectIden).search(dataDir).group(1))
                subTrialDetails_l.loc[:, 'project_uid'] = pd.Series(uid)
                subTrialDetails_l.loc[:, 'subject_initials'] = 'TEST'
                subTrialDetails_l.loc[:, 'imu_order'] = 0
                subTrialDetails_l.loc[:, 'imu_num'] = 0
                subTrialDetails_l.loc[:, 'imu_side'] = 'nan'
                subTrialDetails_l.loc[:, 'imu_loc'] = 'loc' + str(num)
                subTrialDetails_l.loc[:, 'data_dir'] = dataDir
                subProjDetails_l = pd.concat([subProjDetails_l, subTrialDetails_l], sort=False)

            subProjDetails_l['unique_loc'] = subProjDetails_l['imu_side'].str.cat(subProjDetails_l['imu_loc'], sep='_')
            return subProjDetails_l.set_index(['project_uid', 'unique_loc'])

        subProjDetails = manage_data_dirs(dataDirs, subjectIden)
        return subProjDetails

    def import_operations(self, interpNan, dataDirs, taskDir, descDir, updateProg, updateMess, managedPopup,
                          **baseDirs):
        """
        All the operations associated with organising the data files and importing them
        :param dataDirs: List(String) [Directory for each of the data files selected]
        :param taskDir: Dict(List(String)/ String) [Dict of the different directory types]
        :param descDir: String [Directory of the project descriptor files]
        :param updateProg: Signal [Updates the GUI when files have been processed for each trial]
        :param updateMess: Signal [Updates the GUI when expected exceptions occur]
        :param baseDirs
        :return:
        """

        # REQUIRED METHODS (PROJECT SHOWS ERROR MESSAGE IF ERROR THROWN)
        self._allTasks = self.import_proj_tasks(taskDir)
        # Extract only the necessary subset of project details for this sub-project. Also organises the correct directories for the subjects tested
        subProjDetails = self.organise_dirs(dataDirs, self._subjectIden, **baseDirs)
        self._trialDict = self.gen_trials(subProjDetails)

        progressCount = 0
        for key in self._trialDict:

            updateMess.emit('Importing Data (trial %s)...' % key)
            progressCount += 1

            # OPTIONAL METHODS (TRIAL WILL STILL ATTEMPT TO RUN IF ERROR THROWN)
            try:
                self._trialDict[key].import_calibration()
            except BaseException as exception:
                updateMess.emit('Calibration files could not be imported for trial %s' % key)

            # REQUIRED METHODS (TRIAL DISCARDED IF ERROR THROWN HERE)
            keysRemoved = list()
            try:
                headerLines, dataWidth = self._trialDict[key].import_header_data()
                print('Finished Importing Header Data for trial %s (%.1f seconds)' % (key, self._globalTime))
                self._trialDict[key].import_trial_data(headerLines, dataWidth)
                self._trialDict[key].gen_imus()
                self._trialDict[key].import_imu_data(headerLines, dataWidth)
                print('Finished Generating IMUs and Importing Body Data for trial %s (%.1f seconds)' % (key, self._globalTime))
                self._trialDict[key].clean_trial_data()
                self._trialDict[key].make_tasks_unique()
                self._trialDict[key].partition_adc_imu_data()
                self._trialDict[key].continuous_counter()
                self._trialDict[key].interpolate_missing_rows(interpNan)
                self._trialDict[key].calc_time_stamp()
                self._trialDict[key].extract_orientation_data()
                print('Finished Cleaning Data for trial %s (%.1f seconds)' % (key, self._globalTime))
                updateProg.emit('import', progressCount, len(self._trialDict))
            except BaseException as exception:
                raise exception
                print('Trial %s removed from analysis due to exception: %s' % (key, exception))
                keysRemoved.append(key)
                continue

        for key in keysRemoved:
            self._trialDict.pop(key)

        return 'Completed Importing CSV Files', self.AdcRecorded

    # FEATURE CALCULATION
    @staticmethod
    def get_label_data(ratingData=None):
        """
        No rating data present in activity data processing so this method is ignored
        :param ratingData:
        :return:
        """
        return None

    @staticmethod
    def prepare_export(featDict=None, exportManager=None, ratingDf=None):
        """
        Method for preparing the data and passing it to the exporter class instance
        :param featDict:
        :param exportManager:
        :param ratingDf:
        :return:
        """
        return None




























