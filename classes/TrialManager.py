from abc import ABC, abstractmethod
import pandas as pd
import bisect
import csv
import math
import numpy as np

from classes.FilterManager import FilterManager
from classes.Orientation.Quaternion import Quaternion
from classes.Orientation.QuaternionAveraging import QuatAveraging


class TrialManager(ABC):
    """
    Base class for all trial types
    Implements several import methods common across all trial types
    Also implements pre-processing methods to standardise the units and filter the data
    """

    def __init__(self, projManager, assignTiming, imuClass, uid, subject_initials, imu_order, imu_num, imu_side,
                 imu_loc, imu_orientation, affected_side,
                 data_dir, rating_dir=None, timing_dir=None, mag_dir=None, gyr_dir=None, acc_dir=None):
        """
        One trial manager object created per subject tested
        The actual importing of files and processing of data is shared between children of this class and 'ImuManager'
        Directories passed here are first processed in the child classes
        Other Information that is passed is gathered from the trial_descriptor and contains details that are
        required for all trials
        :param projManager: ProjManager [The one instance of project manager created per analysis]
        :param assignTiming: Bool [Whether the project has a separate timings file which must be assigned to the data]
        :param imuClass: ImuManager Child [Pointer to the class which inherits ImuManager]
        :param uid: String [Subject Unique ID]
        :param subject_initials: String [Subject Initials]
        :param imu_order: pd.Series(String) [Collection of order imu recorded, one per IMU]
        :param imu_num: Int [Imu number]
        :param imu_side
        :param imu_orientation
        :param data_dir: pd.Series(String) [Collection of data file directories, one per IMU]
        :param imu_orientation: pd.Series(String) [Collection of imu orientations, one per IMU]
        :param rating_dir: pd.Series(String) [Collection of rating file directories, one per IMU]
        :param timing_dir: pd.Series(String) [Collection of timing file directories, one per IMU]
        :param mag_dir: pd.Series(String) [Collection of magnetometer calibration file directories, one per IMU]
        :param gyr_dir: pd.Series(String) [Collection of gyroscope calibration file directories, one per IMU]
        :param acc_dir: pd.Series(String) [Collection of acceleration calibration file directories, one per IMU]
        """

        super().__init__()

        # Compulsory parameters which must be defined in the project descriptor file (or generated in case of test file)
        self._projManager = projManager
        self._assignTimings = assignTiming
        self._imuClass = imuClass
        self._uid = uid
        self._subjectInitials = subject_initials.iloc[0]
        self._imuOrder = imu_order.to_dict()
        self._imuNum = imu_num.to_dict()
        self._imuSide = imu_side.to_dict()
        self._imuLoc = imu_loc.to_dict()
        self._imuOrient = imu_orientation.to_dict()
        self._affectedSide = affected_side.to_dict()

        # Directories: All optional except the data directory
        dirDict = dict()
        for directory in [data_dir, rating_dir, timing_dir, mag_dir, gyr_dir, acc_dir]:
            if directory is not None:
                dirDict[directory.name] = directory
        self._dirDf = pd.concat(dirDict, axis=1)

        # Trial-wide or IMU-wide data (if each IMU csv file contains a header)
        # There are general project details acquired either from the project details/ header/ locally
        self._headerDetails = {'uid': uid, 'numImus': len(data_dir), 'tasksRec': None, 'numTasks': None, 'timeStampRec': None,
                               'counterRec': None, 'sideRec': None, 'firmware': None, 'taskList': None}
        self._imuDetails = {'adcFreq': None, 'imuFreq': None, 'pins': [], 'loc': None, 'orientation': None}
        self._allImuDetails = dict()

        # Trial-wide DataFrames
        self._inertialData = pd.DataFrame()
        self._orientData = pd.DataFrame()
        self._adcData = pd.DataFrame()
        self._inertialInit = pd.DataFrame()
        self._orientInit = pd.DataFrame()
        self._adcInit = pd.DataFrame()
        self._filtInertialData = pd.DataFrame()
        self._filtAdcData = pd.DataFrame()

        # Trial-wide other data collections
        self._calOrient = dict()
        self._ratingData = None
        uniqueLocs = list(self._imuOrder.keys())
        self._imuDict = dict.fromkeys(uniqueLocs)  # Collection of ImuClass objects
        self._calImported = {'mag': False, 'gyr': False, 'acc': False}
        self._calDict = dict.fromkeys(uniqueLocs)
        self._adcRecorded = False

        # Maps incremental key (int) for every non-unique task recorded to a key (int) index of where that task belongs in a global list of all tasks
        self._taskKeyDict = dict()

    # OPTIONAL IMPORT METHODS
    @abstractmethod
    def import_rating(self):
        pass

    @abstractmethod
    def import_calibration(self):
        pass

    # REQUIRED IMPORT METHODS
    @staticmethod
    def calc_header_specs(headerEnd, dataPath):
        """
        Method for calculating the number of lines of the header section if this is not already
        provided in the ProjManager class
        :return:
        headerLines (Int) [Number of lines the header section takes up]
        dataWidth (Int) [Number of columns of the data section of the data]
        """

        with open(dataPath, 'rt') as csvfile:
            reader = csv.reader(csvfile)
            headerData = list()
            lineNo = 0
            while True:
                line = next(reader)
                if (headerEnd in line):
                    break
                headerData.append(line)
                lineNo += 1
                if lineNo > 50:
                    raise Exception('Header Identifier was not found')

        return headerData, lineNo, len(line)

    @staticmethod
    def standardise_header_data(headerDetails):
        """
        Method for standardising header data collected for different project types
        Ensures that all mandatory data is available and also converts it to the correct data type
        :return:
        """

        # Converting variables to their correct format
        headerDetails['numImus'] = int(headerDetails['numImus'])
        headerDetails['timeStampRec'] = headerDetails['timeStampRec'] == 'Yes'
        headerDetails['counterRec'] = headerDetails['counterRec'] == 'Yes'

        return headerDetails

    @abstractmethod
    def import_trial_data(self, headerLines, dataWidth):
        pass

    @abstractmethod
    def gen_imus(self):
        """
        General method for generating each IMU used in the trial if all imu data contained in one data file
        For trials where there is a separate datafile per IMU (see NOAH) then this method is overloaded
        """
        pass

    @abstractmethod
    def import_imu_data(self, headerLines, dataWidth):
        pass

    def clean_trial_data(self):

        for key in self._imuDict:
            self._imuDict[key].clean_imu_data(self._timeMultiplier)

    def make_tasks_unique(self):

        for key in self._imuDict:
            self._imuDict[key].make_tasks_unique()

    def partition_adc_imu_data(self):
        """
        Method for partitioning the data by separating the inertial and ADC data
        Passes the inertial keys and also the additional keys to perform this
        :return:
        """

        for key in self._imuDict:
            try:
                self._imuDict[key].partition_adc_imu_data()
            except BaseException as exceptioney:
                print('Error partitioning the imported data, perhaps the inertial or ADC headers '
                      'are not as expected: %s' % str(exceptioney))
                raise exceptioney

    def continuous_counter(self):
        """
        Method for converting the counter column (if present) to a continuously incrementing column
        Should be called after "partition_adc_imu_data" since requires inertial and adc data to be separated to
        find missing rows for each modality
        """

        for key in self._imuDict:
            self._imuDict[key].continuous_counter()

    def interpolate_missing_rows(self, interpNan):
        """
        Method for either interpolating or dropping the missing rows found using the included counter column
        Should be called after "continuous_counter" since this method finds the missing rows
        Should also be called after "integrate_task_timings" since assumes task column is present
        """

        for key in self._imuDict:
            self._imuDict[key].interpolate_missing_rows(interpNan)

    def calc_time_stamp(self):

        for key in self._imuDict:
            self._imuDict[key].calc_time_stamp()

    def extract_orientation_data(self):

        for key in self._imuDict:
            self._imuDict[key].extract_orientation_data()

    # IMU PRE-PROCESS METHODS
    @abstractmethod
    def presample_data(self):
        pass

    @abstractmethod
    def calibrate_data(self):
        pass

    def standardise_units(self):
        for key in self._imuDict:
            self._imuDict[key].standardise_units()

    def rotate_to_standard_frame(self):
        """
        Method that calls the apply rotation method for each IMU recorded from
        :return:
        """

        for key in self._imuDict:
            self._imuDict[key].rotate_to_standard_frame()

    @abstractmethod
    def flip_body_side(self):
        pass

    def calc_quaternion(self):
        """
        Method for calculating the orientation between IMUs using Madgwick Gradient Descent Algorithm
        :return:
        """

        for key in self._imuDict:
            self._imuDict[key].calc_quaternion()

    # TRIAL PRE-PROCESS METHODS
    def merge_imu_data(self):
        """
        Method for merging together all the IMU data into a single trial encompassing DataFrame for easier comparison,
        data analysis, and feature calculation purposes
        DataFrames are combined across columns using unique index (either time stamp or combination of task and time stamp)
        Additional data present in one IMU but not another is shown in the other IMU as NaN data
        """

        inertialDataDict = dict()
        quatDataDict = dict()
        adcDataDict = dict()
        trialInertialFreq = self.InertialFreq
        trialAdcFreq = self.AdcFreq

        for imu in self._imuDict:
            inertialDataDict[imu] = self._imuDict[imu].MergeInertialData(trialInertialFreq)
            quatDataDict[imu] = self._imuDict[imu].MergeQuatData(trialInertialFreq)
            adcDataDict[imu] = self._imuDict[imu].MergeAdcData(trialAdcFreq)
        self._taskKeyDict = self._imuDict[imu].TaskKeyDict

        self._inertialData = pd.concat(inertialDataDict, join='outer', axis=1, sort=False).reset_index(col_level=1, col_fill='ALL')
        self._inertialData.columns.set_names(['location', 'metric'], inplace=True)
        if self.QuatRecorded:
            self._orientData = pd.concat(quatDataDict, axis=1, sort=False).reset_index(col_level=1, col_fill='ALL')
            self._orientData.columns.set_names(['location', 'metric'], inplace=True)
        if self.AdcRecorded:
            self._adcData = pd.concat(adcDataDict, axis=1, sort=False).reset_index(col_level=1, col_fill='ALL')
            self._adcData.columns.set_names(['location', 'metric'], inplace=True)

    def missing_trial_data(self):
        """
        Method for dealing with (dropping or interpolating) missing rows which come about due to packet discrepancies
        between IMUs
        """

        self._inertialData.dropna(inplace=True)
        if self.QuatRecorded:
            self._orientData.dropna(inplace=True)
        if self.AdcRecorded:
            self._adcData.dropna(inplace=True)

    def simulate_torso_quaternion(self):
        """
        Method for simulating a unit quaternion for a torso imu which is not present
        """

        if not any('CENTRE_T' in imu for imu in self._orientData.columns.unique(level='location')):
            self._orientData.loc[:, ('CENTRE_T', 'quat')] = 0
            self._orientData.loc[:, ('CENTRE_T', 'quat')] = Quaternion.identity()

    def integrate_task_timings(self):
        """
        Method for assigning task based on the time stamp using user set times
        Also normalises the time stamp per task so that in same format as digitally assigned data
        This method is only called in the event that tasks are not already assigned to the data (using digital markers)
        Should be called after "calculate_timings" to ensure there is a time stamp column to use
        """

        def integrate_timings(timeStamp, timings, taskList):

            taskData = np.zeros(len(timeStamp.index.values), dtype=int)
            for index, row in timings.iterrows():
                if int(row['TaskKey']) in list(taskList.keys()):
                    startIdx = bisect.bisect_left(timeStamp.values, row['StartTime'])
                    endIdx = bisect.bisect_left(timeStamp.values, row['EndTime'])
                    taskData[startIdx: endIdx] = int(row['TaskKey'])
            return taskData

        def norm_time(norm_data):
            norm_data[('ALL', 'time_stamp')] = norm_data[('ALL', 'time_stamp')] - norm_data[('ALL', 'time_stamp')].min()
            return norm_data

        # Read user timings csv file and convert to units of ms (standard units used for time stamp)
        timingData = pd.read_csv(self._dirDf['timing_dir'].iloc[0])
        timingData[['StartTime', 'EndTime']] = timingData[['StartTime', 'EndTime']] * 1000

        # Integrate user timings as a new task column
        self._inertialData[('ALL', 'task')] = integrate_timings(self._inertialData[('ALL', 'time_stamp')], timingData, self._projManager.AllTasks)
        self._inertialData = self._inertialData.groupby(('ALL', 'task')).apply(norm_time)
        if self.QuatRecorded:
            self._orientData['task'] = integrate_timings(self._orientData[('ALL', 'time_stamp')], timingData, self._projManager.AllTasks)
            self._orientData = self._orientData.groupby(['task']).apply(norm_time)
        if self.AdcRecorded:
            self._adcData['task'] = integrate_timings(self._adcData[('ALL', 'time_stamp')], timingData, self._projManager.AllTasks)
            self._adcData = self._adcData.groupby(['task']).apply(norm_time)

    def organise_tasks(self, assignTimings):
        """
        Method for generating a second DataFrame column 'task_inst' which describes the number of times a task has uniquely appeared in the data
        Will also calculate the true 'task' column (int) if 'task key' column is present
        Should be called after "integrate_task_timings" method since this method requires a 'task' or 'task_key' column
        Also can finally map for task index to task string in this method since data has been merged (no memory risks)
        """

        def assign_task_instance_from_task(data):
            """
            Method for assigning a task instance column using the ordered task column (not ordered numerically)
            (1) Pre-allocating new column with all zeros in the DF
            (2) Using the pandas "diff" method to find where tasks are changed to avoid iterating over all rows
            (3) Finding all the unique tasks recorded for this trial
            (4) Creating a dict with a pre-allocated value of 0 for all tasks recorded
            (5) Iterating over all tasks to populate a column with the how many times this task has been seen
            :param data: pd.DataFrame [DataFrame containing data columns and task column]
            :return: data: pd.DataFrame [Same as above but includes task instance column]
            """

            data[('ALL', 'task_inst')] = np.zeros(len(data.index.values), dtype=int)
            orderedTasks_local = data.loc[data[('ALL', 'task')].diff() != 0, ('ALL', 'task')]
            taskInstances = dict.fromkeys(orderedTasks_local.unique(), 0)
            tasks = list(orderedTasks_local.values)
            indices = list(orderedTasks_local.index)
            nextIndices = indices[1:] + [data.index[-1]]
            for task, idx, nextIdx in zip(tasks, indices, nextIndices):
                taskInstances[task] += 1
                data.loc[idx:nextIdx, ('ALL', 'task_inst')] = taskInstances[task]
            return data

        def assign_task_instance_from_key(data, keyDict):
            """
            Method for assigning a task and task instance column using an ordered task key column (ordered numerically)
            :param data: pd.DataFrame [DataFrame containing data columns and task key column]
            :param keyDict: Dict [keys (int which corresponds to order task appeared), values (unique int which corresponds to index of the task name in list of all tasks)]
            :return: data: pd.DataFrame [Same as above but includes task and task instance column (drops task key column)]
            """

            data[('ALL', 'task_inst')] = np.zeros(len(data.index.values), dtype=int)
            data[('ALL', 'task')] = np.zeros(len(data.index.values), dtype=int)
            taskInstances = dict.fromkeys(keyDict.values(), 0)
            for taskKey in keyDict:
                taskInstances[keyDict[taskKey]] += 1
                data.loc[data[('ALL', 'task_key')] == taskKey, ('ALL', 'task_inst')] = taskInstances[keyDict[taskKey]]
                data.loc[data[('ALL', 'task_key')] == taskKey, ('ALL', 'task')] = int(keyDict[taskKey])
            data.drop('task_key', level=1, axis=1, inplace=True)
            return data

        # Generating a dictionary (keys are the task idx, values are the task) to be able to select the tasks from the data indices
        if self._headerDetails['trialTaskList'] is None:
            trialTaskDict = self._projManager.AllTasks
        else:
            trialTaskDict = dict(zip(range(len(self._headerDetails['trialTaskList'])), self._headerDetails['trialTaskList']))

        if assignTimings:
            self._inertialData = assign_task_instance_from_task(self._inertialData)
            if self.QuatRecorded:
                self._orientData = assign_task_instance_from_task(self._orientData)
            if self.AdcRecorded:
                self._adcData = assign_task_instance_from_task(self._adcData)
        else:
            self._inertialData = assign_task_instance_from_key(self._inertialData, self._taskKeyDict)
            if self.QuatRecorded:
                self._orientData = assign_task_instance_from_key(self._orientData, self._taskKeyDict)
            if self.AdcRecorded:
                self._adcData = assign_task_instance_from_key(self._adcData, self._taskKeyDict)

        # Map task indices to universal string tasks (contained in task list)
        orderedIdx = self._inertialData[('ALL', 'task')].unique()
        orderedTasks = [trialTaskDict[idx] for idx in orderedIdx]
        taskMap = dict(zip(orderedIdx, orderedTasks))
        self._inertialData[('ALL', 'task')] = self._inertialData[('ALL', 'task')].map(taskMap)
        if self.QuatRecorded:
            self._orientData[('ALL', 'task')] = self._orientData[('ALL', 'task')].map(taskMap)
        if self.AdcRecorded:
            self._adcData[('ALL', 'task')] = self._adcData[('ALL', 'task')].map(taskMap)

    def tare_orientation(self):
        """
        Method to tare the quaternion data across all axes or just heading axis depending on task performed
        """

        def inverse_quat_initialisation(quatData):
            quatData = quatData.transform(lambda x: x.__array__())
            quatMatrix = np.vstack(list(quatData.values))
            avQuat = QuatAveraging.average_quaternions(quatMatrix)
            return avQuat.conj()

        def euler_to_quat(rot_euler):
            if not np.any(rot_euler):
                rot_quat = Quaternion.identity()
            else:
                rot_quat = Quaternion.from_euler_angle_zyz_intrinsic(rot_euler[0], rot_euler[1], rot_euler[2])
            return rot_quat

        # Tare each of the IMUs and orient them into the correct starting location depending on tare task
        nonTareTasks = ['no task', 'mvc-biceps', 'mvc-flexor digitorum superficialis']
        tareTasks = [task for task in list(self._orientData[('ALL', 'task')].unique()) if task not in nonTareTasks]
        calTasks = ['cal-tare imus (semi-pronated arm vertical)', 'cal-tare imus (semi-pronate arm 90 degrees from vertical)']
        grossTasks = ['flexor synergy', 'extensor synergy', 'hand to lumbar spine', 'shoulder flexion (0-90)',
                      'shoulder flexion (90-180)', 'shoulder abduction', 'coordination-speed']
        nextTasks = tareTasks[1:] + [None]
        for tareTask, nextTask in zip(tareTasks, nextTasks):

            # Set up relative arm rotations depending on task performed
            if tareTask in ['cal-tare imus (semi-pronated arm vertical)']:
                uaRotate = np.array((-math.pi / 2.0, 0, 0))
                laRotate = np.array((0, 0, 0))
            elif tareTask == 'cal-tare imus (semi-pronate arm 90 degrees from vertical)':
                uaRotate = np.array((-math.pi / 2.0, 0, math.pi / 2.0))
                laRotate = np.array((0, 0, 0))
            else:    # Covers any tasks which are not tared in a particular position or tared in the neutral position
                uaRotate = np.array((0, 0, 0))
                laRotate = np.array((0, 0, 0))

            # Set up tare values depending on whether initialisation or regular task performed
            quatInverseDict = dict()
            for uniqueLoc in list(self._imuLoc.keys()):
                if tareTask in calTasks:
                    quatInverseDict[uniqueLoc] = inverse_quat_initialisation(self._orientData.loc[self._orientData[('ALL', 'task')] == tareTask, (uniqueLoc, 'quat')])
                elif tareTask in grossTasks:
                    quatInverseDict[uniqueLoc] = self._orientData.loc[self._orientData[('ALL', 'task')] == tareTask, (uniqueLoc, 'quat')].iloc[0].to_twist_decomposition(np.array([0, 0, -1])).conj()
                else:
                    quatInverseDict[uniqueLoc] = self._orientData.loc[self._orientData[('ALL', 'task')] == tareTask, (uniqueLoc, 'quat')].iloc[0].conj()

            # Find the rotation for the right and left sides of the body
            rotationEuler = dict()
            rotationEuler['RIGHT_UA'] = uaRotate
            rotationEuler['RIGHT_LA'] = laRotate
            for key, rot in zip(['LEFT_UA', 'LEFT_LA'], [uaRotate, laRotate]):
                if isinstance(rot, np.ndarray):
                    rotationEuler[key] = rot * np.array((-1, -1, 1))
                else:
                    rotationEuler[key] = list()
                    for subRot in rot:
                        rotationEuler[key].append(subRot * np.array((-1, -1, 1)))

            # Calculate Quaternion for ZYX order intrinsic euler angles
            rotation = dict()
            for key in ['RIGHT_UA', 'RIGHT_LA', 'LEFT_UA', 'LEFT_LA']:
                if isinstance(rotationEuler[key], np.ndarray):
                    rotation[key] = euler_to_quat(rotationEuler[key])
                else:
                    rotation[key] = euler_to_quat(rotationEuler[key][0]) * euler_to_quat(rotationEuler[key][1])

            # Apply both tare values and relative rotations to the orientation data
            firstIdx = (self._orientData[('ALL', 'task')] == tareTask).idxmax()
            if nextTask is not None:
                nextIdx = (self._orientData[('ALL', 'task')] == nextTask).idxmax() - 1
            else:
                nextIdx = self._orientData[('ALL', 'task')].iloc[-1]

            if 'CENTRE_T' in list(self._imuLoc.keys()):
                self._orientData.loc[firstIdx:nextIdx, ('CENTRE_T', 'quat')] = self._orientData.loc[firstIdx:nextIdx, ('CENTRE_T', 'quat')].transform(lambda x: quatInverseDict['CENTRE_T'] * x)

            for rotKey in ['RIGHT_UA', 'LEFT_UA']:
                if rotKey in list(self._imuLoc.keys()):
                    self._orientData.loc[firstIdx:nextIdx, (rotKey, 'quat')] = self._orientData.loc[firstIdx:nextIdx, (rotKey, 'quat')].transform(lambda x: rotation[rotKey] * (quatInverseDict[rotKey] * x))

            for laRotKey, uaRotKey in zip(['RIGHT_LA', 'LEFT_LA'], ['RIGHT_UA', 'LEFT_UA']):
                if laRotKey in list(self._imuLoc.keys()):
                    self._orientData.loc[firstIdx:nextIdx, (laRotKey, 'quat')] = self._orientData.loc[firstIdx:nextIdx, (laRotKey, 'quat')].transform(lambda x: (rotation[uaRotKey] * rotation[laRotKey]) * (quatInverseDict[laRotKey] * x))

    def offset_orientation(self, directory):
        """
        Method to apply user set heading offset rotations to the tared data
        :param directory: Directory of the offset orientation data [String]
        """

        grossTasks = ['flexor synergy', 'extensor synergy', 'hand to lumbar spine', 'shoulder flexion (0-90)',
                      'shoulder abduction', 'shoulder flexion (90-180)', 'coordination-speed']
        offsetTasks = [task for task in list(self._orientData[('ALL', 'task')].unique()) if task in grossTasks]
        offsetData = pd.read_csv(directory, header=0, index_col=['task', 'joint_offset'])

        for task in offsetTasks:
            for location in [uniqueLoc for uniqueLoc in self._orientData.columns.unique(level='location') if uniqueLoc != 'ALL']:
                if 'CENTRE' in location:
                    continue
                shoulderOffsetData = Quaternion.from_euler_angle_zyz_intrinsic(math.radians(-int(offsetData.loc[(task, 'shoulder otw'), str(self._uid)])), 0, 0)
                elbowOffsetData = Quaternion.from_euler_angle_zyz_intrinsic(math.radians(-int(offsetData.loc[(task, 'elbow otw'), str(self._uid)])), 0, 0)
                if 'LEFT' in location:
                    shoulderOffsetData = shoulderOffsetData.conj()
                    elbowOffsetData = elbowOffsetData.conj()
                if 'UA' in location:
                    self._orientData.loc[self._orientData[('ALL', 'task')] == task, (location, 'quat')] = \
                    self._orientData.loc[self._orientData[('ALL', 'task')] == task, (location, 'quat')].transform(lambda x: shoulderOffsetData * x)
                elif 'LA' in location:
                    self._orientData.loc[self._orientData[('ALL', 'task')] == task, (location, 'quat')] = \
                    self._orientData.loc[self._orientData[('ALL', 'task')] == task, (location, 'quat')].transform(lambda x: shoulderOffsetData * elbowOffsetData * x)

    def remove_initialisation_routines(self):
        """
        Method for removing all of the initialisation routine tasks (orientation and myographic) from the activity tasks
        This will throw error if local task list is not in same format as global task list (all tasks will be
        identified as calibration tasks)
        """

        # Find the task which occurs in the data but not in the global project list of possible clinical tasks
        initTasks = np.setdiff1d(self._inertialData[('ALL', 'task')].unique(), list(self._projManager.AllTasks.values()))

        # Remove initialisation tasks from the normal data
        for initTask in initTasks:
            self._inertialData = self._inertialData.loc[self._inertialData[('ALL', 'task')] != initTask]
            if self.QuatRecorded:
                self._orientData = self._orientData.loc[self._orientData[('ALL', 'task')] != initTask]
            if self.AdcRecorded:
                self._adcData = self._adcData.loc[self._adcData[('ALL', 'task')] != initTask]

    def filter_data(self, filterBounds, filtBool, wavDenoise, notchFilt):
        """
        Method for filtering all data
        :param filterBounds: Dict(String/Bool) [Boundaries of filtering and whether to notch filter the ADC data]
        :param filtBool: Bool [Whether or not to filter any of the data]
        :param wavDenoise: Bool [Whether or not to perform wavelet denoising on the data]
        :param notchFilt: Bool [Whether to apply a notch filter to the ADC data to remove harmonic noise]
        """

        self._filtInertialData = self._inertialData.copy()
        self._filtAdcData = self._adcData.copy()
        if not filtBool:
            return
        for imu in self._imuDict.keys():
            self._filtInertialData[imu] = self._filtInertialData[imu].apply(func=self._imuDict[imu].filter_data, args=('inertial', filterBounds, False))
            if self._imuDict[imu].AdcRecorded:
                self._filtAdcData[imu] = self._filtAdcData[imu].apply(func=self._imuDict[imu].filter_data, args=('adc', filterBounds, notchFilt))

    def save_quat_data(self, updateDataReady):
        """
        Method for saving the orientation data if it was calculated
        :param updateDataReady: Signal [safe-thread method of emitting data back to the main thread]
        """

        updateDataReady.emit('quaternion', self._uid, self._orientData)

    # PROPERTIES
    def InertialTrialDf(self, projInertialFreq, filt):
        """
        Property to return trial wide inertial data
        :param projInertialFreq: Float
        :param filt: Bool [Whether to return filtered or unfiltered data]
        :return data: pd.DataFrame{index: task/ task_inst, columns: location/ metric}
        """

        if filt:
            data = FilterManager.memory_reduce(self._filtInertialData)
            del self._filtInertialData
        else:
            data = FilterManager.memory_reduce(self._inertialData)
            del self._inertialData
        if int(projInertialFreq) < int(self.InertialFreq):
            data = FilterManager.downsample(data, int(int(self.InertialFreq) / int(projInertialFreq)))
        return data

    def AdcTrialDf(self, projAdcFreq, filt):
        """
        Property to return trial wide adc data
        :param projAdcFreq: Float
        :param filt: Bool [Whether to return filtered or unfiltered data]
        :return data: pd.DataFrame{index: task/ task_inst, columns: location/ metric}
        """

        if not self.AdcRecorded:
            return None
        if filt:
            data = FilterManager.memory_reduce(self._filtAdcData)
            del self._filtAdcData
        else:
            data = FilterManager.memory_reduce(self._adcData)
            del self._adcData
        if int(projAdcFreq) < int(self.AdcFreq):
            data = FilterManager.downsample(data, int(int(self.AdcFreq) / int(projAdcFreq)))
        return data

    def OrientTrialDf(self, projInertialFreq):
        """
        Property to return trial wide adc data
        :param projInertialFreq: Float
        :return data: pd.DataFrame{index: task/ task_inst, columns: location/ metric}
        """

        if not self.QuatRecorded:
            return None
        data = self._orientData
        del self._orientData
        if int(projInertialFreq) < int(self.InertialFreq):
            data = FilterManager.downsample(data, int(int(self.InertialFreq) / int(projInertialFreq)))
        return data

    @property
    def ImuDict(self):
        return self._imuDict

    @property
    def RatingData(self):
        return self._ratingData

    @property
    def InertialFreq(self):
        """
        Returns the mimimum inertial frequency recorded across all imus for the trial
        :return:
        """
        return min([self._imuDict[imu].InertialFreq for imu in self._imuDict])

    @property
    def AdcFreq(self):
        """
        Returns the mimimum adc frequency recorded across all imus for the trial
        :return:
        """
        if all(self._imuDict[imu].AdcFreq is None for imu in self._imuDict):
            return None
        return min([self._imuDict[imu].AdcFreq for imu in self._imuDict if self._imuDict[imu].AdcFreq is not None])

    @property
    def AdcRecorded(self):

        for imu in self._imuDict:
            if self._imuDict[imu].AdcRecorded:
                return True
        return False

    @property
    def QuatRecorded(self):

        for imu in self._imuDict:
            if self._imuDict[imu].QuatRecorded:
                return True
        return False

    @property
    def CalImported(self):
        if any(self._calImported.values()):
            return True
        return False


class SingleFileTrialManager(TrialManager):

    def __init__(self, projManager, assignTimings, imuClass, uid, trialDetails):
        """
        One trial manager object created per subject tested (collection of data and label files associated with this
        trial are collected and operated on here)
        :param projManager: ProjManager [The one instance of project manager created per analysis]
        :param assignTimings: LabelManager [The one instance of label manager created per analysis]
        :param imuClass: ImuManager Child [Pointer to the class which inherits ImuManager]
        :param uid: String [Subject Unique ID]
        :param trialDetails: pd.DataFrame
        """

        super().__init__(projManager, assignTimings, imuClass, uid, **trialDetails)

    # OPTIONAL IMPORT METHODS
    @abstractmethod
    def import_rating(self):
        pass

    @abstractmethod
    def import_calibration(self):
        pass

    # REQUIRED IMPORT METHODS
    def import_header_data(self):
        """
        Overloads the abstract method to provide information contained in the header
        Blue trials use a labelled header and se the "read_labelled_header" method is implemented
        :return:
        headerLines: number of lines in the header
        dataWidth: number of rows in the data section
        """

        headerData, headerLines, dataWidth = self.calc_header_specs(self._headerEnd, self._dirDf['data_dir'].iloc[0])
        try:
            self.read_header(headerData, headerLines)
        except BaseException as exception:
            print('Error reading the Header: %s' % exception)
            raise exception
        try:
            self._headerDetails = self.standardise_header_data(self._headerDetails)
        except BaseException as exception:
            print('Error standardising the Header: %s' % exception)
            raise exception

        return headerLines, dataWidth

    def read_header(self, headerData, headerLines):

        for line in range(headerLines):
            if 'HeaderLine1' in headerData[line]:
                self._headerDetails = LabHeaderImpMethods.horz_one(self._headerDetails, self._headerImport['HeaderLine1'],
                                                                   headerData[line])
            if 'TaskLine2' in headerData[line]:
                self._headerDetails = LabHeaderImpMethods.horz_all(self._headerDetails, self._headerImport['TaskLine2'],
                                                                   headerData[line],)
            if 'ImuConfigTitle' in headerData[line]:
                for uniqueLoc in self._imuDict.keys():
                    self._allImuDetails[uniqueLoc] = LabHeaderImpMethods.vert_one_horz_zero(self._imuDetails, self._headerImport['ImuConfig'],
                                                                            headerData[line], headerData[line + self._imuOrder[uniqueLoc] + 1])
                    self._allImuDetails[uniqueLoc] = LabHeaderImpMethods.vert_one_horz_limits(self._imuDetails, self._headerImport['ImuConfig-Pins'],
                                                                            headerData[line], headerData[line + self._imuOrder[uniqueLoc] + 1])

    def import_trial_data(self, headerLines, dataWidth):
        """
        General method for reading the trial body of data if all contained in one data file
        For trials where there is a separate datafile per IMU (see NOAH) then this method is overloaded
        :param headerLines: Int [Number of lines of the header section (to be ignored when importing)]
        :param dataWidth: Int [Expected width of the body section of the data]
        :return:
        datafile: The body of data extracted from the data file
        """

        # Read csv file using the expected data width
        self._allData = pd.read_csv(self._dirDf['data_dir'].iloc[0], header=headerLines, skip_blank_lines=False,
                                    usecols=range(dataWidth))

    def gen_imus(self):

        for uniqueLoc in self._imuDict.keys():
            imuData = self._allData.loc[self._allData[self._eImuIdentifier] == self._imuOrder[uniqueLoc], :].copy()
            imuData.drop([self._eImuIdentifier], axis=1, inplace=True)
            imuManager = self._imuClass(self._imuOrder[uniqueLoc], self._allImuDetails[uniqueLoc], self._sMeta.copy(),
                                        self._sMetrics.copy(), self._transCol, uniqueLoc,
                                        firmware=self._headerDetails['firmware'], imuData=imuData)
            self._imuDict[uniqueLoc] = imuManager

    def import_imu_data(self, headerLines, dataWidth):
        """
        Header lines and data width are passed to this method although they aren't needed for this project type since
        the trial-scope data has already been read
        :param headerLines:
        :param dataWidth:
        :return:
        """

        for key in self._imuDict:
            self._imuDict[key].import_imu_data()


class BlueTrialManager(SingleFileTrialManager):
    """
    This class inherits from the SingleFileTrialManager base class
    Assumes a configuration of 1-4 IMUs placed on the arm in any of the following positions:
    (1) Hand (2) Lower Arm (3) Upper Arm (4) Chest
    """

    def __init__(self, projManager, assignTimings, imuClass, uid, trialDetails):
        """
        One trial manager object created per subject tested (collection of data and label files associated with this
        trial are collected and operated on here)
        :param projManager: ProjManager [The one instance of project manager created per analysis]
        :param assignTimings: LabelManager [The one instance of label manager created per analysis]
        :param imuClass: ImuManager Child [Pointer to the class which inherits ImuManager]
        :param uid: String [Subject Unique ID]
        :param trialDetails: pd.DataFrame
        """

        super().__init__(projManager, assignTimings, imuClass, uid, trialDetails)

        # These are the expected useful metric headings to extract from the data
        # "Counter" column will be removed depending on the IMU firmware
        # "IMU Num" column will be removed when the data is pass to the respective IMU Class
        self._eMetrics = ['Gyro X', 'Gyro Y', 'Gyro Z',
                          'Mag X', 'Mag Y', 'Mag Z', 'Acc X', 'Acc Y', 'Acc Z', 'ADC 1', 'ADC 2', 'ADC 3', 'ADC 4',
                          'ADC 5', 'ADC 6', 'ADC 7', 'ADC 8']
        self._sMetrics = ['gyr_x', 'gyr_y', 'gyr_z',
                          'mag_x', 'mag_y', 'mag_z', 'acc_x', 'acc_y', 'acc_z', 'adc_1', 'adc_2', 'adc_3', 'adc_4',
                          'adc_5', 'adc_6', 'adc_7', 'adc_8']
        self._eMeta = ['Time Stamp', 'Counter', 'IMU Num', 'Task', 'Task Stage']
        self._sMeta = ['time_stamp', 'counter', 'imu_num', 'task', 'task_stage']
        self._transCol = dict(zip(self._eMeta + self._eMetrics, self._sMeta + self._sMetrics))
        self._eImuIdentifier = 'IMU Num'

        # All raw imu data imported from a single csv file (This trial type imports all data into one CSV file
        self._allData = None

        # These parameters define how to read and import the header section of the data file
        self._headerType = 'Labelled'
        self._headerEnd = 'Task Stage'
        self._headerImport = {'HeaderLine1': {'labels': ['No. Tasks', 'True Time Stamp', 'Counter', 'Side', 'Firmware'],
                                              'vars': ['numTasks', 'timeStampRec', 'counterRec', 'sideRec',
                                                       'firmware']},
                              'TaskLine2': {'label': 'TaskLine2', 'var': 'trialTaskList'},
                              'ImuConfig': {'labels': ['ADC Freq', 'IMU Freq', 'Location', 'Orientation'],
                                            'vars': ['adcFreq', 'imuFreq', 'loc', 'orientation']},
                              'ImuConfig-Pins': {'startLabel': 'Pin1', 'endLabel': 'Pin8', 'var': 'pins'}}

        # These parameters define how to read the data section and process the time stamp to convert to ms
        self._timeMultiplier = 10

    # OPTIONAL IMPORT METHODS
    def import_rating(self):
        """
        Method for reading the rating csv file
        """

        # Read in the raw clinician scores and then convert them into a series for ease of indexing
        ratingFile = pd.read_csv(self._dirDf['rating_dir'].iloc[0], names=('task', 'Score'),
                                 nrows=21)
        self._ratingData = pd.Series(data=ratingFile['Score'].values, index=ratingFile['task'].values)

    def import_calibration(self):
        """
        Method for importing calibration data from the directories previously found in the 'organise_dirs' method
        Cycles though each imu and finds the appropriate trial calibration data
        """

        def read_txt_file(path):
            with open(path, 'r') as f:
                calData = np.array(list(map(float, f.read().split('\n'))))
                return calData

        def read_csv_file(path, uid):
            csvData = pd.read_csv(path, index_col='UID')
            if str(uid) in list(csvData.index):
                return csvData.loc[str(uid)].values
            else:
                return csvData.loc['noTrial'].values

        for imuLoc in list(self._imuDict.keys()):

            calData = dict()
            if self._dirDf.loc[imuLoc, 'acc_dir'] is not None:
                calData['acc'] = read_txt_file(self._dirDf.loc[imuLoc, 'acc_dir'])
                self._calImported['acc'] = True
            if self._dirDf.loc[imuLoc, 'gyr_dir'] is not None:
                calData['gyr'] = read_txt_file(self._dirDf.loc[imuLoc, 'gyr_dir'])
                self._calImported['gyr'] = True
            if self._dirDf.loc[imuLoc, 'mag_dir'] is not None:
                calData['mag'] = read_csv_file(self._dirDf.loc[imuLoc, 'mag_dir'], self._uid)
                self._calImported['mag'] = True
            self._calDict[imuLoc] = calData

    # LOCAL PRE-PROCESS METHODS
    def presample_data(self):

        for key in self._imuDict:
            if int(self._headerDetails['firmware']) > 6:
                self._imuDict[key].presample_data()

    def calibrate_data(self):

        for key in self._imuDict:
            self._imuDict[key].calibrate_data(self._calDict[key])

    def flip_body_side(self):
        """
        Method that calls the apply body side inversion to the right side for each IMU recorded from left side
        Not applicable to Trial types which place sensors or both sides of body (i.e. NOAH)
        """

        imuLocs = list(self._imuDict.keys())
        if self._headerDetails['sideRec'] == 'Left':
            for imuLoc in imuLocs:
                self._imuDict[imuLoc].flip_body_side()

                newImuLoc = imuLoc.replace('LEFT', 'RIGHT')
                self._affectedSide[newImuLoc] = self._affectedSide.pop(imuLoc)
                self._allImuDetails[newImuLoc] = self._allImuDetails.pop(imuLoc)
                self._calDict[newImuLoc] = self._calDict.pop(imuLoc)
                self._imuDict[newImuLoc] = self._imuDict.pop(imuLoc)
                self._imuLoc[newImuLoc] = self._imuLoc.pop(imuLoc)
                self._imuNum[newImuLoc] = self._imuNum.pop(imuLoc)
                self._imuOrder[newImuLoc] = self._imuOrder.pop(imuLoc)
                self._imuOrient[newImuLoc] = self._imuOrient.pop(imuLoc)
                self._imuSide[newImuLoc] = self._imuSide.pop(imuLoc)


class MultiFileTrialManager(TrialManager):

    def __init__(self, projManager, assignTimings, imuClass, uid, trialDetails):

        super().__init__(projManager, assignTimings, imuClass, uid, **trialDetails)

    # OPTIONAL IMPORT METHODS
    @abstractmethod
    def import_rating(self):
        pass

    @abstractmethod
    def import_calibration(self):
        pass

    # REQUIRED IMPORT METHODS
    def import_header_data(self):
        """
        Overloads the abstract method to provide information contained in the header
        Blue trials use a labelled header and se the "read_labelled_header" method is implemented
        :return:
        headerLines: number of lines in the header
        dataWidth: number of columns in the data section
        """

        headerData, headerLines, dataWidth = self.calc_header_specs(self._headerEnd, self._dirDf['data_dir'].iloc[0])

        try:
            self._headerDetails = self.standardise_header_data(self._headerDetails)
        except BaseException as exception:
            print('Error standardising the Header: %s' % exception)
            raise exception

        return headerLines, dataWidth

    def import_trial_data(self, headerLines, dataWidth):
        """
        No trial-wide data to import for this project type so this method is ignored
        :param headerLines:
        :param dataWidth:
        :return:
        """
        pass

    def gen_imus(self):

        for uniqueLoc in self._imuDict.keys():
            imuManager = self._imuClass(self._imuOrder[uniqueLoc], self._allImuDetails[uniqueLoc], self._sMeta.copy(),
                                        self._sMetrics.copy(), self._transCol.copy(), uniqueLoc)
            self._imuDict[uniqueLoc] = imuManager

    def import_imu_data(self, headerLines, dataWidth):

        for key in self._imuDict:
            self._imuDict[key].import_imu_data(dataDir=self._dirDf.loc[key, 'data_dir'], headerLines=headerLines,
                                               dataWidth=dataWidth)


class MtTrialManager(MultiFileTrialManager):
    """
    This class inherits from the MultiFileTrialManager base class
    Assumes a configuration of 1-4 IMUs placed on the body in the following configurations:
    (1) Left Wrist (2) Right Wrist (3) Left Ankle (4) Right Ankle
    """

    def __init__(self, projManager, assignTimings, imuClass, uid, trialDetails):
        """
        One trial manager object created per subject tested (collection of data and label files associated with this
        trial are collected and operated on here)
        :param projManager: ProjManager [The one instance of project manager created per analysis]
        :param assignTimings: LabelManager [The one instance of label manager created per analysis]
        :param imuClass: ImuManager Child [Pointer to the class which inherits ImuManager]
        :param uid: String [Subject Unique ID]
        :param trialDetails: pd.DataFrame
        """

        super().__init__(projManager, assignTimings, imuClass, uid, trialDetails)

        # These are the useful data headings to be extracted from the data
        self._eMetrics = ['Acc_X', 'Acc_Y', 'Acc_Z', 'Gyr_X', 'Gyr_Y', 'Gyr_Z', 'Roll', 'Pitch', 'Yaw']
        self._sMetrics = ['acc_x', 'acc_y', 'acc_z', 'gyr_x', 'gyr_y', 'gyr_z', 'roll', 'pitch', 'yaw']
        self._eMeta = ['PacketCounter']
        self._sMeta = ['counter']

        # These parameters define how to import and read the header section of the data file
        # Also includes all the header details locally since no header data expected for this trial type
        self._headerType = None
        self._headerEnd = 'PacketCounter'
        self._headerDetails['tasksRec'] = 'noah_instructed_protocol_v2'
        self._headerDetails['timeStampRec'] = 'No'
        self._headerDetails['counter'] = 'Yes'
        # No trial scope task list for this trial, only project scope task list
        self._headerDetails['trialTaskList'] = None

        if 'LEFT_LA' in self._imuDict.keys():
            self._allImuDetails['LEFT_LA'] = {'adcFreq': '100', 'imuFreq': '100', 'orientation': 'Downwards', 'loc': self._imuLoc['LEFT_LA'], 'pins': []}
        if 'RIGHT_LA' in self._imuDict.keys():
            self._allImuDetails['RIGHT_LA'] = {'adcFreq': '100', 'imuFreq': '100', 'orientation': 'Downwards',
                                           'loc': self._imuLoc['RIGHT_LA'], 'pins': []}
        if 'LEFT_LL' in self._imuDict.keys():
            self._allImuDetails['LEFT_LL'] = {'adcFreq': '100', 'imuFreq': '100', 'orientation': 'Downwards',
                                          'loc': self._imuLoc['LEFT_LL'], 'pins': []}
        if 'RIGHT_LL' in self._imuDict.keys():
            self._allImuDetails['RIGHT_LL'] = {'adcFreq': '100', 'imuFreq': '100', 'orientation': 'Downwards', 'loc': self._imuLoc['RIGHT_LL'], 'pins': []}
        if 'LEFT_LA' not in self._imuDict.keys() and 'RIGHT_LA' not in self._imuDict.keys() and 'LEFT_LL' not in self._imDict.keys() and 'RIGHT_LL' not in self._imuDict.keys():
            for key in self._imuLoc.keys():
                self._allImuDetails[key] = {'adcFreq': '100', 'imuFreq': '100', 'orientation': 'Downwards', 'loc': self._imuLoc[key], 'pins': []}

        # These parameters define how to process the time stamp to convert to ms
        self._timeMultiplier = 10

    # OPTIONAL IMPORT METHODS
    def import_rating(self):
        pass

    def import_calibration(self):
        """
        Calibration data is already applied during import with the GUI
        """
        pass

    # LOCAL PRE-PROCESSING METHODS
    def presample_data(self):
        """
        Not currently implemented
        """
        pass

    def calibrate_data(self):
        """
        Calibration data is already applied during import with the GUI
        """
        pass

    def flip_body_side(self, orientSettings):
        """
        This method makes no sense in the context of this trial since sensors are placed on both sides of the body
        """
        pass


class AppleTrialManager(MultiFileTrialManager):
    """
    This class inherits from the MultiFileTrialManager base class
    Assumes a configuration of 1-4 IMUs placed on the body in the following configurations:
    (1) Left Wrist (2) Right Wrist (3) Left Ankle (4) Right Ankle
    """

    def __init__(self, projManager, assignTimings, imuClass, uid, trialDetails):
        """
        One trial manager object created per subject tested (collection of data and label files associated with this
        trial are collected and operated on here)
        :param projManager: ProjManager [The one instance of project manager created per analysis]
        :param assignTimings: LabelManager [The one instance of label manager created per analysis]
        :param imuClass: ImuManager Child [Pointer to the class which inherits ImuManager]
        :param uid: String [Subject Unique ID]
        :param trialDetails: pd.DataFrame
        """

        super().__init__(projManager, assignTimings, imuClass, uid, **trialDetails)

        # These are the useful data headings to be extracted from the data
        self._eMetrics = ['accelerationX', 'accelerationY', 'accelerationZ', 'rotationRateX', 'rotationRateY', 'rotationRateZ', 'attitudeR', 'attitudeP', 'attitudeY']
        self._sMetrics = ['acc_x', 'acc_y', 'acc_z', 'gyr_x', 'gyr_y', 'gyr_z', 'roll', 'pitch', 'yaw']
        self._eMeta = ['ticks']
        self._sMeta = ['time_stamp']

        # These parameters define how to import and read the header section of the data file
        # Also includes all the header details locally since no header data expected for this trial type
        self._headerType = None
        self._headerEnd = 'PacketCounter'
        self._headerDetails['tasksRec'] = 'noah_instructed_protocol_v2'
        self._headerDetails['timeStampRec'] = 'Yes'
        self._headerDetails['counter'] = 'No'
        # No trial scope task list for this trial, only project scope task list
        self._headerDetails['trialTaskList'] = None

        self._allImuDetails['LEFT_LA'] = {'adcFreq': '100', 'imuFreq': '100', 'orientation': 'Downwards',
                                          'loc': self._imuLoc['LEFT_LA'], 'pins': []}
        self._allImuDetails['RIGHT_LA'] = {'adcFreq': '100', 'imuFreq': '100', 'orientation': 'Downwards',
                                           'loc': self._imuLoc['RIGHT_LA'], 'pins': []}
        self._allImuDetails['LEFT_LL'] = {'adcFreq': '100', 'imuFreq': '100', 'orientation': 'Downwards',
                                          'loc': self._imuLoc['LEFT_LL'], 'pins': []}
        self._allImuDetails['RIGHT_LL'] = {'adcFreq': '100', 'imuFreq': '100', 'orientation': 'Downwards',
                                           'loc': self._imuLoc['RIGHT_LL'], 'pins': []}

        # These parameters define how to process the time stamp to convert to ms
        self._timeMultiplier = 0.0001

    # OPTIONAL IMPORT METHODS
    def import_rating(self):
        pass

    def import_calibration(self):
        """
        Calibration data is already applied during import with the GUI
        """
        pass

    # LOCAL PRE-PROCESSING METHODS
    def presample_data(self):
        """
        Not currently implemented
        """
        pass

    def calibrate_data(self):
        """
        Calibration data is already applied during import with the GUI
        """
        pass

    def flip_body_side(self):
        """
        This method makes no sense in the context of this trial since sensors are placed on both sides of the body
        """
        pass


class LabHeaderImpMethods():

    @staticmethod
    def horz_one(headerDetails, headerSettings, line):

        for label, var in zip(headerSettings['labels'], headerSettings['vars']):
            if label in line:
                headerDetails[var] = line[line.index(label) + 1]
            else:
                print('expected label %s in the header data but this could not be found' % label)
        return headerDetails

    @staticmethod
    def horz_all(headerDetails, headerSettings, line):

        if headerSettings['label'] in line:
            headerDetails[headerSettings['var']] = line[line.index(headerSettings['label'])+1:]
        else:
            print('expected label %s in the header data but this could not be found' % headerSettings['label'])
        return headerDetails

    @staticmethod
    def vert_one_horz_zero(headerDetails, headerSettings, line, nextLine):

        for label, var in zip(headerSettings['labels'], headerSettings['vars']):
            if label in line:
                headerDetails[var] = nextLine[line.index(label)]
        return headerDetails.copy()

    @staticmethod
    def vert_one_horz_limits(headerDetails, headerSettings, line, nextLine):

        startLabel = headerSettings['startLabel']
        endLabel = headerSettings['endLabel']
        if startLabel in line and endLabel in line:
            headerDetails[headerSettings['var']] = nextLine[line.index(startLabel):line.index(endLabel)+1]
        return headerDetails.copy()
