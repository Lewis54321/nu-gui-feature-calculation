from PyQt5.QtCore import *

import traceback
import sys


class WorkerClass(QObject):

    updateMess = pyqtSignal(str)
    updateProg = pyqtSignal(str, int, int)
    updateDataReady = pyqtSignal(str, str, str, object)
    finished = pyqtSignal(str, bool)
    managedPopup = pyqtSignal(str)
    critExcept = pyqtSignal(Exception, str)

    def __init__(self, fn, *args, **nargs):

        super(self.__class__, self).__init__()
        # Store constructor arguments (re-used for processing)
        self.fn = fn
        self.args = args
        self.nargs = nargs

    @pyqtSlot()
    def runFunction(self):

        try:
            message, status = self.fn(*self.args, **self.nargs, updateProg=self.updateProg, updateMess=self.updateMess,
                                      updateDataReady=self.updateDataReady, managedPopup=self.managedPopup)
            self.finished.emit(message, status)

        except BaseException as exception:
            traceback.print_exc()
            exc_type = str(sys.exc_info()[0].__name__)
            self.critExcept.emit(exception, exc_type)



