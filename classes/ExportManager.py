import pickle
import os
import pyarrow.parquet as parq
import pyarrow as pa
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from abc import ABC, abstractmethod

from classes.FilterManager import FilterManager


class ExportManager(ABC):

    def __init__(self, baseExportDir, dateTime):
        """
        Constructor for ExportManager
        :param exportFolderDir: String [Target export directory]
        :param dateTime
        """

        self._baseExportDir = baseExportDir
        os.mkdir(baseExportDir)
        self._subExportDir = baseExportDir

        self._dateTime = dateTime

    @abstractmethod
    def export_report(self):
        pass

    @abstractmethod
    def export_data(self):
        pass

    def new_sub_dir(self, dirPath):
        """
        Method for creating a new one level subdirectory inside the base directory (overwrites base dir when called)
        Can reset back to the base directory by passing empty string
        """

        self._subExportDir = os.path.join(self._baseExportDir, dirPath)
        os.mkdir(self._subExportDir)


class ExportManagerFeature(ExportManager):

    def __init__(self, exportFolderDir, dateTime, procSettings, orientSettings, allMetrics, featureListDict,
                 windowSpecs, uidList):
        """
        Constructor for ExportManager
        :param exportFolderDir: string [Target export directory]
        :param dateTime: string [Current datetime]
        :param procSettings: Dict(bool/dict(dict(string))
        :param orientSettings: Dict(Bool/String)
        :param allMetrics: Dict(bool)
        :param featureListDict: Dict(List(Functions))
        :param windowSpecs: Dict(Bool/String)
        :param uidList: List(Int)
        """

        super().__init__(exportFolderDir, dateTime)

        self._procSettings = procSettings
        self._orientSettings = orientSettings
        self._allMetrics = allMetrics
        self._featureListDict = featureListDict
        self._windowSpecs = windowSpecs
        self._uidList = uidList
        self._noSubjects = len(uidList)

    def export_data(self, filename, data):
        """
        Called once at the end of all other calculations
        :param filename: string [local directory to save this assessment in the base directory]
        :param data: pd.DataFrame(index: label/trial}
        """

        # reduce the memory requirements of the data and make sure index names correct
        data = FilterManager.memory_reduce(data)
        data.columns.rename(['series', 'location', 'metric', 'feature'], inplace=True)
        data.index.rename(['label', 'trial'], inplace=True)

        try:
            featureSaveName = os.path.join(self._subExportDir, filename) + '.pickle'
            data.to_pickle(featureSaveName)
        except MemoryError:
            featureSaveName = os.path.join(self._subExportDir, filename) + '.parquet.gzip'
            data.to_parquet(featureSaveName, compression='gzip')

    def export_report(self, filename, labelList, projInertialFreq, projAdcFreq):
        """
        :param filename: string [The filename for this assessment]
        :param labelList: list(string) [List of all unique labels for the assessment]
        :param projInertialFreq: int [The frequency used for all data within the project]
        :param projAdcFreq: int [The frequency used for all data within the project]
        Method for exporting the report for feature data
        """

        infoSaveName = os.path.join(self._subExportDir, filename) + '.txt'

        textPrint = list()
        textPrint.append('*****PROJECT SETTINGS***** \n')
        textPrint.append('Features Extracted on Date: ' + self._dateTime + '\n')
        textPrint.append('Unique Labels: %s' % len(set(labelList)))
        textPrint.append('Labels Recorded: ')
        textPrint.append(', '.join(str(label) for label in set(labelList)) + '\n')
        textPrint.append('Unique Trials Recorded: ' + str(self._noSubjects))
        textPrint.append('Trial UIDs Recorded: ')
        textPrint.append(', '.join('UID' + str(uid) for uid in self._uidList) + '\n')
        textPrint.append('Project Frequencies (These were chosen as the minimum frequencies across all trials): ')
        textPrint.append('Inertial: ' + str(projInertialFreq) + ' Hz')
        textPrint.append('ADC: ' + str(projAdcFreq) + ' Hz \n')

        textPrint.append('*****ORIENTATION SETTINGS***** \n')
        textPrint.append('Left body sides flipped: %s' % str(self._orientSettings['flipBodySide']))
        textPrint.append('Quaternions Calculated: %s' % str(self._orientSettings['calcOrientData']) + '\n')
        if self._orientSettings['calcOrientData']:
            textPrint.append('Quaternion torso simulated: %s' & str(self._orientSettings['simulateTorso']))
            textPrint.append('Quaternion heading tared: %s' % str(self._orientSettings['tareQuat']))
            textPrint.append('Tared Quaternion offset applied: %s' % str(self._orientSettings['offsetQuat']) + '\n')

        textPrint.append('*****FILTER SETTINGS***** \n')
        textPrint.append('Data Filtered: %s' % str(self._procSettings['filtBool']) + '\n')
        if self._procSettings['filtBool']:
            accBounds = self._procSettings['filtBounds']['accBounds']
            gyroBounds = self._procSettings['filtBounds']['gyrBounds']
            emgBounds = self._procSettings['filtBounds']['emgBounds']
            mmgBounds = self._procSettings['filtBounds']['mmgBounds']
            textPrint.append('Gyroscope filtered with low cut-off of ' + str(gyroBounds['LF'])
                             + ' Hz and high cut-off of ' + str(gyroBounds['HF']) + ' Hz')
            textPrint.append('Accelerometer filtered with low cut-off of ' + str(accBounds['LF'])
                             + ' Hz and high cut-off of ' + str(accBounds['HF']) + ' Hz')
            textPrint.append('EMG filtered with low cut-off of ' + str(emgBounds['LF'])
                             + ' Hz and high cut-off of ' + str(emgBounds['HF']) + ' Hz')
            textPrint.append('MMG filtered with low cut-off of ' + str(mmgBounds['LF'])
                             + ' Hz and high cut-off of ' + str(mmgBounds['HF']) + ' Hz \n')
            textPrint.append('Wavelet De-noising applied to data: ' + str(self._procSettings['wavDenoise']))
            textPrint.append('Notch filter applied to ADC data: ' + str(self._procSettings['notchAdc']) + '\n')

        textPrint.append('*****WINDOW SETTINGS***** \n')
        textPrint.append('Data Windowed: %s' % str(self._windowSpecs['windowBool']) + '\n')
        if self._windowSpecs['windowBool']:
            textPrint.append('Window Length: %s' % self._windowSpecs['windowLength'] + '\n')
            textPrint.append('Window Overlap: %s' % self._windowSpecs['windowOverlap'] + '\n')

        textPrint.append('*****METRICS CALCULATED***** \n')
        for metric in self._allMetrics:
            textPrint.append(metric + ':' + str(self._allMetrics[metric]))
        textPrint.append('')

        textPrint.append('*****FEATURE CONFIGURATION***** \n')
        for featureType in self._featureListDict:
            textPrint.append(featureType + ':')
            if isinstance(self._featureListDict[featureType], dict):
                for key2 in self._featureListDict[featureType]:
                    featureString = key2 + ': ' + ', '.join(
                        (func.__name__) for func in self._featureListDict[featureType][key2])
                    textPrint.append(featureString)
                textPrint.append('')
            else:
                featureString = ', '.join((func.__name__) for func in self._featureListDict[featureType])
                textPrint.append(featureString + '\n')

        # Create and write information to the text file
        with open(infoSaveName, 'w') as f:
            for text in textPrint:
                f.write(text + '\n')


class ExportManagerQuat(ExportManager):

    def __init__(self, exportFolderDir, dateTime, orientSettings):
        """
        Constructor for ExportManager
        :param exportFolderDir: String [Target export directory]
        :param dateTime
        """

        super().__init__(exportFolderDir, dateTime)
        self._orientSettings = orientSettings

    def export_data(self, filename, orientData, uid):
        """
        Method for saving orientation data as well as the task list associated with each quaternion
        Called once for every uid recorded
        :param localDir: local directory to save each trial quaternion data
        :param orientData: pd.DataFrame{index: label}
        :param uid: int
        """

        uniqueLocs = [location for location in self._orientData.columns.unique(level='location') if location != 'ALL']
        for uniqueLoc in uniqueLocs:
            filepath = os.path.join(localDir, uniqueLoc) + '.csv'
            quatData = orientData[(uniqueLoc, 'quat')].transform(lambda x: x.__array__())
            quatMatrix = np.vstack(list(quatData.values))
            dataframe = pd.DataFrame(quatMatrix, columns=['w', 'x', 'y', 'z'])
            dataframe.to_csv(path_or_buf=filepath, index=False)

        orientData[('ALL', 'task')].to_csv(path_or_buf=os.path.join(localDir, 'tasks') + '.csv', index=False)

    def export_report(self):
        """
        Method for exporting the quaternion export report
        """

        infoSaveName = os.path.join(self._exportFolderDir, 'quat_description.txt')

        textPrint = list()
        textPrint.append('PROJECT SETTINGS \n')
        textPrint.append('Quaternions Extracted on Date: ' + self._dateTime + '\n')
        textPrint.append('Left body sides flipped: %s' % str(self._orientSettings['flipBodySide']))
        textPrint.append('Quaternion torso simulated: %s' % str(self._orientSettings['simulateTorso']))
        textPrint.append('Quaternion tared where possible: %s' % str(self._orientSettings['tareQuat']))
        if self._orientSettings['tareQuat']:
            textPrint.append(
                'Quaternion offsets applied to the tared data: %s' % str(self._orientSettings['offsetQuat']))

        # Create and write information to the text file
        with open(infoSaveName, 'w') as f:
            for text in textPrint:
                f.write(text + '\n')


class ExportManagerFigure(ExportManager):

    def __init__(self, baseExportDir, dateTime, plotSelection, plotC, plotD):
        """
        Constructor for ExportManager
        :param exportFolderDir: String [Target export directory]
        :param dateTime
        :param plotSelection
        :param plotC
        :param plotD
        """

        super().__init__(baseExportDir, dateTime)
        self._plotSelection = plotSelection
        self._plotC = plotC
        self._plotD = plotD

    def export_data(self, plotName, figure):
        """
        Method for saving plot figures
        Called once for every figure recorded
        :param plotName: string [name assigned to the plot]
        :param figure: Matplotlib.pyplot.fig
        """

        figure.savefig(os.path.join(self._subExportDir, plotName))
        plt.close(figure)

    def export_report(self, reportName):
        """
        Method for exporting the figure export report
        """

        infoSaveName = os.path.join(self._baseExportDir, reportName) + '.txt'

        textPrint = list()
        textPrint.append('PROJECT SETTINGS \n')
        textPrint.append('Plots saved on date: ' + self._dateTime + '\n')
        textPrint.append('PLOT SELECTION \n')
        textPrint.append('\n'.join(plotType + ': ' + str(self._plotSelection[plotType]) for plotType in self._plotSelection) + '\n')
        textPrint.append('PLOT SETTINGS \n')
        textPrint.append('plotFreqSeries: ' + str(self._plotC['plotFreqSeries']))
        textPrint.append('\n'.join(config + ': ' + str(self._plotD[config]) for config in self._plotD))

        # Create and write information to the text file
        with open(infoSaveName, 'w') as f:
            for text in textPrint:
                f.write(text + '\n')



