import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import os
from PyQt5.QtCore import *
from classes.FilterManager import FilterManager
from matplotlib.ticker import (AutoMinorLocator)
import seaborn as sns


class PlotManager:

    def __init__(self, plotConfig, plotDisplay, taskMap, dataProcessing):
        """
        Default Constructor
        :param plotConfig: Dict{Bool} (Bool represents where various plot configuration settings are active)
        :param plotDisplay: Dict{Bool} (Bool represents where various plot display settings are active)
        :param taskMap: Dict{key: unique number, value: global task string}
        :param dataProcessing: QWaitCondition (Prevents excess figures being created before they are processed and closed on the main thread)
        """

        self._plotConfig = plotConfig
        self._plotDisplay = plotDisplay
        self._globalTasksMap = {v: k for k, v in taskMap.items()}
        self._dataProcessing = dataProcessing

        self._mutex = QMutex()
        self._palette = sns.color_palette('muted', len(taskMap)-1)
        self._freq = None

    def plot_data(self, updateDataReady, data, plotName, freq):
        """
        Method for saving or displaying plots of time or frequency series inertial or myographic data
        If a DataFrame is passed (for multiple metrics) then each column will be plotted in a subplot
        User configurable settings include plotting as a data stream, plotting as task segmented data stream,
        and plotting each task individually
        :param updateDataReady: Signal [Thread-safe method of transporting data to main thread for exporting]
        :param data: pd.DataFrame [columns are XYZ if inertial or modality type if Myographic]
        :param plotName: string [Plot name including uid, location, series type, plot type]
        :param freq: Int [Inertial or ADC freq depending what type of metric is being plotted]
        """

        self._freq = freq
        data.index.rename(['task', 'task_inst'], inplace=True)

        # If we are only plotting active periods or one figure per task then we don't want to plot no task
        if self._plotDisplay['plotActivePeriods'] or self._plotDisplay['plotPerTask']:
            data.drop('no task', level='task', inplace=True)

        """
        metricConfig stores choices as to where put each metric plot in a subplot or overlay on same plot
        numSubPlots: How many subplots to iterate over (>1 if user chooses to plot each metric in a different subplot)
        subplotIter: Iterator up to the value of above (this will iterate over same subplot if above equals 1)
        listMetrics: List of all metrics to be iterated over
        """
        numMetrics = len(data.columns)
        if self._plotDisplay['subplotMetrics']:
            numSubPlots = numMetrics
            subplotIter = range(numMetrics)
        else:
            numSubPlots = 1
            subplotIter = np.repeat(0, numMetrics)

        # Drop the location column level since not required
        data.columns = data.columns.droplevel(level=0)
        listMetrics = list(data.columns)
        metricConfig = dict({'numSubPlots': numSubPlots, 'subplotIter': subplotIter, 'numMetrics': numMetrics, 'listMetrics': listMetrics})
        localTasks = list(data.index.unique(level='task'))

        # Split data processing into two possibilities:
        # Plot per task (may plot time or freq series data)
        # Plot as segmented data stream (time series data only) and plot as data stream (time or freq series data)
        if self._plotDisplay['plotTasksSeg']:
            graphDfList, graphLabelsList, localFigRange, globalFigRange = self.plot_segmented(data, plotName, metricConfig)
        else:
            graphDfList, graphLabelsList, localFigRange, globalFigRange = self.plot_stream(data, plotName, metricConfig, self._plotDisplay['plotPerTask'])

        run = False
        for graphDf, graphLabels, localFigRangeIdx in zip(graphDfList, graphLabelsList, range(len(localFigRange.index))):
            self._mutex.lock()
            if run:
                self._dataProcessing.wait(self._mutex)
            fig = self.annotate_plots(graphDf, graphLabels, localFigRange.iloc[localFigRangeIdx], globalFigRange, localTasks, metricConfig)
            updateDataReady.emit('figure', 'data', graphLabels['title'], fig)
            self._mutex.unlock()
            run = True

    def plot_segmented(self, data, plotName, metricConfig):
        """
        Method for plotting task segmented figures (makes sense for time series data only)
        :param data: pd.DataFrame [columns: inertial or pin metric, index: task/task instance]
        :param plotName: String [name that describes the plot. Combination of Pt and IMU number, and filtered metric]
        :param metricConfig: dict{list{string} + int + array{int}} [contains list/number of metrics.
                             Also contains the number of subplots to be generated and an array that iterates over these]
        :return graphDf: pd.DataFrame [columns: x and y data, index: subplot/ metricIdx/ task/ taskIdx]
        """

        # Pre-configuring the time and freq series data (and setting appropriate xLabel values)
        procData = FilterManager.standardise_time_series(data, self._freq, True)
        xLabel = 'Time (Seconds)'

        # Cannot sort the DataFrame here since method "Annotate" plots relies on it being ordered

        """
        subplot: which subplot to plot data on (This could be the metric)
        metric_idx: This is the first priority lines to be drawn (i.e. metric if these are overlaid on each other)
        task: This is the secondary priority lines to be drawn (i.e. if the metrics are to separated by task)
        TaskIdx: This is the tertiary priority lines to be drawn (i.e. if the tasks are to be plotted in order)
        """
        plotDf = pd.DataFrame(data=np.zeros((len(procData.index), 3)), index=procData.index, columns=['subplot', 'x', 'y'])
        plotDfDict = dict()

        """
        Each metric corresponds to a new subplot and a new column of data selected from the "procData" DataFrame
        Cycle through each task and each task instance to fill the remaining two columns
        """
        for metric, metricIdx, subplot in zip(metricConfig['listMetrics'], range(len(metricConfig['listMetrics'])), metricConfig['subplotIter']):
            plotDfDict[metricIdx] = plotDf.copy()
            plotDfDict[metricIdx]['subplot'] = subplot
            plotDfDict[metricIdx]['x'] = procData.index.get_level_values('bins')
            plotDfDict[metricIdx]['y'] = procData[metric]

        graphDf = pd.concat(plotDfDict, names=['metric_idx', 'task', 'task_inst', 'bins'])
        graphDf.reset_index(inplace=True)
        graphDf.set_index(['subplot', 'metric_idx', 'task', 'task_inst'], inplace=True)
        graphDf.drop('bins', axis=1, inplace=True)

        graphLabels = dict({'title': plotName, 'xLabel': xLabel, 'yLabel': 'Amplitude'})

        localFigRange = pd.DataFrame(data=graphDf['y'].agg(['min', 'max'])).T

        return [graphDf], [graphLabels], localFigRange, None

    def plot_stream(self, data, plotName, metricConfig, figurePerTask):
        """
        Method for plotting each task in its own separate figure (still following user preference for subplot or same plot per metrics)
        :param data: pd.DataFrame (columns: inertial or pin metric, index: task/ task index)
        :param plotName: String (name that describes the plot. Combination of Pt and IMU number, and filtered metric)
        :param metricConfig: dict{list{string}/ int/ array{int}} (contains list/number of metrics.
                             Also contains the number of subplots to be generated and an array that iterates over these
        :param figurePerTask: Bool [Whether or not the user wants each task to be plotted in a seperate figure]
        :return plotDfTaskList
        """

        # If want to plot a figure per task then the iterList is populated with the different tasks
        # Otherwise iterList is just populated with one iterable which covers all tasks
        if figurePerTask:
            taskName = None
            taskList = list(data.index.unique(level='task'))
        else:
            taskName = 'all'
            taskList = [data.index.unique(level='task')]

        plotDfTaskList = list()
        plotLabelTaskList = list()
        axesFigRangeList = list()
        for task in taskList:

            # Pre-configuring the time and freq series data (and setting appropriate xLabel values)
            if self._plotConfig['plotFreqSeries']:
                procData = FilterManager.fast_fourier_transform(data.loc[task].reset_index(drop=True), self._freq)
                xLabel = 'Frequency (Hz)'
            else:
                procData = FilterManager.standardise_time_series(data.loc[task].reset_index(drop=True), self._freq, False)
                xLabel = 'Time (Seconds)'

            plotDf = pd.DataFrame(data=np.zeros((len(procData.index), 3)), columns=['subplot', 'x', 'y'])
            plotDfDict = dict()
            for metric, metricIdx, subplot in zip(metricConfig['listMetrics'], range(len(metricConfig['listMetrics'])), metricConfig['subplotIter']):
                plotDfDict[metricIdx] = plotDf.copy()
                plotDfDict[metricIdx]['subplot'] = subplot
                plotDfDict[metricIdx]['x'] = procData.index
                plotDfDict[metricIdx]['y'] = procData[metric].values

            graphDf = pd.concat(plotDfDict, names=['metric_idx', 'dummy'])
            graphDf.reset_index(inplace=True)
            graphDf.set_index(['subplot', 'metric_idx'], inplace=True)
            graphDf.drop('dummy', axis=1, inplace=True)
            plotDfTaskList.append(graphDf)

            if taskName != 'all':
                taskName = task

            if figurePerTask:
                graphLabels = dict({'title': plotName + '_' + taskName, 'xLabel': xLabel, 'yLabel': 'Amplitude'})
            else:
                graphLabels = dict({'title': plotName, 'xLabel': xLabel, 'yLabel': 'Amplitude'})
            plotLabelTaskList.append(graphLabels)
            # Finding the output ranges locally (min and max for each figure) and globally (min and max for each metric across all figures)
            axesFigRangeList.append(procData[metricConfig['listMetrics']])

        axesFigRange = pd.concat(axesFigRangeList, axis=0, keys=range(len(axesFigRangeList)))
        localFigRange = axesFigRange.groupby(level=0).agg(['min', 'max']).agg(['min', 'max'], axis=1)
        globalFigRange = axesFigRange.agg(['min', 'max']).T

        return plotDfTaskList, plotLabelTaskList, localFigRange, globalFigRange

    def annotate_plots(self, graphDf, graphLabels, localFigRange, globalFigRange, orderedTasks, metricConfig):
        """
        Method accepts a single pd.DataFrame that describes a single figure
        Generates axes per subplot and annotates them according to the user preference
        :param graphDf: pd.DataFrame(columns: xData, yData, multi-index: subplotIdx/ metricOrder/ plotOrder (0/1/2)
        :param graphLabels: dict{string} (labels for the graph title, x-axis, and y-axis
        :param localFigRange: Normalises the metric axis across subplots for each tasks
        :param globalFigRange: Normalises the metric axis across all tasks
        :param orderedTasks: list{string} (contains the list of unique tasks and the length of this list)
        :param metricConfig: dict{list{string} + int + array{int}} (contains list/number of metrics.
                             Also contains the number of subplots to be generated and an array that iterates over these
        """

        def find_marker(task_loc):
            """
            Method for assigning the plot style parameters depending on the task being plotted
            :param task_loc: string [The unique task name of the data being plotted]
            :return: plotPatameters {array(string, string)}
            """

            if task_loc == 'no task':
                return ['--', '0.9']
            else:
                return ['-', self._palette[self._globalTasksMap[task_loc]-1]]

        fig, axes = plt.subplots(metricConfig['numSubPlots'], 1, sharex=True, sharey=False, figsize=(16, 8), dpi=200, tight_layout=False)
        if not isinstance(axes, np.ndarray):
            axes = [axes]

        """
        Generate a list of plots depending on the multi-index of the input pd.DataFrame
        First nested loop covers case whereby want to plot each task and task instance individually (with different colour for each task)
        Second nested loop covers case where only care about plotting each metric (in own subplot or not)
        """
        plotList = list()
        tasksSeen = list()
        if self._plotDisplay['plotTasksSeg']:
            # graphDf.sort_index(inplace=True)    # Sort tasks alphabetically here rather than temporally for speed
            for subplot in graphDf.index.unique(level=0):
                for metricIdx in graphDf.loc[subplot].index.unique(level=0):
                    for task in graphDf.loc[subplot, metricIdx].index.unique(level=0):
                        for taskInst in graphDf.loc[subplot, metricIdx, task].index.unique(level=0):
                            marker = find_marker(task)
                            plot, = axes[int(subplot)].plot(graphDf.loc[(int(subplot), int(metricIdx), task, int(taskInst)), 'x'],
                                                            graphDf.loc[(int(subplot), int(metricIdx), task, int(taskInst)), 'y'],
                                                            linewidth=0.5, linestyle=marker[0], label=task,  color=marker[1], gid=task)
                            if task not in tasksSeen:
                                plotList.append(plot)
                                tasksSeen.append(task)
        else:
            for subplot in graphDf.index.unique(level=0):
                for metricIdx in graphDf.loc[subplot].index.unique(level=0):
                    plot, = axes[int(subplot)].plot(graphDf.loc[(int(subplot), int(metricIdx)), 'x'],
                                                    graphDf.loc[(int(subplot), int(metricIdx)), 'y'],  linewidth=0.5)
                    if self._plotDisplay['normTaskAxes']:
                        axes[int(subplot)].set_ylim([globalFigRange['min'].iloc[metricIdx], globalFigRange['max'].iloc[metricIdx]])
                    plotList.append(plot)

        fig.suptitle(graphLabels['title'], y=0.93)
        plt.xlabel(graphLabels['xLabel'])
        for ax, metric in zip(axes, metricConfig['listMetrics']):
            if len(graphDf.index.unique(level=0)) > 1:
                ax.set_title(metric)
            ax.set_ylabel(graphLabels['yLabel'])
            if self._plotDisplay['normMetricAxes']:
                ax.set_ylim([localFigRange['min'], localFigRange['max']])

        # Generate Figure Legend
        if self._plotDisplay['legend']:
            if self._plotDisplay['plotTasksSeg']:
                fig.legend(plotList, tasksSeen, loc=4, markerscale=50)
            else:
                fig.legend([plotList[count] for count in range(metricConfig['numMetrics'])], metricConfig['listMetrics'], loc=4, markerscale=50)
        # Generate Figure Grid
        if self._plotDisplay['grid']:
            [ax.grid(True, which='major', axis='both') for ax in axes]
            [ax.grid(True, which='minor', axis='x') for ax in axes]
            [ax.minorticks_on() for ax in axes]
            [ax.xaxis.set_minor_locator(AutoMinorLocator(20)) for ax in axes]
        # Generate RMS line (only suitable for time series subplots)
        if self._plotDisplay['rmsLine'] and len(graphDf.index.unique(level='subplot')) > 1:
            rmsData = [np.sqrt(np.mean(graphDf['y'].loc[subplotOrder]**2)) for subplotOrder in graphDf.index.unique(level='subplot')]
            signData = np.sign([np.mean(graphDf['y'].loc[subplotOrder]) for subplotOrder in graphDf.index.unique(level='subplot')])
            for ax, rmsDatum, sign in zip(axes, rmsData, signData):
                ax.axhline(y=sign * rmsDatum, color='red')
                ax.text(x=0.8, y=0.8, transform=ax.transAxes, bbox=dict(facecolor='red', alpha=0.1), s='RMS Value: ' + str(np.round(rmsDatum, 3)), color='red')
        # Generate mean freq line (only suitable for freq series subplots)
        if self._plotDisplay['meanFreq'] and len(graphDf.index.unique(level='subplot')) > 1:
            meanFreqData = [np.sum(graphDf['x'].loc[subplotOrder] * graphDf['y'].loc[subplotOrder]) / np.sum(graphDf['y'].loc[subplotOrder]) for subplotOrder in graphDf.index.unique(level='subplot')]
            for ax, meanFreqDatum in zip(axes, meanFreqData):
                ax.axvline(x=meanFreqDatum, color='red')
                ax.text(x=0.8, y=0.8, transform=ax.transAxes, bbox=dict(facecolor='red', alpha=0.1), s='Mean Freq. Value: ' + str(np.round(meanFreqDatum, 3)), color='red')

        return fig











