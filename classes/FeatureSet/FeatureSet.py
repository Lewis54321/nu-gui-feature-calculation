import numpy as np
import math
import statsmodels.tsa.ar_model as SMAR
import statsmodels.api as SM
import pandas as pd
import warnings


class FeatureSet:

    class Decorators:

        def __init(self):
            pass

        @classmethod
        def missing_data(cls, func):
            """
            Decorator for ensuring that feature functions do not throw errors by attemmpting to operate on missing data
            This situation will arise if the analysis includes trials with fewer imu locations used that others
            Change the name of the wrapper function to inner function to prevent all functions having same name
            :param func:
            :return:
            """

            def func_wrapper_missing(data):
                if any(data.isna()):
                    return np.nan
                else:
                    return func(data)
            func_wrapper_missing.__name__ = func.__name__
            return func_wrapper_missing

        @classmethod
        def verbose_series(cls, func):
            """
            Decorator method for verbose flags when using groupby functions
            :param func:
            :return:
            """

            def func_wrapper_verbose_series(data):
                print('function: %s, feature: %s-%s, pt: %s-%s' % (func.__name__, data.name[0], data.name[1],
                                                                   data.index.get_level_values('pt')[0], data.index.get_level_values('window')[0]))
                return func(data)
            return func_wrapper_verbose_series

        @classmethod
        def verbose(cls, func):
            """
            Decorator method for verbose flags when using groupby functions
            :param func:
            :return:
            """

            def func_wrapper_verbose(data):
                print('function: %s, pt: %s' % (func.__name__, data.index.get_level_values('pt')[0]))
                return func(data)
            return func_wrapper_verbose

    def __init__(self):
        pass

    # ******** INTERMEDIATE METHODS ********** #

    @staticmethod
    def SumSquares(dataX, dataY, dataZ):
        """
        Sum Squares
        :param dataX:
        :param dataY:
        :param dataZ:
        :return:
        """

        return np.sum(dataX**2, dataY**2, dataZ**2)

    # ****** TIME SERIES FEATURES ****** #

    """ ************** Sensor Features ************* """

    @staticmethod
    def Unprocessed(data):
        return data

    @staticmethod
    @Decorators.missing_data
    def cusMedianCrossNorm(data):
        """
        Method of searching for sign changes in the data
        :param data:
        :return dataOut:
        """

        data = data.values
        newData = data - np.median(data)
        prodData = newData[:-1] * newData[1:]
        prodData[prodData > 0] = 0
        prodData[prodData < 0] = 1

        return (1 / float(len(data))) * np.sum(prodData)

    @staticmethod
    @Decorators.missing_data
    def MeanAbsValues(data):
        """
        Mean Absolute Value
        :param data:
        :return:
        """
        newData = (1 / float(len(data))) * sum(np.abs(data))
        return newData

    @staticmethod
    @Decorators.missing_data
    def MedianAbsValues(data):
        """
        Median Absolute Value
        :param data:
        :return:
        """
        newData = np.median(np.abs(data))
        return newData

    @staticmethod
    @Decorators.missing_data
    def cusRms(data):

        newData = math.sqrt((1/float(len(data)) * np.sum(data*data)))
        return newData

    @staticmethod
    @Decorators.missing_data
    def SignalPower(data):

        data.fillna(method='ffill', inplace=True)
        data.fillna(method='bfill', inplace=True)
        return np.sum(data*data)

    @staticmethod
    @Decorators.missing_data
    def TrapInt(data):

        data.fillna(method='ffill', inplace=True)
        data.fillna(method='bfill', inplace=True)
        return np.trapz(data, dx=0.01)

    @staticmethod
    @Decorators.missing_data
    def PeakToPeak(data):

        return (np.max(data) - np.min(data))

    @staticmethod
    @Decorators.missing_data
    def NewJerkMetric(IMUAddMetrics, IMUResData, freq):
        """
        New Jerk Metric as proposed by Cruz, 2014
        :param IMUAddMetrics:
        :param IMUResData:
        :param freq:
        :return:
        """

        jerkSumSquared = FeatureSet.SumSquares(IMUAddMetrics['Jerk X'].astype(float), IMUAddMetrics['Jerk Y'].astype(float),
                         IMUAddMetrics['Jerk Z'].astype(float), freq.Sensor)

        velSumRootSquared = sum(IMUResData['Res Vel'].astype(float) * (1 / float(freq.Sensor)))

        jerkMetric = (jerkSumSquared * (2 ** 5)) / (velSumRootSquared ** 2)

        return jerkMetric

    @staticmethod
    @Decorators.missing_data
    def OldJerkMetric(IMUAddMetrics, IMUResData):

        num = np.mean(np.sqrt(IMUAddMetrics['Jerk X'].astype(float)**2, IMUAddMetrics['Jerk Y'].astype(float)**2,IMUAddMetrics['Jerk Z'].astype(float)**2))
        den = np.max(IMUResData['Res Vel'].astype(float))

        dataOut = -num / den
        return dataOut

    """ **************** ADC Features ****************** """

    @staticmethod
    @Decorators.missing_data
    def ModMeanAbsValue1(data):
        """
        Modified mean absolute value (Like MAV but threshold reduces significance of data outside 25-75 % data)
        :param data:
        :return dataOut:
        """

        dataOut = np.empty(len(data))

        for count in range(len(data)):
            if count >= 0.25 * len(data) and count <= 0.75 * len(data):
                dataOut[count] = abs(data.iloc[count])
            else:
                dataOut[count] = abs(data.iloc[count])*0.5

        dataOut = sum(dataOut) * (1/float(len(data)))

        return dataOut

    @staticmethod
    @Decorators.missing_data
    def AbsTemporalMoment(data):
        """
        Absolute Temporal Moments (RETURNS 3 PARAMETERS)
        :param data:
        :return:
        """

        dataOut = np.zeros(3)

        # Third, fourth, and fifth temporal moment respectively
        dataOut[0] = np.abs((1/float(len(data))) * sum(data ** 3))
        dataOut[1] = (1 / float(len(data))) * sum(data ** 4)
        dataOut[2] = np.abs((1 / float(len(data))) * sum(data ** 5))

        return pd.Series(dataOut)

    @staticmethod
    @Decorators.missing_data
    def LogDetector(data):
        """
        Estimation of muscle contraction force
        :param data: np.array
        :return: float
        """

        logData = (1/float(len(data))) * sum(np.log10(np.abs(data)))
        dataOut = np.exp(logData)

        return dataOut

    @staticmethod
    @Decorators.missing_data
    def WaveFormLength(data):
        """
        Measure of the complexity of the signal
        Calculation is composed of normal signal and the signal which leads by one point
        The subtraction results in the difference in amplitude from one point to the next
        :param data:
        :return dataOut:
        """

        data = data.values
        signal = data[:-1]
        signalLeadOne = data[1:]

        dataOut = np.abs(signalLeadOne - signal)
        dataOut = np.sum(dataOut)

        return dataOut

    @staticmethod
    @Decorators.missing_data
    def AverageAmplitudeChange(data):
        """
        Same as waveform length except normalised
        :param data:
        :return dataOut:
        """

        data = data.values
        signal = data[:-1]
        signalLeadOne = data[1:]

        dataOut = np.abs(signalLeadOne - signal)
        dataOut = (1/float(len(data))) * sum(dataOut)

        return dataOut

    @staticmethod
    @Decorators.missing_data
    def DifferenceAbsSD(data):
        """
        Standard deviation of the waveform
        :param data:
        :return:
        """

        data = data.values
        signal = data[:-1]
        signalLeadOne = data[1:]

        dataOut = (signalLeadOne - signal)**2
        dataOut = (1/float(len(data)-1)) * sum(dataOut)
        dataOut = np.sqrt(dataOut)

        return dataOut

    @staticmethod
    @Decorators.missing_data
    def ZeroCrossingPercentage(data):
        """
        Number of times the amplitude crosses the zero line (threshold means that noise is not included in this)
        Need to calculate a suitable threshold for EMG (at both levels) and MMG
        :param data:
        :return:
        """

        # Threshold for MMG set at RMS at rest
        if ('MMG' in data.name[1]):
            threshold = 0.01654
        # Threshold for EMG set at approximation of RMS at rest
        elif ('EMG' in data.name[1]):
            threshold = 0.0194

        dataOutArray = np.empty(len(data)-1)

        for count in range(len(data)-1):
            if np.sign(data.iloc[count] * data.iloc[count+1]) == -1:
                if np.abs(data.iloc[count] - data.iloc[count+1]) >= threshold:
                    dataOutArray[count] = 1
                else:
                    dataOutArray[count] = 0
            else:
                dataOutArray[count] = 0

        dataOut = (1/float(len(data)-1)) * np.sum(dataOutArray)
        return dataOut

    @staticmethod
    @Decorators.missing_data
    def MyopulseRatePercentage(data):
        """
        Percentage of times that the amplitude exceeds a pre-defined threshold
        Need to calculate a suitable threshold for EMG and MMG
        :param data:
        :return:
        """

        # Threshold for MMG set at 2*RMS at rest
        if ('MMG' in data.name[1]):
            threshold = 0.01654
        # Threshold for EMG set at approximation of 2*RMS at rest
        elif ('EMG' in data.name[1]):
            threshold = 0.0194

        dataOut = np.zeros(len(data))
        dataOut[data.abs() > threshold] = 1
        dataOut = (1/float(len(data))) * np.sum(dataOut)

        return dataOut

    @staticmethod
    @Decorators.missing_data
    def WillisonAmplitudePercentage(data):
        """
        Numbers of times the difference between two adjoined amplitude points exceeds a threshold value
        Need to calculate a suitable threshold for EMG and MMG
        :param data:
        :return:
        """

        # Threshold for MMG set at RMS at rest
        if ('MMG' in data.name[1]):
            threshold = 0.0083
        # Threshold for EMG set at approximation of 2*RMS at rest
        elif ('EMG' in data.name[1]):
            threshold = 0.0097

        dataOut = np.zeros(len(data)-1)
        dataDiff = np.diff(data)
        dataOut[dataDiff >= threshold] = 1
        dataOut = (1/float(len(data))) * np.sum(dataOut)

        return dataOut

    @staticmethod
    @Decorators.missing_data
    def SlopeSignChangePercentage(data):
        """
        Numbers of times the slope changes sign (threshold used to account for noise)
        Need to calculate a suitable threshold for EMG and MMG
        May have to remove this feature due to the drastically different levels of noise between MMG sensors
        :param data: pd.Series
        :return:
        """

        # Threshold for MMG set at 0.1 * RMS at rest
        if ('MMG' in data.name[1]):
            threshold = 0.00082
        # Threshold for EMG set at approximation of 2*RMS at rest
        elif ('EMG' in data.name[1]):
            threshold = 0.0097
        else:
            raise Exception('Missing expected myographic type name')

        # Create 2 segments, first is the difference between leading by 1 point and the point before
        # Second is the different between leading by 1 point and leading by 2 points
        # Sign change is represented by a positive product
        data = data.values
        # leading by one point data minus normal data
        firstSegment = data[1:-1] - data[:-2]
        # leading by one point data minus leading by 2 points data
        secondSegment = data[1:-1] - data[2:]
        dataOut = firstSegment * secondSegment

        dataOut[dataOut >= threshold] = 1
        dataOut[dataOut < threshold] = 0
        dataOut = (1/float(len(data))) * sum(dataOut)

        return dataOut

    @staticmethod
    @Decorators.missing_data
    def HammingWindowPercentage(data):
        """
        Energy of data after is has been convolved with Hamming Window
        :param data:
        :return dataOut:
        """

        window = np.hamming(len(data))

        dataOut = (window * data)**2
        dataOut = (1/float(len(data))) * sum(dataOut)

        return dataOut

    @staticmethod
    @Decorators.missing_data
    def Histogram(data):
        """
        Number of values present in each of the 9 equally spaced bins [RETURNS 9 PARAMETERS]
        :param data:
        :return:
        """

        dataOut = np.histogram(data, bins=9)
        # First array (out of the two arrays returned) is the number of elements which fit into each linearly spaced bin
        return pd.Series((1/float(len(data))) * dataOut[0])

    @staticmethod
    @Decorators.missing_data
    def AutoRegressiveCoefficients(data):
        """
        The 5 coefficients of a 4 lag autoregressive model of the signal [RETURNS 5 PARAMETERS]
        :param data:
        :return:
        """

        order = 4
        model = SMAR.AR(data.values)
        model_fit = model.fit(maxlag=order)
        lag = model_fit.k_ar
        coeff = model_fit.params

        return pd.Series(coeff)

    # ************************** FREQUENCY SERIES FEATURES ******************************#

    """ *************************** General Features *************************************** """

    @staticmethod
    @Decorators.missing_data
    def DomFrequency(freqData):
        """
        Frequency which carries the highest amount of energy
        :param freqData:
        :return:
        """

        bins = freqData.index.get_level_values(2).values
        domFreq = bins[np.argmax(freqData.values ** 2)]
        return domFreq

    @staticmethod
    @Decorators.missing_data
    def MeanFrequency(freqData):
        """
        Mean Frequency
        :param freqData:
        :param bins:
        :return:
        """

        warnings.filterwarnings("error")
        bins = freqData.index.get_level_values(2)
        num = np.sum(bins * (freqData**2))
        denom = np.sum(freqData**2)
        dataOut = num/denom

        return dataOut

    @staticmethod
    @Decorators.missing_data
    def MedianFrequency(freqData):
        """
        Median Frequency
        :param freqData:
        :param bins:
        :return:
        """

        power = 0
        halfPower = 0.5 * np.sum(freqData**2)
        for bin, freqValue in zip(freqData.index.get_level_values(2), freqData):
            power += freqValue**2
            if power > halfPower:
                return bin

    @staticmethod
    @Decorators.missing_data
    def MeanPower(freqData):
        """
        Average power of the power spectrum
        :param freqData:
        :param bins:
        :return:
        """

        dataOut = np.sum(freqData**2)/len(freqData)
        return dataOut

    @staticmethod
    @Decorators.missing_data
    def TotalPower(freqData):
        """
        Total power of the signal
        :param freqData:
        :param bins:
        :return:
        """

        dataOut = np.sum(freqData**2)
        return dataOut

    @staticmethod
    @Decorators.missing_data
    def FrequencyRatio(freqData):
        """
        Finds the frequency power ratio between the high frequency components (> mean frequency) and the low frequency
        components (< mean frequency)
        :param freqData:
        :param bins:
        :return:
        """

        bins = freqData.index.get_level_values(2)
        Thresh = FeatureSet.MeanFrequency(freqData)

        powerAboveThresh = (freqData[bins >= Thresh])**2
        powerBelowThresh = (freqData[bins < Thresh])**2
        dataOut = np.sum(powerBelowThresh) / np.sum(powerAboveThresh)

        return dataOut

    """ *************************** Sensor Features *************************************** """

    @staticmethod
    @Decorators.missing_data
    def PowerSpectrumRatioFreq(freqData):
        """
        Finds the ratio of a 0.2 Hz band around the peak power frequency and the power of all frequencies
        :param freqData:
        :param bins:
        :return:
        """

        bins = freqData.index.get_level_values(2).values
        freqData = freqData.values
        # This is the window around the peak value (in frequency) where the sum of values is calculated
        windowAroundPeak = 0.2

        # Finding the frequency idx and frequency where the peak power is
        peakFreqIdx = np.argmax(freqData ** 2)
        peakFreq = bins[peakFreqIdx]

        # Finding the frequency idx where the bottom and top frequency band is
        peakFreqBottomIdx = (np.abs(bins - (peakFreq - windowAroundPeak/2))).argmin()
        peakFreqTopIdx = (np.abs(bins - (peakFreq + windowAroundPeak / 2))).argmin()
        peakWindow = np.arange(peakFreqBottomIdx, peakFreqTopIdx + 1)

        # Finding the window of power around the peak value
        peakEnergyWindow = (freqData[peakWindow])**2

        # Finding the power of the whole signal
        energySignal = freqData**2

        dataOut = np.sum(peakEnergyWindow) / np.sum(energySignal)
        return dataOut

    """ *************************** ADC Features *************************************** """

    @staticmethod
    @Decorators.missing_data
    def SpectralMoments(freqData):
        """
        Finds the first three spectral moments [RETURNS 3 PARAMETERS]
        :param freqData:
        :param bins:
        :return:
        """

        bins = freqData.index.get_level_values(2)
        SM1 = np.sum((freqData**2) * bins)
        SM2 = np.sum((freqData**2) * bins**2)
        SM3 = np.sum((freqData**2) * bins**3)

        dataOut = np.array([SM1, SM2, SM3])
        temp = pd.Series(dataOut)
        return pd.Series(dataOut)

    @staticmethod
    @Decorators.missing_data
    def PowerSpectrumRatioPoints(freqData):
        """
        Finds the ratio of 20 data points around the peak power frequency and the power between two frequencies
        :param freqData:
        :param bins:
        :return:
        """

        bins = freqData.index.get_level_values(2).values
        freqData = freqData.values
        # This is the window around the peak value (in data points) where the sum of values is calculated
        windowAroundPeak = 20
        freqRange = (5, 100)

        # Finding the frequency idx where the peak power is
        peakFreqIdx = np.argmax(freqData ** 2)

        # Window around peak idx
        peakWindow = np.arange((peakFreqIdx - int(windowAroundPeak/2)), (peakFreqIdx + int(windowAroundPeak/2))+1)

        # Finding the window of power around the peak value
        peakEnergyWindow = (freqData[peakWindow])**2

        # Finding lower and upper boundary freq
        lowFreqIdx = (np.abs(bins - freqRange[0])).argmin()
        highFreqIdx = (np.abs(bins - freqRange[1])).argmin()
        energyWindow = np.arange(lowFreqIdx, highFreqIdx+1)

        # Finding the whole energy of the signal (between 5 and 100 Hz)
        wholeEnergyWindow = (freqData[energyWindow])**2

        dataOut = np.sum(peakEnergyWindow) / np.sum(wholeEnergyWindow)
        return dataOut

    @staticmethod
    @Decorators.missing_data
    def VarianceCentralFrequency(freqData):
        """
        Variance of Central Frequency
        :param freqData:
        :param bins:
        :return:
        """

        bins = freqData.index.get_level_values(2)
        SM0 = np.sum(freqData**2)
        SM1 = np.sum((freqData**2) * bins)
        SM2 = np.sum((freqData**2) * bins**2)

        dataOut = (SM2/SM0) - (SM1/SM0)**2
        return dataOut
