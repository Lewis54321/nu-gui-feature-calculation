# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'input_dialog.ui'
#
# Created by: PyQt5 UI code generator 5.9.2
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_input_dialog(object):
    def setupUi(self, input_dialog):
        input_dialog.setObjectName("input_dialog")
        input_dialog.resize(566, 228)
        self.verticalLayout = QtWidgets.QVBoxLayout(input_dialog)
        self.verticalLayout.setObjectName("verticalLayout")
        self.widgetDatabase = QtWidgets.QWidget(input_dialog)
        self.widgetDatabase.setObjectName("widgetDatabase")
        self.formLayout_2 = QtWidgets.QFormLayout(self.widgetDatabase)
        self.formLayout_2.setObjectName("formLayout_2")
        self.label = QtWidgets.QLabel(self.widgetDatabase)
        font = QtGui.QFont()
        font.setBold(True)
        font.setWeight(75)
        self.label.setFont(font)
        self.label.setObjectName("label")
        self.formLayout_2.setWidget(0, QtWidgets.QFormLayout.LabelRole, self.label)
        self.lineEditSchema = QtWidgets.QLineEdit(self.widgetDatabase)
        self.lineEditSchema.setObjectName("lineEditSchema")
        self.formLayout_2.setWidget(0, QtWidgets.QFormLayout.FieldRole, self.lineEditSchema)
        self.label_2 = QtWidgets.QLabel(self.widgetDatabase)
        font = QtGui.QFont()
        font.setBold(True)
        font.setWeight(75)
        self.label_2.setFont(font)
        self.label_2.setObjectName("label_2")
        self.formLayout_2.setWidget(1, QtWidgets.QFormLayout.LabelRole, self.label_2)
        self.lineEditUsername = QtWidgets.QLineEdit(self.widgetDatabase)
        self.lineEditUsername.setObjectName("lineEditUsername")
        self.formLayout_2.setWidget(1, QtWidgets.QFormLayout.FieldRole, self.lineEditUsername)
        self.label_3 = QtWidgets.QLabel(self.widgetDatabase)
        font = QtGui.QFont()
        font.setBold(True)
        font.setWeight(75)
        self.label_3.setFont(font)
        self.label_3.setObjectName("label_3")
        self.formLayout_2.setWidget(2, QtWidgets.QFormLayout.LabelRole, self.label_3)
        self.lineEditPassword = QtWidgets.QLineEdit(self.widgetDatabase)
        self.lineEditPassword.setEchoMode(QtWidgets.QLineEdit.Password)
        self.lineEditPassword.setObjectName("lineEditPassword")
        self.formLayout_2.setWidget(2, QtWidgets.QFormLayout.FieldRole, self.lineEditPassword)
        self.verticalLayout.addWidget(self.widgetDatabase)
        self.pushButtonConnect = QtWidgets.QPushButton(input_dialog)
        self.pushButtonConnect.setObjectName("pushButtonConnect")
        self.verticalLayout.addWidget(self.pushButtonConnect)

        self.retranslateUi(input_dialog)
        QtCore.QMetaObject.connectSlotsByName(input_dialog)

    def retranslateUi(self, input_dialog):
        _translate = QtCore.QCoreApplication.translate
        input_dialog.setWindowTitle(_translate("input_dialog", "Connect to MySQL Database"))
        self.label.setText(_translate("input_dialog", "Schema Name: "))
        self.lineEditSchema.setText(_translate("input_dialog", "temp"))
        self.label_2.setText(_translate("input_dialog", "Username: "))
        self.lineEditUsername.setText(_translate("input_dialog", "root"))
        self.label_3.setText(_translate("input_dialog", "Password: "))
        self.pushButtonConnect.setText(_translate("input_dialog", "Connect"))

