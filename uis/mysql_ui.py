# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'mysql_ui.ui'
#
# Created by: PyQt5 UI code generator 5.9.2
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_mysqlUi(object):
    def setupUi(self, mysqlUi):
        mysqlUi.setObjectName("mysqlUi")
        mysqlUi.resize(566, 228)
        self.verticalLayout = QtWidgets.QVBoxLayout(mysqlUi)
        self.verticalLayout.setObjectName("verticalLayout")
        self.widgetDatabase = QtWidgets.QWidget(mysqlUi)
        self.widgetDatabase.setObjectName("widgetDatabase")
        self.formLayout_2 = QtWidgets.QFormLayout(self.widgetDatabase)
        self.formLayout_2.setObjectName("formLayout_2")
        self.label = QtWidgets.QLabel(self.widgetDatabase)
        font = QtGui.QFont()
        font.setBold(True)
        font.setWeight(75)
        self.label.setFont(font)
        self.label.setObjectName("label")
        self.formLayout_2.setWidget(0, QtWidgets.QFormLayout.LabelRole, self.label)
        self.lineEditSchema = QtWidgets.QLineEdit(self.widgetDatabase)
        self.lineEditSchema.setObjectName("lineEditSchema")
        self.formLayout_2.setWidget(0, QtWidgets.QFormLayout.FieldRole, self.lineEditSchema)
        self.label_2 = QtWidgets.QLabel(self.widgetDatabase)
        font = QtGui.QFont()
        font.setBold(True)
        font.setWeight(75)
        self.label_2.setFont(font)
        self.label_2.setObjectName("label_2")
        self.formLayout_2.setWidget(1, QtWidgets.QFormLayout.LabelRole, self.label_2)
        self.lineEditUsername = QtWidgets.QLineEdit(self.widgetDatabase)
        self.lineEditUsername.setObjectName("lineEditUsername")
        self.formLayout_2.setWidget(1, QtWidgets.QFormLayout.FieldRole, self.lineEditUsername)
        self.label_3 = QtWidgets.QLabel(self.widgetDatabase)
        font = QtGui.QFont()
        font.setBold(True)
        font.setWeight(75)
        self.label_3.setFont(font)
        self.label_3.setObjectName("label_3")
        self.formLayout_2.setWidget(2, QtWidgets.QFormLayout.LabelRole, self.label_3)
        self.lineEditPassword = QtWidgets.QLineEdit(self.widgetDatabase)
        self.lineEditPassword.setEchoMode(QtWidgets.QLineEdit.Password)
        self.lineEditPassword.setObjectName("lineEditPassword")
        self.formLayout_2.setWidget(2, QtWidgets.QFormLayout.FieldRole, self.lineEditPassword)
        self.verticalLayout.addWidget(self.widgetDatabase)
        self.pushButtonConnect = QtWidgets.QPushButton(mysqlUi)
        self.pushButtonConnect.setObjectName("pushButtonConnect")
        self.verticalLayout.addWidget(self.pushButtonConnect)

        self.retranslateUi(mysqlUi)
        QtCore.QMetaObject.connectSlotsByName(mysqlUi)

    def retranslateUi(self, mysqlUi):
        _translate = QtCore.QCoreApplication.translate
        mysqlUi.setWindowTitle(_translate("mysqlUi", "Connect to MySQL Database"))
        self.label.setText(_translate("mysqlUi", "Schema Name: "))
        self.lineEditSchema.setText(_translate("mysqlUi", "temp"))
        self.label_2.setText(_translate("mysqlUi", "Username: "))
        self.lineEditUsername.setText(_translate("mysqlUi", "root"))
        self.label_3.setText(_translate("mysqlUi", "Password: "))
        self.pushButtonConnect.setText(_translate("mysqlUi", "Connect"))

