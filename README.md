# NUIMU GUI Feature Calculation

## Overview

GUI designed to read in CSV files made up from streams of data recorded using "NUIMU GUI Saver".
Operations performed in this program include:

- Read in the header data provided in the CSV files to inform how the data is used
- Read in the stream of data, seperated by metric recorded (inertial and ADC data)
- Interpolate the data to accomodate for dropped packets during recording
- Apply custom filter to the recorded data
- Display or save plots of the unfiltered and filtered data
- Calculate custom time and frequency features and export

## User Requirements

- Python (version 3)
- MySQL Database Server
- Windows/ Ubuntu/ MacOS

## Development Requirement

- Qt Designer

## Python Library Dependencies

- pyqt (version 5.9.2 tested)
- pandas (version 1.0.3 tested)
- numpy (version 1.18.1 tested)
- scipy (version 1.4.1 tested)
- sqlalchemy (version 1.3.16 tested)
- matplotlib (version 3.1.3 tested)
- mysql-connector-python (version 8.0.18 tested)
- pyarrow (version 0.13.0 tested)
- pymysql (version 0.9.3 tested)
- scipy (version 1.4.1 tested)
- seaborn (version 0.10.1 tested)
- sqlite (version 3.31.1 tested)

## Important Classes

### Main

This is the entry point of the program and the file which should be run.
Creates instance and initialises the "UI_MainWindow" class in order to generate the GUI window.
Contains event triggers for all user exposed interaction including: selecting data/ label files,
importing data/ label files, processing data, and extracting features.
A new TrialManager instance is created for every trial operated on and a new ImuManager is created for each
IMU within that trial.
For each event handler triggered, the TrialManager instance calls an underlying method in this class, which then
typically calls an underlying method within this class

### FileManager

Manages the directories of data and label files and extracts relevant information from their filenames.
Also responsible for generating the "TrialManager" instance using this information in the constructor.

### TrialManager

Responsible for all subsequent operations on the data, all of which are subsequently handled by the 
"ImuManager" except for the following methods which it is directly responsible for:

- ReSampByTask: Calculates the number of packets recorded for each IMU across the same task in order to 
downsample IMUs which recorded "too many" packets for these tasks.
- ReSampByTaskQuat: Same as above except operates on the quaternion (rather than inertial and ADC data).
- CalcTime: Calculates the time (in seconds) for each task recorded using the known sampling rate
- CalcJointAngle: Uses the quaternions calculated for two adjacent IMUs in order to calculate the joint angle
between them

### ImuManager

Variously calls "ExportManager", "FeatureManager", "FilterManager", and "PlotManager" to deal with these operations.
Directly responsible for the following methods:

- PartitionData: Seperates the inertial and ADC data in seperate dataframes since they are sampled at different rates.
- PreSampleData: If a timestamp is provided with the data then this is used in order accurately interpolate for
missed packets
- CalibrateImus: Searches for the appropriate calibration file, imports the calibration values, and applies it to the data
- StandardiseUnits: Converts the data from its raw format to standard units
- ApplyRotation: Applies a rotation to the IMU device inertial data in order to simulate the same orientation of the IMU device
relative to the human body
- CalcOrientation: Calculates the orientation of the IMU relative to a global axis frame using the Madgwick gradient descent algorithm

### WorkerClass

Inherits from QObject. Generic background worker class which can be easily be run on a new thread and provide updates to the
GUI. Responsible for running methods on all threads except the main GUI thread

### FilterManager

Used to apply filters to the data as well as for applying FFT to the time series data to calculate the frequency series.

### PlotManager

Responsible for plotting time or frequency data and either displaying or saving these figures

### FeatureManager

Class for calculating all the time and frequency series features across each task

### ExportManager

Responsible for exporting the calculated features (using pickle) as well as a text descriptor which provides details about the test and how
the data was processed


